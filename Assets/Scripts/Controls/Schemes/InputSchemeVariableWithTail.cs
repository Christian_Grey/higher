﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputSchemeVariableWithTail : InputScheme
{
    //Setting
    [SerializeField] float _minAngle, _maxAngle;
    [SerializeField] float _maxPressTime = 3f;
    [SerializeField] float _pullbackDeadzone = 50f;
    [SerializeField] float _maxPullBackDistance = 400f;

    //Links
    [SerializeField] CharacterController _character;
    [SerializeField] ForceDrawer _forceDrawer;

    float _buttonPressedTimer;
    Vector3 _inputDownPos;
    Vector3 _inputHoldPos;
    Vector3 _inputClampedHoldPos;
    float _dist;
    float _force;
    bool isFirstTouch = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ReadKeyboard();
        ReadMouse();
        //ReadTouch();
    }



    private void ReadKeyboard()
    {
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow))
        {
            _buttonPressedTimer += Time.deltaTime;

            _force = Mathf.Clamp(_buttonPressedTimer, 0f, _maxPressTime) / _maxPressTime;
        }


        if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow))
        {
            float angle = Mathf.Lerp(_minAngle, _maxAngle, _force);
            Vector3 testVec = Quaternion.AngleAxis(angle, Vector3.back) * Vector3.left;
            //Debug.DrawRay(transform.position, testVec, Color.red, 5f);
            //normalize
            if (Input.GetKeyUp(KeyCode.RightArrow))
                _character.Jump(_force, testVec.With(x: -testVec.x));
            else if (Input.GetKeyUp(KeyCode.LeftArrow))
                _character.Jump(_force, testVec);
            _buttonPressedTimer = 0;
        }
    }



    private void ReadTouch()
    {
        if (Input.touchCount > 0 && isFirstTouch == true)
        {
            //print("Touch down");
            isFirstTouch = false;
            _inputDownPos = Input.touches[0].position;
            _forceDrawer.ReceiveInputPos(_inputDownPos, _inputDownPos, _inputDownPos);
        }
        else if (Input.touchCount > 0 && isFirstTouch == false)
        {
            //DebugText.text = "GOING";
            _inputHoldPos = Input.touches[0].position;
            _inputClampedHoldPos = Vector3.ClampMagnitude(_inputHoldPos - _inputDownPos, _maxPullBackDistance);
            _dist = Vector3.Distance(_inputHoldPos, _inputDownPos);
            _force = Mathf.Clamp(_dist, 0, _maxPullBackDistance) / _maxPullBackDistance;
            _forceDrawer.ReceiveForce(_force);
            _forceDrawer.ReceiveInputPos(_inputDownPos, _inputHoldPos, _inputClampedHoldPos);
        }
        else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            isFirstTouch = true;

            _forceDrawer.EraseLine();
            _character.Jump(_force, _inputDownPos - _inputHoldPos);
        }

    }

    private void ReadMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            print("Mouse down");
            _inputDownPos = Input.mousePosition;
            _forceDrawer.ReceiveInputPos(_inputDownPos, _inputDownPos, _inputDownPos);
        }

        else if (Input.GetMouseButton(0))
        {
            _inputHoldPos = Input.mousePosition;
            if (Vector3.Distance(_inputDownPos, _inputHoldPos) < _pullbackDeadzone)
            {
                _inputHoldPos = _inputDownPos;
                _forceDrawer.EraseLine();
                return;
            }

            //FIXED FORCE
            //_dist = Vector3.Distance(_inputUpPos, _inputDownPos);
            // _force = 1f;

            /*
            //TRYING TO MAKE MORE INTUITIVE CONTROLL
            _dist = Vector3.Distance(_inputUpPos, _inputDownPos);
            var quant = MaxPullBackDistance / 3f;
            var howManyQuants = Mathf.CeilToInt((Mathf.Clamp(_dist, 0, MaxPullBackDistance) / quant));
            _force = howManyQuants / 3f;
            */
            _inputClampedHoldPos = _inputDownPos + Vector3.ClampMagnitude(_inputHoldPos - _inputDownPos, _maxPullBackDistance);
            _dist = Vector3.Distance(_inputHoldPos, _inputDownPos);
            _force = Mathf.Clamp(_dist, 0, _maxPullBackDistance) / _maxPullBackDistance;
            _forceDrawer.ReceiveForce(_force);
            _forceDrawer.ReceiveInputPos(_inputDownPos, _inputHoldPos, _inputClampedHoldPos);
        }

        else if (Input.GetMouseButtonUp(0))
        {
            _character.Jump(_force, _inputDownPos - _inputHoldPos);
            _forceDrawer.EraseLine();
        }
    }


}
