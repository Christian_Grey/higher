﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSchemeVelocity : MonoBehaviour
{
    [SerializeField] CharacterController2 _characterController2;


    // Update is called once per frame
    void Update()
    {
        //TODO:Touch input

        Vector3 mousePos = Vector3.zero;
        float horizontalInput = 0f;

        //Keyboard input
        horizontalInput = Input.GetAxisRaw("Horizontal");

        //Touch / mouse input
        if (Input.GetMouseButton(0))
        {
            mousePos = Input.mousePosition;

            if (mousePos.x > Screen.width / 2f)
                horizontalInput = 1f;
            else
                horizontalInput = -1f;
        }


        _characterController2.ReceiveHorizInput(horizontalInput);
    }

}
