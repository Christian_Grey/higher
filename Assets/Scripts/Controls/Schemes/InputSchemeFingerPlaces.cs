﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputSchemeFingerPlaces : InputScheme
{
    //Setting
    [SerializeField] float _pullbackDeadzone = 50f;
    [SerializeField] float _maxPullBackDistance = 400f;

    //Links
    [SerializeField] CharacterController _character;
    [SerializeField] ForceDrawer _forceDrawer;

    Vector3 _inputDownPos;
    Vector3 _inputHoldPos;
    Vector3 _inputClampedHoldPos;
    float _dist;
    float _force;
    bool isFirstTouch = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ReadMouse();
    }

    private void ReadMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            print("Mouse down");
            _inputDownPos = Input.mousePosition;
            _forceDrawer.ReceiveInputPos(_inputDownPos, _inputDownPos, _inputDownPos);
        }

        else if (Input.GetMouseButton(0))
        {
            _inputHoldPos = Input.mousePosition;
            if (Vector3.Distance(_inputDownPos, _inputHoldPos) < _pullbackDeadzone)
            {
                _inputHoldPos = _inputDownPos;
                _forceDrawer.EraseLine();
                return;
            }

            _inputClampedHoldPos = _inputDownPos + Vector3.ClampMagnitude(_inputHoldPos - _inputDownPos, _maxPullBackDistance);
            _dist = Vector3.Distance(_inputHoldPos, _inputDownPos);
            _force = Mathf.Clamp(_dist, 0, _maxPullBackDistance) / _maxPullBackDistance;
            _forceDrawer.ReceiveForce(_force);
            _forceDrawer.ReceiveInputPos(_inputDownPos, _inputHoldPos, _inputClampedHoldPos);
        }

        else if (Input.GetMouseButtonUp(0))
        {
            _character.Jump(_force, _inputDownPos - _inputHoldPos);
            _forceDrawer.EraseLine();
        }
    }


}
