﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Created on Fri Jun 28 2019
 *
 * Copyright (c) 2019 Global Sculptor LLC
 */
[System.Serializable]
public class LevelSave
{
    public List<HigherObject> objects;
}

[System.Serializable]
public class HigherObject
{
    public float x;
    public float y;
    public float z;
    public float width;
    public float height;
    public string prefab;
    public MovingObjectData movingObject;
}

[System.Serializable]
public class MovingObjectData
{
    public List<MovingPoint> points;
    public float speed;
}

[System.Serializable]
public class MovingPoint
{
    public float x;
    public float y;
    public float z;
}





