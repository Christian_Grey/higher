﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using JsonFx.Json;
using System;

public class LevelLoader : PersistentHumbleSingleton<LevelLoader>
{
    public string level = "1";
    public string variant = "1";

    LevelSave levelSave;

    //Dictionary<string, string> prefabs = new Dictionary<string, string>()
    //{
    //    {"RegularPlatform", "Prefabs/Platforms/RegularPlatform" },
    //    {"OneWayPlatform Variant", "Prefabs/Platforms/OneWayPlatform Variant"},
    //    {"MovingPlatform Variant", "Prefabs/Platforms/MovingPlatform Variant"},
    //    {"JumpyPlatform", "Prefabs/Platforms/JumpyPlatform"},
    //    {"IcePlatform Variant", "Prefabs/Platforms/IcePlatform Variant"},
    //    {"BouncyPlatform Variant", "Prefabs/Platforms/BouncyPlatform Variant"},

    //    {"Bat", "Prefabs/ObjectsAndMechanics/Bat"},
    //    {"BlackHole", "Prefabs/ObjectsAndMechanics/BlackHole"},
    //    {"Chest", "Prefabs/ObjectsAndMechanics/Chest"},
    //    {"Coin", "Prefabs/ObjectsAndMechanics/Coin"},
    //    {"Flag", "Prefabs/ObjectsAndMechanics/Flag"},
    //    {"Spike", "Prefabs/ObjectsAndMechanics/Spike"},
    //    {"Collectable", "Prefabs/ObjectsAndMechanics/Collectable"},
    //};

    // Use this for initialization
    public void Start()
    {

        print($"Calling start");
        /*
        prefabs.Add("RegularPlatform", "Prefabs/Platforms/RegularPlatform");
        prefabs.Add("OneWayPlatform Variant", "Prefabs/Platforms/OneWayPlatform Variant");
        prefabs.Add("MovingPlatform Variant", "Prefabs/Platforms/MovingPlatform Variant");
        prefabs.Add("JumpyPlatform", "Prefabs/Platforms/JumpyPlatform");
        prefabs.Add("IcePlatform Variant", "Prefabs/Platforms/IcePlatform Variant");
        prefabs.Add("BouncyPlatform Variant", "Prefabs/Platforms/BouncyPlatform Variant");

        prefabs.Add("Bat", "Prefabs/ObjectsAndMechanics/Bat");
        prefabs.Add("BlackHole", "Prefabs/ObjectsAndMechanics/BlackHole");
        prefabs.Add("Chest", "Prefabs/ObjectsAndMechanics/Chest");
        prefabs.Add("Coin", "Prefabs/ObjectsAndMechanics/Coin");
        prefabs.Add("Flag", "Prefabs/ObjectsAndMechanics/Flag");
        prefabs.Add("Spike", "Prefabs/ObjectsAndMechanics/Spike");
        prefabs.Add("Collectable", "Prefabs/ObjectsAndMechanics/Collectable");
        */
    }

    public void insert()
    {
        int currentLevel = System.Convert.ToInt32(level);
        int lastLevel = currentLevel;
        while (true)
        {
            var textAsset = Resources.Load("Levels/Level_" + lastLevel + "_" + variant) as TextAsset;
            if (textAsset != null)
            {
                ++lastLevel;
            }
            else
            {
                break;
            }
        }

        --lastLevel;

        while (true)
        {
            if (lastLevel == currentLevel)
            {
                break;
            }
            else
            {
                var textAsset = Resources.Load("Levels/Level_" + lastLevel + "_" + variant) as TextAsset;
                try
                {
                    int nextLevel = lastLevel + 1;
                    using (StreamWriter sw = new StreamWriter("Assets/Resources/Levels/Level_" + nextLevel + "_" + variant + ".txt", false))
                    {
                        sw.Write(textAsset.text);
                    }
                }
                catch (IOException e)
                {
                    Debug.Log(e.Message);
                }
            }

            --lastLevel;
        }

        Debug.LogError("TODO: Doesnt Save in INSERT!");
        //saveOld();
    }


    public void SaveLevelSettings()
    {
        var s = new LevelJsonGen()
        {
            GeneratorSettingsName = GameController.I.CurrentLevel.Settings.name,
            GoalsCombination = GameController.I.CurrentLevel.Settings.GoalsCombination
        };
        SaveToFile(s, "Assets/Resources/Levels/Level_" + level + "_" + variant + ".json");
        Debug.Log($"Level {level}, var {variant} SAVED");

    }

    public static LevelJsonGen LoadFromResources(string path)
    {
        var textAsset = Resources.Load(path) as TextAsset;
        Debug.Log($"Path {path}");
        Debug.Log($"Text asset {textAsset}");
        return JsonReader.Deserialize<LevelJsonGen>(textAsset.text);
    }


    public static T LoadFromFile<T>(string path)
    {
        string data = FileUtils.Load(path);
        if (data == null) return default(T);
        T obj = JsonReader.Deserialize<T>(data);
        return obj;
    }

    public static void SaveToFile(object data, string path)
    {
        try
        {
            using (StreamWriter sw = new StreamWriter(path, false))
            {
                sw.Write(JsonWriter.Serialize(data));
            }
        }
        catch (IOException e)
        {
            Debug.Log(e.Message);
        }
        finally
        {
#if UNITY_EDITOR
            AssetDatabase.Refresh();
#endif
        }
    }

    public void load()
    {
        Debug.Log("Levels/Level_" + level + "_" + variant);
        var textAsset = Resources.Load("Levels/Level_" + level + "_" + variant) as TextAsset;
        levelSave = JsonUtility.FromJson<LevelSave>(textAsset.text);

        clear();

        Debug.Log(levelSave.objects.Count);

        GameObject levelObject = null;

        GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        foreach (var obj in allObjects)
        {
            if (obj.name.StartsWith("Level"))
                levelObject = obj;
        }

        if (levelObject == null)
            levelObject = new GameObject();

        levelObject.name = $"Level_{level}_{variant}";

        foreach (HigherObject higherObject in levelSave.objects)
        {
            foreach (string key in Prefabs.DestructibleObjects.Keys)
            {
                if (higherObject.prefab.StartsWith(key))
                {
                    GameObject inst = Instantiate(Prefabs.DestructibleObjects[key]) as GameObject;
                    inst.transform.localPosition = new Vector3(higherObject.x, higherObject.y, higherObject.z);
                    inst.name = higherObject.prefab;
                    inst.transform.parent = levelObject.transform;


                    SpriteRenderer spriteRenderer = inst.GetComponent<SpriteRenderer>();
                    spriteRenderer.size = new Vector2(higherObject.width, higherObject.height);

                    MovingObject movingObject = inst.GetComponent<MovingObject>();
                    if (movingObject != null && higherObject.movingObject != null)
                    {
                        MovingObjectData movingObjectData = higherObject.movingObject;
                        movingObject.Speed = movingObjectData.speed;
                        movingObject.Points = new List<Vector3>(movingObjectData.points.Count);
                        int i = 0;
                        foreach (MovingPoint movingPoint in movingObjectData.points)
                        {
                            movingObject.Points.Add(new Vector3(movingPoint.x, movingPoint.y, movingPoint.z));
                            i++;
                        }
                    }

                    break;
                }
            }
        }
    }

    public static void clear()
    {
        print("Clear level");
        int destroyedObjectsCount = 0;
        GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();

        foreach (GameObject go in allObjects)
        {
            if (go == null) continue;

            foreach (string key in Prefabs.DestructibleObjects.Keys)
            {
                if (go.name.StartsWith(key))
                {

                    if (Application.isPlaying)
                        Destroy(go);
                    else
                        DestroyImmediate(go);

                    ++destroyedObjectsCount;
                    break;
                }
            }
        }
    }




    public void saveOld()
    {
        levelSave = new LevelSave();
        levelSave.objects = new List<HigherObject>();
        GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
        foreach (GameObject go in allObjects)
        {
            foreach (string key in Prefabs.DestructibleObjects.Keys)
            {
                if (go.name.StartsWith(key))
                {
                    HigherObject ho = new HigherObject();
                    ho.x = go.transform.position.x;
                    ho.y = go.transform.position.y;
                    ho.z = go.transform.position.z;
                    SpriteRenderer spriteRenderer = go.GetComponent<SpriteRenderer>();
                    ho.width = spriteRenderer.size.x;
                    ho.height = spriteRenderer.size.y;
                    ho.prefab = go.name;

                    MovingObject movingObject = go.GetComponent<MovingObject>();
                    if (movingObject != null)
                    {
                        MovingObjectData movingObjectData = new MovingObjectData();
                        movingObjectData.speed = movingObject.Speed;
                        movingObjectData.points = new List<MovingPoint>();
                        foreach (Vector3 point in movingObject.Points)
                        {
                            MovingPoint movingPoint = new MovingPoint();
                            movingPoint.x = point.x;
                            movingPoint.y = point.y;
                            movingPoint.z = point.z;
                            movingObjectData.points.Add(movingPoint);
                        }
                        ho.movingObject = movingObjectData;
                    }

                    levelSave.objects.Add(ho);
                    break;
                }
            }
        }

        Debug.Log($"Saved {levelSave.objects.Count} objects.");

        try
        {
            using (StreamWriter sw = new StreamWriter("Assets/Resources/Levels/Level_" + level + "_" + variant + ".txt", false))
            {
                sw.Write(JsonUtility.ToJson(levelSave));
            }
        }
        catch (IOException e)
        {
            Debug.Log(e.Message);
        }
        finally
        {
#if UNITY_EDITOR
            AssetDatabase.Refresh();
#endif
        }
    }
}
