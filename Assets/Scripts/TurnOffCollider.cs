﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffCollider : MonoBehaviour
{
    [SerializeField] Collider2D _colliderToTurnOff;


    private void OnTriggerEnter2D(Collider2D incomingCol)
    {
        if (incomingCol.gameObject.layer == 10)
        {
            // print("Turn off collisions");
            Physics2D.IgnoreCollision(incomingCol, _colliderToTurnOff, true);
            _colliderToTurnOff.gameObject.layer = 2;
        }
    }

    private void OnTriggerStay2D(Collider2D incomingCol)
    {
        if (incomingCol.gameObject.layer == 10)
        {
            // print("Turn off collisions");
            Physics2D.IgnoreCollision(incomingCol, _colliderToTurnOff, true);
            _colliderToTurnOff.gameObject.layer = 2;
        }
    }

    private void OnTriggerExit2D(Collider2D outcomingCol)
    {
        //print("Trigger exit");
        if (outcomingCol.gameObject.layer == 10)
            StartCoroutine(TurnOnCollision(outcomingCol));
    }

    IEnumerator TurnOnCollision(Collider2D col)
    {
        yield return new WaitForSeconds(0.04f);
        //print("Turn on collisions");
        Physics2D.IgnoreCollision(col, _colliderToTurnOff, false);
        _colliderToTurnOff.gameObject.layer = 0;
    }

}
