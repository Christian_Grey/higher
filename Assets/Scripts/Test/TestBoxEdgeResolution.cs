﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBoxEdgeResolution : MonoBehaviour
{
    Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _rb.velocity = new Vector2(30, 30);


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log($"BOX COLLISION");
    }
}
