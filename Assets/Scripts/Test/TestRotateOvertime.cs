﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRotateOvertime : MonoBehaviour
{
    [SerializeField] float _rotationSpeed = 4f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        transform.eulerAngles = transform.eulerAngles.WithModified(z: _rotationSpeed * Time.deltaTime);

    }
}
