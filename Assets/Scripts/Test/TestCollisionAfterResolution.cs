﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCollisionAfterResolution : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log($"Collision registered");
        GetComponent<BoxCollider2D>().enabled = false;
    }


}
