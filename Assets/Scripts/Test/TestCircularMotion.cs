﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCircularMotion : MonoBehaviour
{
    [SerializeField] float speed = 2f;
    [SerializeField] float width = 2f;
    [SerializeField] float height = 2f;
    float timeCounter = 0;
    Vector3 newPos = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timeCounter += Time.deltaTime * speed;


        newPos.x = Mathf.Cos(timeCounter) * width;
        newPos.y = Mathf.Sin(timeCounter) * height;


        transform.position = newPos;
    }
}
