﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPoolPreudoRandom : MonoBehaviour
{

    List<PlatformWeight> platforms;
    PoolPseudoRandom<PlatformType> Pool;

    // Start is called before the first frame update
    void Start()
    {
        platforms = new List<PlatformWeight>()
    {
        {new PlatformWeight(PlatformType.OneWay, 140) },
        {new PlatformWeight(PlatformType.False, 20) },
        {new PlatformWeight(PlatformType.Icy, 20) },
        {new PlatformWeight(PlatformType.Jumpy, 20) }
    };
        var plArray = platforms.ToArray();


        List<PlatformType> platformTypes = new List<PlatformType>();
        List<float> platformWeights = new List<float>();

        foreach (var pl in platforms)
        {
            platformTypes.Add(pl.Type);
            platformWeights.Add(pl.Weight);
        }


        Pool = new PoolPseudoRandom<PlatformType>(platformTypes, platformWeights);



    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            var obj = Pool.GetRandom();
            Pool.DecreaseChance(obj);
        }

    }

    private void OnDrawGizmos()
    {
        if (Pool == null) return;

        for (int i = 0; i < Pool.Objects.Count; i++)
        {
            Gizmos.DrawLine(Vector3.down * i, Vector3.down * i + Vector3.right * Pool.CurrentWeights[i] / 10f);

        }


    }
}
