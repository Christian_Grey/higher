﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/*
 * Created on Sun Jun 23 2019
 *
 * Copyright (c) 2019 Global Sculptor LLC
 */
public class LevelEditor : EditorWindow
{
    public static Vector3 mousePosition;
    public static Vector3 mouseWorldPosition;
    private static string prefabPath = null;
    private static LevelLoader levelLoader;

    static LevelEditor()
    {
        //levelLoader = LevelLoader.I;
        //levelLoader.Start();
        levelLoader = new LevelLoader();
        levelLoader.Start();
        SceneView.onSceneGUIDelegate += SceneGUI;
        Debug.Log("constructor created");
    }

    [MenuItem("Window/Level Editor/Higher")]
    public static void ShowWindow()
    {
        GetWindow<LevelEditor>("Higher");
    }
    private void OnGUI()
    {
        GUILayout.Label("Higher Level Editor", EditorStyles.boldLabel);

        GUILayout.Label("Platforms", EditorStyles.label);

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Regular Platform"))
        {
            prefabPath = "Prefabs/Platforms/RegularPlatform";
        }

        if (GUILayout.Button("One Way Platform"))
        {
            prefabPath = "Prefabs/Platforms/OneWayPlatform Variant";
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Moving Platform"))
        {
            prefabPath = "Prefabs/Platforms/MovingPlatform Variant";
        }

        if (GUILayout.Button("Jumpy Platform"))
        {
            prefabPath = "Prefabs/Platforms/JumpyPlatform";
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Ice Platform"))
        {
            prefabPath = "Prefabs/Platforms/IcePlatform Variant";
        }

        if (GUILayout.Button("Bouncy Platform"))
        {
            prefabPath = "Prefabs/Platforms/BouncyPlatform Variant";
        }

        GUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        // Objects and Mechanics

        GUILayout.Label("Objects and Mechanics", EditorStyles.label);

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Bat"))
        {
            prefabPath = "Prefabs/ObjectsAndMechanics/Bat";
        }

        if (GUILayout.Button("BlackHole"))
        {
            prefabPath = "Prefabs/ObjectsAndMechanics/BlackHole";
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Chest"))
        {
            prefabPath = "Prefabs/ObjectsAndMechanics/Chest";
        }

        if (GUILayout.Button("Coin"))
        {
            prefabPath = "Prefabs/ObjectsAndMechanics/Coin";
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Flag"))
        {
            prefabPath = "Prefabs/ObjectsAndMechanics/Flag";
        }

        if (GUILayout.Button("Spike"))
        {
            prefabPath = "Prefabs/ObjectsAndMechanics/Spike";
        }

        GUILayout.EndHorizontal();

        // File

        EditorGUILayout.Space();

        GUILayout.BeginVertical(EditorStyles.helpBox);

        EditorGUILayout.Space();

        GUILayout.Label("File", EditorStyles.label);

        EditorGUILayout.Space();

        levelLoader.level = EditorGUILayout.TextField("Level", levelLoader.level);

        levelLoader.variant = EditorGUILayout.TextField("Variant", levelLoader.variant);

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Load"))
        {
            levelLoader.load();
        }

        if (GUILayout.Button("Save"))
        {
            levelLoader.SaveLevelSettings();
        }

        GUILayout.EndHorizontal();

        EditorGUILayout.Space();

        EditorGUILayout.Space();

        levelLoader.level = EditorGUILayout.TextField("Level", levelLoader.level);

        levelLoader.variant = EditorGUILayout.TextField("Variant", levelLoader.variant);

        if (GUILayout.Button("Insert"))
        {
            levelLoader.insert();
        }

        if (GUILayout.Button("Random Level"))
        {
            //LevelGenerator.I.GenerateLevel();
            GameController.I.GenerateLevel();
        }

        GUI.backgroundColor = Colors.RedCrayola;
        if (GUILayout.Button("Clear objects"))
        {
            LevelLoader.clear();
        }

        EditorGUILayout.Space();

        GUILayout.EndVertical();



    }

    static void SceneGUI(SceneView sceneView)
    {
        // This will have scene events including mouse down on scenes objects
        Event current = Event.current;

        if (prefabPath != null && current.type == EventType.MouseDown && current.button == 0)
        {
            // left mouse click
            mousePosition = (Vector3)current.mousePosition;
            mousePosition.y = Camera.current.pixelHeight - mousePosition.y;
            mouseWorldPosition = sceneView.camera.ScreenToWorldPoint(mousePosition);
            mouseWorldPosition.z = 0;

            GameObject inst = Instantiate(Resources.Load(prefabPath)) as GameObject;
            inst.transform.localPosition = mouseWorldPosition;
            prefabPath = null;
        }

    }

    GameObject currentSelection;

    private void OnFocus()
    {
        currentSelection = Selection.activeGameObject;
    }

    private void OnLostFocus()
    {
        currentSelection = null;
    }
}
