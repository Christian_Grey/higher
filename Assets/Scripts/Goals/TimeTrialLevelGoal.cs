﻿using UnityEngine;
using System.Collections;


//public class TimeTrialLevelGoal : LevelGoal
//{
//    [SerializeField] RectTransform _clockArrow;
//    public float TargetTime;
//    float _timer = 100000f;

//    public override LevelGoalType GoalType => LevelGoalType.TimeTrial;

//    public void SetTargetTime(float targetTime)
//    {
//        TargetTime = Mathf.RoundToInt(targetTime * (1 + GameController.I.Difficulty));
//        _timer = TargetTime;
//        Reached = true;
//        InitTargetAndCounterText();
//    }

//    public override void InitTargetAndCounterText()
//    {
//        base.InitTargetAndCounterText();
//        _uiCounter.text = _timer.ToString("0.0");
//    }

//    protected override void OnEnable()
//    {
//        base.OnEnable();
//        //_uiName.text = "Timer";
//        Reached = true;
//    }

//    void Update()
//    {
//        if (GameController.I.IsRunning == false) return;

//        _timer -= Time.deltaTime;
//        float progress = (_timer / TargetTime).Invert01();

//        //_clockArrow.localRotation = Quaternion.Lerp(Quaternion.identity, Quaternion.Euler(0, 0, -180f), progress);
//        _clockArrow.Rotate(new Vector3(0, 0, (-360f / TargetTime) * Time.deltaTime));
//        _uiCounter.text = _timer.ToString("0.0");

//        if (_timer <= 0)
//        {

//            _timer = 0;
//            Reached = false;
//            Failed = true;
//        }
//    }
//}
