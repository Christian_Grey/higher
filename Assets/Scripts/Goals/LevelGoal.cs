﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public abstract class LevelGoal : MonoBehaviour
{
    public bool Reached = false;
    public bool SuperReached = false;
    public bool Failed = false;
    public abstract LevelGoalType GoalType { get; }

    protected TextMeshProUGUI _uiCounter;

    protected virtual void OnEnable()
    {
        Debug.Log($"GOAL ON ENABLE {gameObject.name}");
        InitTargetAndCounterText();
        transform.SetParent(GUIController.I.HiddenGoalsGrid);
    }

    public virtual void InitTargetAndCounterText()
    {
        //_uiCounter = transform.Find("Counter").GetComponent<TextMeshProUGUI>();
        _uiCounter = GetComponentInChildren<TextMeshProUGUI>();
    }

    public virtual void GetIntoPlace()
    {


    }


    //public static Dictionary<LevelGoalCombination, int> Combinations = new Dictionary<LevelGoalCombination, int>()
    //{
    //    {new LevelGoalCombination(LevelGoalType.ReachTheFlag), 30 },
    //    {new LevelGoalCombination(LevelGoalType.Collect), 20 },
    //    {new LevelGoalCombination(LevelGoalType.FloorsInRow), 10 },

    //    {new LevelGoalCombination(LevelGoalType.ReachTheFlag,LevelGoalType.TimeTrial ), 20 },
    //    {new LevelGoalCombination(LevelGoalType.Collect,LevelGoalType.TimeTrial ), 15 },
    //    {new LevelGoalCombination(LevelGoalType.FloorsInRow,LevelGoalType.TimeTrial ), 8 },
    //    {new LevelGoalCombination(LevelGoalType.Collect ,LevelGoalType.FloorsInRow ), 4 },
    //};
}

[System.Serializable]
public class LevelGoalCombination
{
    [SerializeField]
    public LevelGoalType Goal1;
    [SerializeField]
    public LevelGoalType Goal2;

    public LevelGoalCombination(LevelGoalType goal1, LevelGoalType goal2)
    {
        Goal1 = goal1;
        Goal2 = goal2;
    }
    public LevelGoalCombination(LevelGoalType goal1)
    {
        Goal1 = goal1;
    }

    public LevelGoalCombination()
    {
    }

    public bool Contains(LevelGoalType type)
    {
        if (Goal1 == type || Goal2 == type)
            return true;
        else
            return false;
    }

}



public enum LevelGoalType
{
    None,
    ReachTheFlag,
    Collect,
    //TimeTrial,
    FloorsInRow,
    SlowLava,
    FastLava,
    DontDie,
    Die,
}


