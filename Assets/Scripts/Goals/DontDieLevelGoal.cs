﻿using UnityEngine;
using System.Collections;

public class DontDieLevelGoal : LevelGoal
{
    public override LevelGoalType GoalType => LevelGoalType.DontDie;




    protected override void OnEnable()
    {
        base.OnEnable();

        EventSystem.AddListener<PlayerHasDied>(OnPlayerHasDied);
    }

    void OnDisable()
    {
        EventSystem.DeleteListener<PlayerHasDied>(OnPlayerHasDied);
    }


    void OnPlayerHasDied(PlayerHasDied info)
    {
        Failed = true;
    }

}
