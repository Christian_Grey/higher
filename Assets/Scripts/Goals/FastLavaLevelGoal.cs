﻿using UnityEngine;
using System.Collections;

public class FastLavaLevelGoal : LevelGoal
{
    public override LevelGoalType GoalType => LevelGoalType.FastLava;

    protected override void OnEnable()
    {
        base.OnEnable();
        Reached = true;
    }
}
