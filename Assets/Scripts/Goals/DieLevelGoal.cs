﻿using UnityEngine;
using System.Collections;

public class DieLevelGoal : LevelGoal
{
    public override LevelGoalType GoalType => LevelGoalType.Die;




    protected override void OnEnable()
    {
        base.OnEnable();

        EventSystem.AddListener<PlayerHasDied>(OnPlayerHasDied);
    }

    void OnDisable()
    {
        EventSystem.DeleteListener<PlayerHasDied>(OnPlayerHasDied);
    }


    void OnPlayerHasDied(PlayerHasDied info)
    {
        SuperReached = true;

        //if (GameController.I.AlreadyDied == false)
        //{
        //    Failed = true;
        //}
        //else
        //{
        //    Reached = true;
        //    SuperReached = true;
        //}

    }

}
