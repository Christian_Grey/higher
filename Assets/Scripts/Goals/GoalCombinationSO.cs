﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
[System.Serializable]
public class GoalCombinationSO : ScriptableObject
{
    public List<LevelGoalType> Goals;
    public int Probability;
}
