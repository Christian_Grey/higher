﻿using UnityEngine;
using System.Collections;

public class ReachTheFlagLevelGoal : LevelGoal
{
    public override LevelGoalType GoalType => LevelGoalType.ReachTheFlag;

    public void OnFlagReached(FlagReached info)
    {
        Reached = true;
        SuperReached = true;
    }

    public override void InitTargetAndCounterText()
    {
        base.InitTargetAndCounterText();
        _uiCounter.text = "";
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        EventSystem.AddListener<FlagReached>(OnFlagReached);
    }


    private void OnDisable()
    {
        EventSystem.DeleteListener<FlagReached>(OnFlagReached);
    }
}