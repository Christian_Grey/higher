﻿using UnityEngine;
using System.Collections;

public class CollectLevelGoal : LevelGoal
{
    public int TargetValue;
    public override LevelGoalType GoalType => LevelGoalType.Collect;

    int _currentValue;

    void OnCollectableCollected(CollectableCollected info)
    {
        _currentValue++;
        _uiCounter.text = $"{ _currentValue} / {TargetValue}";
        if (_currentValue >= TargetValue)
        {
            SuperReached = true;
            Reached = true;
            EventSystem.DeleteListener<CollectableCollected>(OnCollectableCollected);
        }
    }

    public override void InitTargetAndCounterText()
    {
        base.InitTargetAndCounterText();
        TargetValue += Mathf.RoundToInt(TargetValue * GameController.I.Difficulty);
        _uiCounter.text = $"{ _currentValue} / {TargetValue}";
    }

    public override void GetIntoPlace()
    {
        base.GetIntoPlace();
        transform.SetParent(GUIController.I.GoalsGrid.transform);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        EventSystem.AddListener<CollectableCollected>(OnCollectableCollected);
    }
    private void OnDisable()
    {
        EventSystem.DeleteListener<CollectableCollected>(OnCollectableCollected);
    }
}
