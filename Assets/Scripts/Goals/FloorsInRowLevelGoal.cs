﻿using UnityEngine;
using System.Collections;

public class FloorsInRowLevelGoal : LevelGoal
{
    public int TargetValue;

    public override LevelGoalType GoalType => LevelGoalType.FloorsInRow;

    void Update()
    {
        _uiCounter.text = $"{FloorsInRowController.I.NumberOfSuperJumps.ToString("0")} / {TargetValue.ToString("0")}";
        if (FloorsInRowController.I.NumberOfSuperJumps >= TargetValue)
        {
            Reached = true;
            // EventSystem.DeleteListener<FloorsInRowValuesChanged>(OnFloorsInRowValuesChanged);
        }
    }

    //public void OnFloorsInRowValuesChanged(FloorsInRowValuesChanged info)
    //{
    //    _uiCounter.text = $"{info.FloorInRow.ToString("0")} / {TargetValue.ToString("0")}";
    //    if (info.FloorInRow >= TargetValue)
    //    {
    //        Reached = true;
    //        EventSystem.DeleteListener<FloorsInRowValuesChanged>(OnFloorsInRowValuesChanged);
    //    }
    //}

    public override void InitTargetAndCounterText()
    {
        base.InitTargetAndCounterText();
        TargetValue *= Mathf.RoundToInt(1f + GameController.I.Difficulty);
        _uiCounter.text = $"0 / {TargetValue.ToString("0")}";
    }

    //protected override void OnEnable()
    //{
    //    base.OnEnable();
    //    EventSystem.AddListener<FloorsInRowValuesChanged>(OnFloorsInRowValuesChanged);
    //}
    //private void OnDisable()
    //{
    //    EventSystem.DeleteListener<FloorsInRowValuesChanged>(OnFloorsInRowValuesChanged);
    //}
}
