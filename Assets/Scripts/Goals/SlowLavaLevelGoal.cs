﻿using UnityEngine;
using System.Collections;

public class SlowLavaLevelGoal : LevelGoal
{
    public override LevelGoalType GoalType => LevelGoalType.SlowLava;

    protected override void OnEnable()
    {
        base.OnEnable();
        Reached = true;
    }
}
