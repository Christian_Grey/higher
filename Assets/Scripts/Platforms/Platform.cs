﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Platform : MonoBehaviour
{
    public bool OneWay = true;
    public bool CanStretch = true;
    public bool CanAnythingSpawnOnTop = true;
    public float PlatformDrag = 20f; // up to 20
    public float SideBounciness = 0.5f; // from 0 to 1
    public float JumpMultiplier = 1f;

    BoxCollider2D _platformCollider;
    SpriteRenderer _sprite;
    OneWayTrigger _oneWayTrigger;
    BoxCollider2D _oneWayTriggerCol;
    Vector2 _initSize;

    public virtual void Init(Vector2 size)
    {
        _platformCollider = GetComponent<BoxCollider2D>();
        _sprite = GetComponent<SpriteRenderer>();
        _initSize = size;
        _sprite.size = new Vector2(_initSize.x + (CanStretch ? Variables.I.AdditionalPlatformWidth : 0f), _initSize.y);

        if (OneWay)
            SetUpOneWay();
    }

    public virtual void Init(float width)
    {
        Init(new Vector2(width, GetComponent<SpriteRenderer>().size.y));
    }


    void Update()
    {
        DestroyIfOutOfBounds();

        if (CanStretch)
            Stretch();
    }

    private void Stretch()
    {
        var targetSize = new Vector2(_initSize.x + Variables.I.AdditionalPlatformWidth, _initSize.y);
        var targetTriggerSize = targetSize + new Vector2(2, 3);

        if (_sprite == null)
            Debug.Log($"Sprite null on {gameObject.name} obj");
        _sprite.size = Vector2.Lerp(_sprite.size, targetSize, Time.deltaTime * 2f);

        if (_oneWayTriggerCol != null)
        {
            _oneWayTriggerCol.size = Vector2.Lerp(_oneWayTriggerCol.size, targetTriggerSize, Time.deltaTime * 2f);
        }
    }

    private void DestroyIfOutOfBounds()
    {
        if (GameController.I.CurrentLevel.Settings.Direction == DirectionType.Down)
        {
            float cameraUpperBoundY = Camera.main.ViewportToWorldPoint(Vector3.up).y;

            if (cameraUpperBoundY + 15f < transform.position.y)
                Prime31Pool.I.Despawn(gameObject);
            //Destroy(gameObject);
        }


        //else
        //{
        //    float cameraLowerBoundY = Camera.main.ViewportToWorldPoint(Vector3.zero).y - Variables.I.PlatformDestroyMargin;

        //    if (cameraLowerBoundY > transform.position.y)
        //        Prime31Pool.I.Despawn(gameObject);
        //}
    }

    void SetUpOneWay()
    {
        //Assigning
        if (GetComponentInChildren<OneWayTrigger>() == null)
        {
            CreateOneWayTrigger();
        }
        else
        {
            Destroy(GetComponentInChildren<OneWayTrigger>().gameObject);
            CreateOneWayTrigger();
        }

        _oneWayTriggerCol.size = _sprite.size + new Vector2(2, 3);
        _oneWayTrigger.Init();

    }

    private void CreateOneWayTrigger()
    {
        var triggerGO = Instantiate(Prefabs.I.OneWayTrigger, transform.position, transform.rotation, transform);
        triggerGO.name = "OneWayTrigger";
        _oneWayTrigger = triggerGO.GetComponent<OneWayTrigger>();
        _oneWayTriggerCol = triggerGO.GetComponent<BoxCollider2D>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (_oneWayTrigger == null) return;

        if (Vector2.Dot(collision.GetContact(0).normal, Vector2.right).Abs().CloseTo(1, 0.2f))
        {
            _oneWayTrigger.PrintDebug();
        }
    }

}

public enum PlatformType
{
    Regular,
    OneWay,
    Jumpy,
    Sticky,
    Moving,
    Icy,
    False,
    Crumbly,
    Chest
}

[System.Serializable]
public class PlatformWeight
{
    public PlatformType Type;
    public int Weight;

    public PlatformWeight(PlatformType type, int weight)
    {
        Type = type;
        Weight = weight;
    }

    public static List<PlatformType> GetTypes(PlatformWeight[] platformsAndWeights)
    {
        var types = new List<PlatformType>();
        foreach (var pair in platformsAndWeights)
        {
            types.Add(pair.Type);
        }
        return types;
    }
    public static List<float> GetWeights(PlatformWeight[] platformsAndWeights)
    {
        var weights = new List<float>();
        foreach (var pair in platformsAndWeights)
        {
            weights.Add(pair.Weight);
        }
        return weights;
    }

    public static Dictionary<PlatformType, float> ToDictionary(PlatformWeight[] platformsAndWeights)
    {
        var dic = new Dictionary<PlatformType, float>();
        foreach (var pair in platformsAndWeights)
        {
            dic.Add(pair.Type, pair.Weight);
        }
        return dic;

    }

}
