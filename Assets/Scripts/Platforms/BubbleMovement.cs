﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleMovement : MonoBehaviour
{
    Collider2D _collider;

    float _highestYReached;

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float cameraBottom = Camera.main.ViewportToWorldPoint(Vector3.zero).y;
        float lavaTop = 0f;
        if (GameController.I.Lava != null)
            lavaTop = GameController.I.Lava.Top;

        float highestYThisFrame = cameraBottom > lavaTop ? cameraBottom : lavaTop;
        _highestYReached = highestYThisFrame > _highestYReached ? highestYThisFrame : _highestYReached;

        transform.position = new Vector3(0f, _highestYReached + _collider.bounds.size.y / 2f, 0f);

    }
}
