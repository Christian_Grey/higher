﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPhysicsMaterial : MonoBehaviour
{
    public float PlatformDrag = 20f; // up to 20
    public float SideBounciness = 0.5f; // from 0 to 1
    public float JumpMultiplier = 1f;


}
