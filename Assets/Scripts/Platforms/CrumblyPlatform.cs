﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrumblyPlatform : Platform
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            //gameObject.SetActive(false);

            Prime31Pool.I.Despawn(gameObject);

        }
    }
}
