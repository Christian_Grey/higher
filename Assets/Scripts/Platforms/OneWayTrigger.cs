﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayTrigger : MonoBehaviour
{
    public LayerMask RaycastAgainstThis;

    BoxCollider2D _platformCollider;
    SpriteRenderer _platformSprite;
    BoxCollider2D _target;
    Rigidbody2D _targetRb;

    float _platformTop;
    float _playerBottom;
    Vector2 _anglePos;
    Vector2 _anglePos2;

    void Start()
    {
        _target = GameController.I.Player.GetComponent<BoxCollider2D>();
        _targetRb = _target.GetComponent<Rigidbody2D>();
        _platformCollider = transform.parent.GetComponent<BoxCollider2D>();
        _platformTop = _platformCollider.bounds.max.y;
        _platformSprite = transform.parent.GetComponent<SpriteRenderer>();

        Debug.DrawRay(_platformCollider.bounds.center.With(y: _platformTop), Vector3.right, Color.green);
    }


    public void PrintDebug()
    {
        Debug.Log($"Player bottom: {_playerBottom}");
        Debug.Log($"Platf top: {_platformTop}");
        Debug.Log($"VelY {_targetRb.velocity.y}");
        Debug.Log($"Edge case: {TestAnglesEdgeCase()}");
        Debug.Log($"ENABLED???: {_platformCollider.enabled}");
    }

    public void Init()
    {
        _platformCollider = transform.parent.GetComponent<BoxCollider2D>();
        _platformTop = _platformCollider.bounds.max.y;
        //_platformSprite = transform.parent.GetComponent<SpriteRenderer>();
    }

    void LateUpdate()
    {
        //Debug.Log($"--------- Start frame -------");

        _platformTop = _platformSprite.bounds.max.y;
        _playerBottom = _target.bounds.min.y;
        //Debug.Log($"Player bottom: {_playerBottom}");
        //Debug.Log($"Platf top: {_platformTop}");

        if (TestAnglesEdgeCase() == false)
        {
            _platformCollider.enabled = false;
            //if (playerBottom > _platformTop && GameController.I.Player.CurrentVeloctiy.y <= 0)
            if (_playerBottom > _platformTop && _targetRb.velocity.y <= 0)
            {
                //if ((_playerBottom.Abs() - _platformTop.Abs()).CloseTo(0f, 0.5f))
                //{
                //    //Debug.Log($"pl bot: {_playerBottom.Abs()}, Platf top: {_platformTop.Abs()}, dif: {(_playerBottom.Abs() - _platformTop.Abs())}");
                //    Debug.Log($"### ON ### Player bottom: {_playerBottom}, Platf top: {_platformTop}");

                //}
                //Debug.Log($"Player is higher && velY<0, Turn on colliders");
                _platformCollider.enabled = true;
            }
            else
            {
                //if ((_playerBottom.Abs() - _platformTop.Abs()).CloseTo(0f, 0.5f))
                //    Debug.Log($"+++ OFF +++  Player bottom: {_playerBottom}, Platf top: {_platformTop}");
                //Debug.Log($"Player is lower or velY>0, Turn off colliders");
                _platformCollider.enabled = false;
            }
        }

    }

    private bool TestAnglesEdgeCase()
    {
        if (_playerBottom < _platformTop) return false;

        float velX = _targetRb.velocity.x;
        _anglePos = new Vector2(velX > 0 ? _target.bounds.max.x : _target.bounds.min.x, _target.bounds.min.y);
        _anglePos2 = _anglePos + _targetRb.velocity * 2f * Time.deltaTime;
        //Debug.DrawRay(_anglePos, Vector3.right, Color.blue);
        //Debug.DrawRay(_anglePos2, Vector3.right, Color.red);
        //Debug.DrawRay(_anglePos, _anglePos2 - _anglePos, Color.green);

        RaycastHit2D hit = Physics2D.Raycast(_anglePos, _anglePos2 - _anglePos, (_anglePos2 - _anglePos).magnitude, RaycastAgainstThis);

        if (hit)
        {
            //Debug.Log($"Hit: {hit.collider.gameObject.name}");
            //Debug.DrawRay(hit.point, hit.normal, Color.cyan);
            if (Vector2.Dot(hit.normal, Vector2.right).Abs().CloseTo(1f, 0.2f))
            {
                //Debug.Log($"Hit side: turn off collider");
                _platformCollider.enabled = false;
                return true;
            }
            else
                return false;

        }
        else
            return false;
    }



}
