﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPlatformSetup : MonoBehaviour
{
    [SerializeField] SpriteRenderer _platformSprite;

    [SerializeField] BoxCollider2D _oneWayTrigger;

    // Start is called before the first frame update
    void Start()
    {
        Vector2 newColSize = _platformSprite.size;
        newColSize.x += 2f;
        newColSize.y += 3f;

        _oneWayTrigger.size = newColSize;
        _oneWayTrigger.offset = new Vector2(0f, 0f);

    }
}
