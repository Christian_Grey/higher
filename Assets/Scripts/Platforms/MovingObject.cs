﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : Platform
{
    public float Speed
    {
        get
        {
            return _speed;
        }
        set
        {
            _speed = value;
        }
    }

    public Vector3 Velocity;

    public List<Vector3> Points = new List<Vector3>();
    [SerializeField] float _speed;

    int _currPointIndex = 0;
    Vector3 _target;

    public override void Init(Vector2 size)
    {
        base.Init(size);

        float rightPoint = GameController.I.CurrentLevel.Generator.HalfLevelWidth - size.x / 2f;
        Points.Clear();
        Points.Add(transform.position.With(x: rightPoint));
        Points.Add(transform.position.With(x: -rightPoint));

        _currPointIndex = UnityEngine.Random.Range(0, 2);
        _target = Points[_currPointIndex];
    }



    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameController.I.IsRunning == false) return;

        Vector3 remainingPath = _target - transform.position;
        Vector3 projVelocity = remainingPath.normalized * _speed;
        Velocity = projVelocity;

        if (remainingPath.sqrMagnitude < (projVelocity * Time.fixedDeltaTime).sqrMagnitude)
            transform.position = _target;
        else
            transform.position += projVelocity * Time.fixedDeltaTime;

        if (Vector3.Distance(_target, transform.position) < 0.1f)
            NextPoint();
    }

    private void NextPoint()
    {
        _currPointIndex++;
        _currPointIndex %= Points.Count;
        _target = Points[_currPointIndex];
    }
}
