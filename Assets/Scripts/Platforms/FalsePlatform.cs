﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FalsePlatform : Platform
{

    void OnTriggerStay2D(Collider2D incomingCol)
    {
        if (incomingCol.gameObject.layer == 10)
        {
            if (incomingCol.GetComponent<Rigidbody2D>().velocity.y < 0)
                Destroy(gameObject);
        }
    }
}
