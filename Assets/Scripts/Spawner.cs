﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject _objectToSpawn;
    [SerializeField] int _quantity;
    [SerializeField] float _initDelay;
    [SerializeField] float _delayBetweenSpawns;

    public void SpawnObjects()
    {
        if (_quantity == 0)
            return;

        Sequence seq = DOTween.Sequence();

        for (int i = 0; i < _quantity; i++)
        {
            seq.AppendCallback(SpawnObject);
            seq.AppendInterval(_delayBetweenSpawns);
        }
    }

    void SpawnObject()
    {
        Instantiate(_objectToSpawn, transform.position, Quaternion.identity);
    }
}
