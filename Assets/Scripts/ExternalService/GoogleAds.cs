using System;
using GoogleMobileAds.Api;

namespace ExternalService
{
    public class GoogleAds : PersistentHumbleSingleton<GoogleAds>
    {
        public static event Action UserEarnedRewardEvent;
        public static event Action RewardedClosedEvent;
        public static event Action InterstitialClosedEvent;

        private InterstitialAd _interstitial;
        private RewardedAd _rewardedAd;

        public void Start()
        {
            // Initialize the Google Mobile Ads SDK.
            MobileAds.Initialize(initStatus =>
            {
                RequestInterstitial();
                RequestRewarded();
            });

        }

        public bool IsRewardedReady()
        {
            return this._rewardedAd != null && this._rewardedAd.IsLoaded();
        }

        public bool IsInterstitialReady()
        {
            return this._interstitial != null && this._interstitial.IsLoaded();
        }

        public void RequestRewarded()
        {
            string adUnitId;
#if UNITY_ANDROID
            adUnitId = "ca-app-pub-2931094395701931/3135559332";
#elif UNITY_IPHONE
            adUnitId = "ca-app-pub-3940256099942544/1712485313";
#else
            adUnitId = "unexpected_platform";
#endif

            this._rewardedAd = new RewardedAd(adUnitId);

            // Called when an ad request has successfully loaded.
            //            this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
            // Called when an ad request failed to load.
            //            this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
            // Called when an ad is shown.
            //            this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
            // Called when an ad request failed to show.
            //            this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
            // Called when the user should be rewarded for interacting with the ad.
            this._rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
            // Called when the ad is closed.
            this._rewardedAd.OnAdClosed += OnRewardedClosed;

            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder()
                .AddTestDevice("3953FE906022A5D623C63B8A0129FD8B")
                .AddTestDevice("B76A6087AEEEDCB16B172CE7A09C40B8")
                .AddTestDevice("7845EED39B520BD9DAB336F2B249237B")
                .Build();
            // Load the rewarded ad with the request.
            this._rewardedAd.LoadAd(request);
        }

        private void HandleUserEarnedReward(object sender, Reward e)
        {
            UserEarnedReward();
        }


        public void RequestInterstitial()
        {
#if UNITY_ANDROID
            string adUnitId = "ca-app-pub-2931094395701931/9799711043";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
            string adUnitId = "unexpected_platform";
#endif

            // Initialize an InterstitialAd.
            this._interstitial = new InterstitialAd(adUnitId);
            // Create an empty ad request.
            var request = new AdRequest.Builder()
                .AddTestDevice("3953FE906022A5D623C63B8A0129FD8B")
                .AddTestDevice("B76A6087AEEEDCB16B172CE7A09C40B8")
                .AddTestDevice("7845EED39B520BD9DAB336F2B249237B")
                .Build();
            // Load the interstitial with the request.
            this._interstitial.LoadAd(request);
            this._interstitial.OnAdClosed += OnInterstitalClosed;
        }

        public void ShowInterstitial()
        {
            if (_interstitial != null && this._interstitial.IsLoaded())
            {
                this._interstitial.Show();
            }
            else
            {
                RequestInterstitial();
            }
        }

        public void ShowRewarded()
        {
            if (_rewardedAd != null && this._rewardedAd.IsLoaded())
            {
                UIController.I.PlayMessage("Connecting to server", 1.25f);
                this._rewardedAd.Show();
            }
            else
            {
                RequestRewarded();
            }
        }

        private void OnRewardedClosed(object sender, EventArgs args)
        {
            RequestRewarded();
            RewardedClosedEvent?.Invoke();
        }

        private void OnInterstitalClosed(object sender, EventArgs args)
        {
            RequestInterstitial();
            InterstitialClosedEvent?.Invoke();
        }

        private static void UserEarnedReward()
        {
            UserEarnedRewardEvent?.Invoke();
        }
    }
}