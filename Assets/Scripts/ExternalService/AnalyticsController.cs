﻿using Firebase.Analytics;

namespace ExternalService
{
    public class AnalyticsController : PersistentHumbleSingleton<AnalyticsController>
    {

        public void LogEvent(string name, params Parameter[] parameters)
        {
            if (!FirebaseLauncher.IsReady) return;

            //   Level: 25 | Num of tries: 5 | Dif: 0.84
            FirebaseAnalytics.LogEvent(name, parameters);
        }

        public void SetUserProperty(string propertyName, object value)
        {
            if (!FirebaseLauncher.IsReady) return;

            FirebaseAnalytics.SetUserProperty(propertyName, value.ToString());
        }
    }
}
