﻿using System;
using Firebase;
using UnityEngine;

public class FirebaseLauncher : MonoBehaviour
{
    private static FirebaseApp _app;

    public static FirebaseApp App
    {
        get { return _app; }
    }

    public static bool IsReady
    {
        get { return _app != null; }
    }

    void Start()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                _app = FirebaseApp.DefaultInstance;
                
                SendMessage("InitFirebaseMessaging");

                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                Debug.LogError(String.Format(
                    "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }
}