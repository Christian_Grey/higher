﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public enum CameraFollowType { HeightFollow, PlatformFollow, PlayerFollow }

    public CameraFollowType CameraType;
    public bool TrackX;
    public bool TrackSize;

    [SerializeField] float _cameraCeiling = 0.65f; // 0 - 1
    [SerializeField] Transform _target;
    [SerializeField] Rigidbody2D _targetRB;
    [SerializeField] float _heightFollowSensitivityY = 5f;
    [SerializeField] float _heightFollowSensitivityX = 5f;
    [SerializeField] float _platformFollowSensitivity = 5f;
    [Header("Change Size")]
    [SerializeField] float _sizeChangeSensitivity = 1f;
    [SerializeField] float _velChangeSpeed = 1f;
    [SerializeField] float _minOrthSize = 10f;
    [SerializeField] float _maxOrthSize = 20f;
    [Header("Player follow settings")]
    [SerializeField] float _velocityDivider = 5f;
    [SerializeField] float _playerFollowSensitivity = 5f;

    Vector3 _targetPos;
    float _targetVelMag;
    float _currVelMag;


    public void TeleportTo(Vector3 newPos)
    {
        if (TrackX == true)
            transform.position = newPos.With(z: -10f);
        else
            transform.position = newPos.With(x: 0f, z: -10f);
        _targetPos = transform.position;
    }


    void Start()
    {
        _targetPos = transform.position;
    }



    void Update()
    {
        if (CameraType == CameraFollowType.HeightFollow)
            HeightFollowUpdate();
        else if (CameraType == CameraFollowType.PlatformFollow)
            PlatformFollowUpdate();
        else
            PlayerFollowUpdate();
    }

    void PlayerFollowUpdate()
    {
        if (TrackX == false)
        {
            transform.position = Vector3.Lerp(transform.position,
                _target.transform.position.With(x: 0f, z: transform.position.z),
                 _playerFollowSensitivity * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position,
                _target.transform.position.With(z: transform.position.z),
                 _playerFollowSensitivity * Time.deltaTime);
        }
    }

    void PlatformFollowUpdate()
    {
        if (TrackX)
            transform.position = Vector3.Lerp(transform.position, _targetPos.With(x: _target.position.x) + Vector3.down * 5f, _platformFollowSensitivity * Time.deltaTime);
        else
            transform.position = Vector3.Lerp(transform.position, _targetPos + Vector3.down * 5f, _platformFollowSensitivity * Time.deltaTime);
    }

    void HeightFollowUpdate()
    {
        float viewportCeilingY = Camera.main.ViewportToWorldPoint(Vector3.up * _cameraCeiling).y;

        float delta = 0;
        if (_target.position.y > viewportCeilingY)
        {
            delta = _target.position.y - viewportCeilingY;
        }
        if (TrackX)
            _targetPos = new Vector3(_target.position.x, (transform.position.y + delta), transform.position.z);
        else
            _targetPos = new Vector3(0, (transform.position.y + delta), transform.position.z);

        if (TrackSize)
        {
            //_currVelMag = _currVelMag.StepTo(_targetRB.velocity.x.Abs(), _velChangeSpeed * Time.deltaTime);
            _currVelMag = _currVelMag.StepTo(_targetRB.velocity.x.Abs(), _velChangeSpeed * Time.deltaTime);
            float progress = _currVelMag.Remap(0f, 25f, 0f, 1f).Clamp01();
            Camera.main.orthographicSize = Mathf.Lerp(_minOrthSize, _maxOrthSize, progress);

            //float progress = _targetRB.velocity.x.Abs().Remap(0f, 25f, 0f, 1f).Clamp01();
            //float targetOrthSize = _minOrthSize.LerpTo(_maxOrthSize, progress);
            //Camera.main.orthographicSize = Camera.main.orthographicSize.LerpTo(targetOrthSize, _velChangeSpeed * Time.deltaTime);
        }

        var posX = transform.position.x.LerpTo(_targetPos.x, _heightFollowSensitivityX * Time.deltaTime);
        var posY = transform.position.y.LerpTo(_targetPos.y, _heightFollowSensitivityY * Time.deltaTime);
        transform.position = new Vector3(posX, posY, transform.position.z);
    }



    void OnPlayerLand(PlayerStartedJump info)
    {
        if (CameraType == CameraFollowType.PlatformFollow)
            _targetPos = info.FromCoords.With(x: 0f, z: -10f);
    }

    private void OnEnable()
    {
        EventSystem.AddListener<PlayerStartedJump>(OnPlayerLand);
    }

    private void OnDisable()
    {
        EventSystem.DeleteListener<PlayerStartedJump>(OnPlayerLand);
    }
}
