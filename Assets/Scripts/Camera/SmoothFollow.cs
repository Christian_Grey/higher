﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollow : VirtCamera
{
    public Transform Target;
    public Vector3 CameraOffset;
    public float Smoothing;
    public float SmoothTime = 1f;
    public bool LockCameraX;

    Vector3 _targetPos;
    Vector3 _velocity;

    // Start is called before the first frame update
    void Start()
    {
        _targetPos = Target.position;
    }

    public override void TeleportTo(Vector3 newPos)
    {
        transform.position = newPos.With(z: -10f);
        _targetPos = Target.position;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        Vector3 newPos = Vector3.SmoothDamp(transform.position, _targetPos + CameraOffset, ref _velocity, SmoothTime, 100f, Time.deltaTime);

        if (LockCameraX)
            newPos.x = 0;

        transform.position = newPos;
        */


    }

    private void FixedUpdate()
    {
        _targetPos = Target.position;

        Vector3 newPos = Vector3.SmoothDamp(transform.position, _targetPos + CameraOffset, ref _velocity, SmoothTime, 100f, Time.fixedDeltaTime);

        if (LockCameraX)
            newPos.x = 0;

        transform.position = newPos;


        //transform.position = Vector3.Lerp(transform.position, Target.position, Time.fixedDeltaTime * Smoothing).With(z: -10f);
    }
}
