﻿using UnityEngine;
using System.Collections;

public class CameraShaker : MonoBehaviour
{

    [Header("Shake")]
    public float TraumaDecreaseRate = 1f;
    public float ShakeRotation = 5f;
    public float ShakeTranslation = 1f;
    public float ShakeNoiseScale = 1f;
    //privates
    float _trauma = 0f;
    Transform _cachedTransform;
    Vector3 _cameraPosPreviousToShake;
    Quaternion _cameraRotPreviousToShake;

    Vector3 _lastFrameShakePos;
    Quaternion _lastFrameShakeRot;

    // Use this for initialization

    int _seed;

    void Awake()
    {
        _cachedTransform = GetComponent<Transform>();
        _cameraPosPreviousToShake = transform.position;
        _cameraRotPreviousToShake = transform.rotation;
        _seed = Random.Range(0, 100000);
    }

    public void AddTrauma(float s)
    {
        _trauma = Mathf.Clamp01(_trauma + s);
    }



    /// <summary>
    /// Should use Late Update because all translations should happen earlier in update or fixedupdate
    /// </summary>
    void LateUpdate()
    {
        //if (Input.GetKeyDown(KeyCode.Q))
        //    AddTrauma(1f);

        _cameraPosPreviousToShake = _cachedTransform.position;
        _cameraRotPreviousToShake = _cachedTransform.rotation;

        Shake();
    }


    void Shake()
    {
        //Decrease trauma
        _trauma = Mathf.Clamp01(_trauma - Time.deltaTime * TraumaDecreaseRate);

        _cachedTransform.position -= _lastFrameShakePos;
        _cachedTransform.rotation *= Quaternion.Inverse(_lastFrameShakeRot);

        _lastFrameShakePos = new Vector3(GetPerlinNoise(Time.time), GetPerlinNoise(Time.time + 10f), 0f) * ShakeTranslation * _trauma * _trauma;
        _lastFrameShakeRot = Quaternion.AngleAxis(GetPerlinNoise(Time.time + 33f) * ShakeRotation * _trauma * _trauma, Vector3.forward);

        _cachedTransform.position = _cachedTransform.position + _lastFrameShakePos;
        _cachedTransform.rotation = _cachedTransform.rotation * _lastFrameShakeRot;
    }


    float GetPerlinNoise(float coordinate)
    {
        return (Mathf.PerlinNoise((float)_seed + coordinate * ShakeNoiseScale, (float)_seed + coordinate * ShakeNoiseScale) * 2f) - 1f;
    }

}
