﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScaler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        float orthoSize = (6.7f * 2f) * Screen.height / Screen.width * 0.5f;
        Camera.main.orthographicSize = orthoSize;
    }

}
