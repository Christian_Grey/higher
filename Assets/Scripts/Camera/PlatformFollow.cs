﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformFollow : VirtCamera
{
    [SerializeField] Vector3 _targetPos;
    [SerializeField] float _followSensitivity = 5f;
    [SerializeField] Vector3 _offset;

    public override void TeleportTo(Vector3 newPos)
    {
        transform.position = newPos.With(z: -10f);
        _targetPos = transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, _targetPos + _offset, _followSensitivity * Time.deltaTime);
    }

    void OnPlayerLand(PlayerStartedJump info)
    {
        _targetPos = info.FromCoords.With(x: 0f, z: -10f);
    }

    private void OnEnable()
    {
        EventSystem.AddListener<PlayerStartedJump>(OnPlayerLand);
    }

    private void OnDisable()
    {
        EventSystem.DeleteListener<PlayerStartedJump>(OnPlayerLand);
    }

}
