﻿using System.Collections;
using System.Collections.Generic;
using ExternalService;
using Firebase.Analytics;
using UnityEngine;

public class GameDataHolder : PersistentHumbleSingleton<GameDataHolder>
{
    public GameDataSave Save;
    private string GAME_DATA_SAVE_PATH => Application.persistentDataPath + "/Save.json";

    public void SaveGameData()
    {
        Debug.Log($"Saving game data");
        LevelLoader.SaveToFile(Save, GAME_DATA_SAVE_PATH);
    }

    public void LoadGameData()
    {
        Save = LevelLoader.LoadFromFile<GameDataSave>(GAME_DATA_SAVE_PATH);
        if (Save == null)
        {
            FreshSave();
        }
    }

    public void FreshSave()
    {
        Save = new GameDataSave();
        Save.Seed = Random.Range(0, 100000);
        AnalyticsController.I.SetUserProperty("seed", Save.Seed);
        CreateBonusData();
        InitStartingSkins();
        SaveGameData();
    }

    public override void Init()
    {
        base.Init();
        LoadGameData();
    }

    public void IncreaseBonusStatLevel(string bonusName, string statName)
    {
        AnalyticsController.I.LogEvent("increased_stat", new Parameter("bonus_name", bonusName), new Parameter("stat_name", statName));
        Save.BonusesData.GetByName(bonusName, (x) => x.Name).Stats.GetByName(statName, (s) => s.Name).Level++;
        SaveGameData();
    }

    public void AddMoney(int amount)
    {
        Debug.Log($"Money: {Save.Money}, Add: {amount}. All: {Save.Money + amount}");
        Save.Money += amount;
        AnalyticsController.I.LogEvent("earned_virtual_currency", new Parameter("amount", amount));
        AnalyticsController.I.SetUserProperty("money", Save.Money);
        EventSystem.FireEvent<MoneyChanged>(new MoneyChanged(Save.Money, false));
        SaveGameData();
    }

    public void SpendMoney(int amount, System.Action OnSuccess)
    {
        Debug.Log($"Trying to spend money");
        if (HaveEnoughMoney(amount))
        {
            Save.Money -= amount;
            AnalyticsController.I.LogEvent("spent_virtual_currency", new Parameter("amount", amount));
            AnalyticsController.I.SetUserProperty("money", Save.Money);
            EventSystem.FireEvent<MoneyChanged>(new MoneyChanged(Save.Money, false));
            OnSuccess();
            SaveGameData();
        }
        else
        {
            UIController.I.HotEnoughMoneyPopup();
        }
    }

    public void UnlockSkin(string skinName)
    {
        AnalyticsController.I.LogEvent("skin_unlocked", new Parameter("skin_name", skinName));
        Save.UnlockedSkins.Add(skinName);
        SaveGameData();
    }

    public bool HaveEnoughMoney(int amount)
    {
        if (Save.Money >= amount)
            return true;
        else
            return false;
    }

    void CreateBonusData()
    {
        Debug.Log($"Init game data holder");

        foreach (var bonusGO in Prefabs.I.Bonuses)
        {
            var bonus = bonusGO.GetComponent<Bonus>();
            var bonusData = new BonusData();

            bonusData.Name = bonusGO.name;
            foreach (var stat in bonus.Stats)
            {
                bonusData.Stats.Add(new StatData() { Name = stat.Name, Level = 0 });
            }
            Save.BonusesData.Add(bonusData);
        }
    }

    void InitStartingSkins()
    {
        Debug.Log($"Writing init skins to save");
        Save.CurrentSkin = "Char1";
        Save.CurrentLandingFX = "Rocks1";
        Save.CurrentTrail = "Trail1";

        UnlockSkin("Char1");
        UnlockSkin("Rocks1");
        UnlockSkin("Trail1");
    }
}

[System.Serializable]
public class GameDataSave
{
    public int Money = 0;
    public int CurrentLevel = 1;
    public float AvgMoneyPerLevel = 0;
    public int Seed;
    public int CurrentLevelAttempt = 0;
    public bool SoundEnabled = true;
    public List<BonusData> BonusesData = new List<BonusData>();
    public HashSet<string> UnlockedSkins = new HashSet<string>();
    public Dictionary<string, int> LevelsToNumberOfTries = new Dictionary<string, int>();
    public string CurrentSkin;
    public string CurrentLandingFX;
    public string CurrentTrail;
    public int NextInterstitialAd = 8;
    public int NextX10Ad = 4;
}

[System.Serializable]
public class BonusData
{
    public string Name;
    public List<StatData> Stats = new List<StatData>();

}

[System.Serializable]
public class StatData
{
    public string Name;
    public int Level;
}