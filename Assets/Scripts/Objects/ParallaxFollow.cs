﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxFollow : MonoBehaviour
{
    [SerializeField] float _parallaxFactor;

    Transform _mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        _mainCamera = Camera.main.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {

        transform.position = (_mainCamera.position * _parallaxFactor).With(z: transform.position.z);


    }
}
