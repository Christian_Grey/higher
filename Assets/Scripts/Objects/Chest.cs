﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    [SerializeField] Collectable _coinsToSpawn;
    [SerializeField] int _quantity;
    [SerializeField] float _initDelay;
    [SerializeField] float _delayBetweenSpawns;

    bool _activated;

    void OnEnable()
    {
        _activated = false;
    }

    public void SpawnCoins()
    {
        if (_quantity == 0)
            return;

        Sequence seq = DOTween.Sequence();

        for (int i = 0; i < _quantity; i++)
        {
            seq.AppendCallback(() => SpawnCoin(transform.position));
            seq.AppendInterval(_delayBetweenSpawns);
        }
        seq.AppendCallback(() => Prime31Pool.I.Despawn(gameObject));
    }

    void SpawnCoin(Vector3 spawnPos)
    {
        Collectable c = Instantiate(_coinsToSpawn, spawnPos, Quaternion.identity);
        c.Launch(GameController.I.Player.transform);
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if (_activated) return;

        if (collision.gameObject.layer == 10)
        {
            SpawnCoins();
            _activated = true;
            gameObject.SetActive(false);
            //Destroy(gameObject);

        }
    }

}
