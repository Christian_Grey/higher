﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestructionArea : MonoBehaviour
{
    Collider2D _col;

    void Start()
    {
        _col = GetComponent<Collider2D>();
    }


    void OnTriggerStay2D(Collider2D collision)
    {
        string objName = collision.gameObject.name.Replace("(Clone)", "");
        objName = objName.Replace(" Variant", "");

        if (Prefabs.DestructibleByLava.Contains(objName))
        {
            if (_col.bounds.ContainBounds(collision.bounds))
                Prime31Pool.I.Despawn(collision.gameObject);
        }
    }

}
