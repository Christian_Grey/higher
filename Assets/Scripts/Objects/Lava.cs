﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    public float Top
    {
        get
        { return _collider.bounds.max.y; }
    }

    [SerializeField] Color _slowLavaColor;
    [SerializeField] Color _fastLavaColor;

    [SerializeField] float _headstartTime = 2f;
    [SerializeField] float _slowLavaMinSpeed = 2f;
    [SerializeField] float _slowLavaMaxSpeed = 5.3f;
    [SerializeField] float _fastLavaMinSpeed = 5f;
    [SerializeField] float _fastLavaMaxSpeed = 15f;
    [SerializeField] float _speedChangeSensitivity = 3f;

    [SerializeField] SpriteRenderer _lavaTop;

    BoxCollider2D _collider;
    SpriteRenderer _lavaSprite;

    float _targetSpeed;
    float _currSpeed;
    bool _slowLava;
    float _headstartStartTime;
    bool _initHeadstart = true;

    public void Init(bool slowLava, float levelWidth)
    {
        float lavaWidth = levelWidth + Variables.I.WallThickness * 2 + 2f;

        _slowLava = slowLava;
        _lavaSprite = GetComponent<SpriteRenderer>();
        _lavaSprite.size = _lavaSprite.size.With(x: lavaWidth);
        _lavaSprite.color = _slowLava ? _slowLavaColor : _fastLavaColor;

        _lavaTop.size = _lavaTop.size.With(x: lavaWidth);

        var partSystems = GetComponentsInChildren<ParticleSystem>();
        foreach (var pSys in partSystems)
        {
            var shape = pSys.shape;
            shape.scale = shape.scale.With(x: levelWidth);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        //Vector3 lowerCamPoint = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0f, 0f)).With(z: 0f);
        //transform.position = lowerCamPoint + Vector3.down * _collider.bounds.size.y / 2f + Vector3.down * 5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.I.IsRunning == false) return;

        if (_initHeadstart)
        {
            _headstartStartTime = Time.time;
            _initHeadstart = false;
        }

        if (Time.time - _headstartStartTime < _headstartTime) return;

        float playerY = GameController.I.Player.transform.position.y;
        float lavaTop = _collider.bounds.max.y;
        float dist = playerY - lavaTop;
        float multiplier = ((GameController.I.Difficulty - 0.5f) / 3f) + 1f;

        if (_slowLava)
            _currSpeed = dist.RemapClamp(10, 30, _slowLavaMinSpeed * multiplier, _slowLavaMaxSpeed * multiplier);
        else
            _currSpeed = dist.RemapClamp(10, 30, _fastLavaMinSpeed * multiplier, _fastLavaMaxSpeed * multiplier);

        if (dist > 50f)
            _currSpeed = 25f;
        transform.position += Vector3.up * _currSpeed * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            EventSystem.FireEvent<PlayerHasDied>(new PlayerHasDied());
            return;
        }

        //string objName = collision.gameObject.name.Replace("(Clone)", "");
        //objName = objName.Replace(" Variant", "");

        //if (Prefabs.DestructibleByLava.Contains(objName))
        //{
        //    Prime31Pool.I.Despawn(collision.gameObject);
        //}


    }

}
