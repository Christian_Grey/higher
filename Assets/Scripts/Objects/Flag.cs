﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D collision)
    {
        // if object is not player
        if (collision.gameObject.layer != 10) return;

        EventSystem.FireEvent<FlagReached>(new FlagReached());
    }
}
