﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallFollow : MonoBehaviour
{
    Transform _target;

    Vector3 _offset;

    void Start()
    {
        _target = Camera.main.transform;
        _offset = transform.position - _target.position;
    }

    void Update()
    {
        transform.position = _target.position.With(x: 0) + _offset;
    }


    private void OnDestroy()
    {
        Debug.Log("Wall destroyed");
    }
}
