﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

/// <summary>
/// This class works via FixedUpdate. You supposed to call it in FixedUpdate in your main class.
/// </summary>
public class Collectable : MonoBehaviour
{
    public float weight;
    [SerializeField] AnimationCurve _veloctiyToPlayerCurve;
    [SerializeField] float _disableColliderForSeconds = 0.3f;
    [SerializeField] float _timeToReachTarget = 1f;
    [SerializeField] float _toTargetSpeed = 10f;
    [SerializeField] Vector2 _launchVelocity;
    [SerializeField] float _spread = 0.1f;

    Collider2D _col;
    Rigidbody2D _rigidb;
    Transform _target;
    float _timeAlive;
    bool _firstContact = true;

    private void OnEnable()
    {
        _rigidb = GetComponent<Rigidbody2D>();
        _col = GetComponent<Collider2D>();
    }

    public virtual void Launch()
    {
        Collect();
        AudioController.I.Play(AudioController.I.CollectableSound, 0.25f);
        _firstContact = false;
        _col.enabled = false;

        Invoke("EnableCollider", _disableColliderForSeconds);

        _timeAlive = 0f;
        _launchVelocity = Quaternion.AngleAxis(0f.Range(_spread), Vector3.forward) * _launchVelocity;
        _rigidb.velocity = _launchVelocity;
    }

    public void Launch(Transform target)
    {
        _target = target;
        Launch();
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        if (_target == null) return;

        Vector2 gravity = Vector3.down * 55f * Time.fixedDeltaTime;
        Vector2 velocityWithGravity = _rigidb.velocity + gravity;
        Vector2 velocityToTarget = ((Vector2)_target.position - _rigidb.position).normalized * _toTargetSpeed;

        float progressAlongCurve = _timeAlive.Remap(0, _timeToReachTarget, 0, 1f);
        float progress = _veloctiyToPlayerCurve.Evaluate(progressAlongCurve).Clamp01();
        _rigidb.velocity = Vector2.Lerp(velocityWithGravity, velocityToTarget, progress);
        _timeAlive += Time.fixedDeltaTime;
        weight = progress;


    }

    private void EnableCollider()
    {
        _col.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            if (_firstContact == true)
            {
                _target = collision.transform;
                Launch();
            }
            else
            {
                //Collect();
                Destroy(gameObject);
            }
        }
    }

    protected virtual void Collect()
    {
        EventSystem.FireEvent<CollectableCollected>(new CollectableCollected());

        //Destroy(gameObject);
    }
}
