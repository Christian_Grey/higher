﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Collectable
{
 

    protected override void Collect()
    {
        GameController.I.CurrentLevel.AddLevelMoney(1);
        //Destroy(gameObject);
    }
}
