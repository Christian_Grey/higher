﻿using UnityEngine;
using System.Collections;

public class TrailShadow : Trail
{
    void Awake()
    {
        _trailRenderer = GetComponentInChildren<TrailRenderer>();
        _partictles = GetComponentsInChildren<ParticleSystem>();

    }
}
