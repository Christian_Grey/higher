﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour
{
    protected TrailRenderer _trailRenderer;
    protected ParticleSystem[] _partictles;

    void Awake()
    {
        _trailRenderer = GetComponentInChildren<TrailRenderer>();
        _partictles = GetComponentsInChildren<ParticleSystem>();
    }

    public void Play()
    {
        if (_partictles.Length != 0)
        {
            foreach (var partSystem in _partictles)
            {
                partSystem.Stop();
                partSystem.Play();
            }
        }

        if (_trailRenderer != null)
            _trailRenderer.gameObject.SetActive(true);
    }

    public void Stop()
    {
        if (_partictles.Length != 0)
        {
            foreach (var partSystem in _partictles)
            {
                partSystem.Stop();
            }
        }

        if (_trailRenderer != null)
        {
            _trailRenderer.Clear();
            _trailRenderer.gameObject.SetActive(false);
        }
    }
}
