﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusFlight : Bonus
{

    protected override void Enable()
    {
        EventSystem.FireEvent<BonusFlightEvent>(new BonusFlightEvent(true));
    }

    protected override void Disable()
    {
        EventSystem.FireEvent<BonusFlightEvent>(new BonusFlightEvent(false));
    }
}
