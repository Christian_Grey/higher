﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusCollectable : MonoBehaviour
{
    [SerializeField] GameObject _bonusToSpawn;

    public void Init(GameObject bonusToSpawn)
    {
        _bonusToSpawn = bonusToSpawn;
        var bonus = _bonusToSpawn.GetComponent<Bonus>().BonusImage.sprite;

        GetComponent<SpriteRenderer>().sprite = bonus;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // if object is not player
        if (collision.gameObject.layer != 10) return;

        AudioController.I.Play(AudioController.I.BonusPickup, 0.6f);
        Instantiate(_bonusToSpawn);
        Destroy(gameObject);
    }
}
