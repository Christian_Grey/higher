﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusPlatformStretch : Bonus
{
    protected override void Enable()
    {
        BonusStat stretchAmount = Stats.GetByName("Stretch Amount", x => x.Name);
        Variables.I.AdditionalPlatformWidth = stretchAmount.Value + stretchAmount.UpgradeStep * GetStatLevel("Stretch Amount");
    }

    protected override void Disable()
    {
        Variables.I.AdditionalPlatformWidth = 0f;
    }
}

