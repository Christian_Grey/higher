﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusHigherJump : Bonus
{
    protected override void Enable()
    {
        BonusStat jumpIncrease = Stats.GetByName("Jump Height", x => x.Name);
        GameController.I.Player.SetJumpMultiplier(jumpIncrease.Value + jumpIncrease.UpgradeStep * GetStatLevel("Jump Height"));
    }

    protected override void Disable()
    {
        GameController.I.Player.SetJumpMultiplier(1f);
    }

}
