﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Bonus : MonoBehaviour
{
    public string Description;
    public BonusStat[] Stats;

    public Image BonusImage;
    [SerializeField] protected Text _timerText;

    float _targetTime;
    float _timer = 100f;

    //protected BonusUpgradeArgs _args;
    bool _usedToAddTime;

    void Update()
    {
        if (GameController.I.IsRunning == false)
            return;

        _timer = _timer - Time.deltaTime;
        float progress = (_timer / _targetTime);
        BonusImage.fillAmount = progress;
        _timerText.text = _timer.ToString("0.0");


        if (_timer <= 0)
            Destroy(gameObject);
    }

    void OnEnable()
    {
        //GameDataHolder.I.Save.BonusArgs.TryGetValue(GetType().Name, out _args);
        //if (_args == null)
        //    _args = new BonusUpgradeArgs();

        if (GameController.I.Bonuses.ContainsKey(GetType().Name))
        {
            GameController.I.Bonuses[GetType().Name]._timer += GetDuration();
            GameController.I.Bonuses[GetType().Name]._targetTime += GetDuration();
            _usedToAddTime = true;
            Destroy(gameObject);
            return;
        }

        GameController.I.Bonuses.Add(GetType().Name, this);

        transform.SetParent(GUIController.I.BonusGrid.transform);
        _targetTime = GetDuration();
        _timer = _targetTime;

        Enable();
    }


    void OnDisable()
    {
        if (_usedToAddTime)
            return;

        GameController.I.Bonuses.Remove(GetType().Name);

        Disable();
    }

    protected virtual float GetDuration()
    {
        BonusStat duration = Stats.GetByName("Duration", x => x.Name);
        return duration.Value + duration.UpgradeStep * GetStatLevel("Duration");
    }

    protected virtual int GetStatLevel(string statName)
    {
        return GameDataHolder.I.Save.BonusesData.GetByName(GetType().Name, n => n.Name).Stats.GetByName(statName, x => x.Name).Level;
    }

    protected virtual void Enable()
    {
    }

    protected virtual void Disable()
    {
    }

}
[System.Serializable]
public class BonusStat
{
    public string Name;
    public float Value;
    public float UpgradeStep;
    public int Cost;
    public int CostStep;
    public int Level;
}

//public class BonusUpgradeArgs
//{
//    public float Duration;
//    public float Strength = 0f;
//}

