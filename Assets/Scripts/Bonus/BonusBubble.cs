﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusBubble : Bonus
{
    GameObject _bubblePlatform;

    protected override void Enable()
    {
        EventSystem.AddListener<PlayerStartedJump>(OnPlayerJump);
        float yPos = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).y;

        float platformOffset = Prefabs.I.BubblePlatform.GetComponent<SpriteRenderer>().size.y / 2f;
        _bubblePlatform = Instantiate(Prefabs.I.BubblePlatform, Vector3.zero.With(y: yPos + platformOffset), Quaternion.identity, Camera.main.transform);
        Debug.Log($"bubble pl width: {GameController.I.CurrentLevel.Generator.HalfLevelWidth * 2f}");
        _bubblePlatform.GetComponent<Platform>().Init(GameController.I.CurrentLevel.Generator.HalfLevelWidth * 2f);
    }


    protected override void Disable()
    {
        EventSystem.DeleteListener<PlayerStartedJump>(OnPlayerJump);
        Destroy(_bubblePlatform);
    }


    void OnPlayerJump(PlayerStartedJump info)
    {
        if (info.FromObject == _bubblePlatform)
            Destroy(gameObject);
    }


}
