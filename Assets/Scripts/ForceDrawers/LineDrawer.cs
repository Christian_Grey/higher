﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : ForceDrawer
{
    [SerializeField] float _maxPullbackInUnits = 4f;
    [SerializeField] Texture _lowForceSprite;
    [SerializeField] Texture _midForceSprite;
    [SerializeField] Texture _highForceSprite;
    [SerializeField] Sprite Mark;
    [SerializeField] Gradient _lowToHighGradient;
    [SerializeField] LineRenderer _lineRenderer;
    [SerializeField] Transform _player;
    [SerializeField] bool _reverseTail;

    Vector3 _mouseDownPos, _mouseHoldPos, _mouseClampedHoldPos;
    float _force;
    bool _shouldDraw;



    public override void ReceiveForce(float force)
    {
        _force = force;
    }

    public override void ReceiveInputPos(Vector2 downPos, Vector2 holdPos, Vector2 clampedHoldPos)
    {
        _mouseDownPos = Camera.main.ScreenToWorldPoint(downPos);
        _mouseHoldPos = Camera.main.ScreenToWorldPoint(holdPos);
        _mouseClampedHoldPos = Camera.main.ScreenToWorldPoint(clampedHoldPos);
        _shouldDraw = true;
    }

    public override void EraseLine()
    {
        _shouldDraw = false;
        _lineRenderer.SetPosition(0, new Vector2(1000, 1000));
        _lineRenderer.SetPosition(1, new Vector2(1000, 1000));
    }

    // Update is called once per frame
    void Update()
    {
        if (!_shouldDraw) return;

        Vector3 forceDir = (_mouseHoldPos - _mouseDownPos).normalized;
        if (_reverseTail) forceDir *= -1f;
        Vector3 startPos = (_player.position + forceDir * 0.5f).With(z: -1f);
        Vector3 endPos = (startPos + (forceDir * _force * _maxPullbackInUnits)).With(z: -1f);

        if (_force < 0.50f)
            _lineRenderer.sharedMaterial.mainTexture = _lowForceSprite;
        else if (_force < 0.75f)
            _lineRenderer.sharedMaterial.mainTexture = _midForceSprite;
        else
            _lineRenderer.sharedMaterial.mainTexture = _highForceSprite;

        // _lineRenderer.sharedMaterial.color = _lowToHighGradient.Evaluate(_force);

        _lineRenderer.SetPosition(0, startPos);
        _lineRenderer.SetPosition(1, endPos);
    }


}
