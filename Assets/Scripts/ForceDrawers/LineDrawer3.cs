﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer3 : ForceDrawer
{
    [SerializeField] float _maxPullbackInUnits = 4f;
    [SerializeField] Texture _lowForceSprite;
    [SerializeField] Texture _midForceSprite;
    [SerializeField] Texture _highForceSprite;
    [SerializeField] Texture _lowForceArrowSprite;
    [SerializeField] Texture _midForceArrowSprite;
    [SerializeField] Texture _highForceArrowSprite;
    [SerializeField] Transform _touchIndicator1, _touchIndicator2;
    [SerializeField] LineRenderer _lineRendererBetweenFingers;
    [SerializeField] LineRenderer _lineRendererArrow;
    [SerializeField] Transform _player;

    Vector3 _mouseDownPos, _mouseHoldPos, _mouseClampedHoldPos;
    float _force;
    bool _shouldDraw;

    public override void ReceiveForce(float force)
    {
        _force = force;
    }

    public override void ReceiveInputPos(Vector2 downPos, Vector2 holdPos, Vector2 clampedHoldPos)
    {
        _mouseDownPos = Camera.main.ScreenToWorldPoint(downPos);
        _mouseHoldPos = Camera.main.ScreenToWorldPoint(holdPos);
        _mouseClampedHoldPos = Camera.main.ScreenToWorldPoint(clampedHoldPos);
        _shouldDraw = true;
    }

    public override void EraseLine()
    {
        _shouldDraw = false;
        _lineRendererBetweenFingers.SetPosition(0, new Vector2(1000, 1000));
        _lineRendererBetweenFingers.SetPosition(1, new Vector2(1000, 1000));
        _touchIndicator1.position = new Vector2(1000, 1000);
        _touchIndicator2.position = new Vector2(1000, 1000);
        _lineRendererArrow.SetPosition(0, new Vector2(1000, 1000));
        _lineRendererArrow.SetPosition(1, new Vector2(1000, 1000));
    }

    // Update is called once per frame
    void Update()
    {
        if (!_shouldDraw) return;

        // Draw between fingers
        Vector3 forceDir = (_mouseHoldPos - _mouseDownPos).normalized;
        Vector3 startPos = _mouseDownPos.With(z: -1f);
        Vector3 endPos = (startPos + (forceDir * _force * _maxPullbackInUnits)).With(z: -1f);

        if (_force < 0.50f)
            _lineRendererBetweenFingers.material.mainTexture = _lowForceSprite;
        else if (_force < 0.75f)
            _lineRendererBetweenFingers.material.mainTexture = _midForceSprite;
        else
            _lineRendererBetweenFingers.material.mainTexture = _highForceSprite;


        _lineRendererBetweenFingers.SetPosition(0, _mouseDownPos.With(z: -1f));
        _lineRendererBetweenFingers.SetPosition(1, _mouseClampedHoldPos.With(z: -1f));

        _touchIndicator1.position = _mouseDownPos.With(z: -2f);
        _touchIndicator2.position = _mouseClampedHoldPos.With(z: -2f);

        //Draw arrow

        forceDir = (_mouseDownPos - _mouseHoldPos).normalized;
        startPos = (_player.position + forceDir * 0.5f).With(z: -1f);
        endPos = (startPos + (forceDir * _force * _maxPullbackInUnits)).With(z: -1f);

        if (_force < 0.50f)
            _lineRendererArrow.material.mainTexture = _lowForceArrowSprite;
        else if (_force < 0.75f)
            _lineRendererArrow.material.mainTexture = _midForceArrowSprite;
        else
            _lineRendererArrow.material.mainTexture = _highForceArrowSprite;

        // _lineRenderer.sharedMaterial.color = _lowToHighGradient.Evaluate(_force);

        _lineRendererArrow.SetPosition(0, startPos);
        _lineRendererArrow.SetPosition(1, endPos);
    }


}
