﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ForceDrawer : MonoBehaviour
{
    public abstract void ReceiveForce(float force);
    public abstract void ReceiveInputPos(Vector2 downPos, Vector2 holdPos, Vector2 clampedHoldPos);
    public abstract void EraseLine();
}
