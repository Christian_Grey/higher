﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class LevelGenerator
{
    public float LevelHeight => _levelHeight;
    public float CurrentLevelHeight => _currentLevelHeight;
    public float LastPlatformHeight => _lastPlatformHeight;
    public float HalfLevelWidth => _levelHalfWidth;
    public bool IsInfinite;

    protected GeneratorSettings _settings;
    protected List<LevelGoal> _goals;
    protected Transform _parent;
    protected float _levelHalfWidth;
    protected float _levelHeight;
    protected float _currentLevelHeight;
    protected bool _spawnCollectables;
    protected bool _spawnGold;
    protected PoolPseudoRandom<PlatformType> _platformPool;

    protected Platform _lastPlatform;
    protected float _lastPlatformHeight;
    protected float _lastPlatformWidth;
    protected float _lastWallHeight;
    protected float _lastCollectableY;
    protected float _lastCoinY;
    protected float _lastBonusY;
    protected TextMeshPro _levelText;

    public virtual void GenerateLevel(GeneratorSettings settings, Transform parent, List<LevelGoal> goals)
    {
        //_settings = ScriptableObject.Instantiate(settings);

        _lastCollectableY = Random.Range(Variables.I.CollectableSpreadDistance, 0);
        _lastCoinY = Random.Range(Variables.I.CoinSpreadDistance, 0);
        _lastBonusY = Random.Range(Variables.I.BonusSpreadDistance, 0);

        LevelLoader.clear();
        _settings = settings;
        _currentLevelHeight = 10f;
        _lastWallHeight = -10f;
        _parent = parent;
        _levelHeight = _settings.LevelHeight + (_settings.LevelHeight * GameController.I.Difficulty);
        _goals = goals;
        _levelHalfWidth = Variables.I.LEVEL_HALF_WIDTH * _settings.LevelWidth;
        _spawnCollectables = goals.Any(x => x.GoalType == LevelGoalType.Collect);
        _platformPool = new PoolPseudoRandom<PlatformType>(PlatformWeight.ToDictionary(_settings.PlatformWeights));

        // INFINITE level if NOT reach the flag
        if (_goals.Any(g => g.GoalType == LevelGoalType.ReachTheFlag) == false)
            IsInfinite = true;

        Checkpoint.Point = new Vector3(0, 0, 0);

        //Make starting platform
        GenerateFirstPlatform();
        TryGenerateFlag();

        //Intermediate platforms
        GenerateIntermediatePlatforms();

        //Set up Checkpoint
        if (_settings.Direction == DirectionType.Down)
        {
            Checkpoint.Point = _lastPlatform.transform.position + Vector3.up;
        }

        SetUpCamera();
        GameController.I.Player.DescentMode = false;
        GameController.I.RepositionPlayer();
        GenerateLava();
    }

    public void RevealGoals()
    {
        if (_goals.Any(x => x.GoalType == LevelGoalType.Collect))
        {
            LevelGoal goal = _goals.First(x => x.GoalType == LevelGoalType.Collect);
            goal.GetIntoPlace();
        }

        TryGenerateLevelText();
    }

    public virtual void GenerateIntermediatePlatforms(float addHeight = 0f)
    {
        _currentLevelHeight = _currentLevelHeight + addHeight;

        //if (IsInfinite)
        //    _currentLevelHeight = _currentLevelHeight + addHeight;
        //else
        //    _currentLevelHeight = Mathf.Clamp(_currentLevelHeight + addHeight, 0, _levelHeight);


        while (_lastPlatformHeight < _currentLevelHeight)
        {
            SetPlatformHeight();
            if (_lastPlatformHeight > _levelHeight - 2.5f && IsInfinite == false)
            {
                _lastPlatformHeight = _levelHeight;
                break;
            }

            float width = GetPlatformWidth();
            float xPos = GetXPos(width);

            TryGenerateHeightMarker();

            Vector3 platformPos = new Vector3(xPos, _lastPlatformHeight);

            PlatformType platformType = _platformPool.GetRandom();
            _platformPool.DecreaseChance(platformType);
            //var platformName = _settings.PlatformWeights.Random(x => x.Weight).Type.ToString();
            GameObject platformToSpawn = Prefabs.DestructibleObjects[platformType.ToString()];


            _lastPlatform = Prime31Pool.I.Spawn(platformToSpawn, platformPos, Quaternion.identity, _parent).GetComponent<Platform>();
            _lastPlatform.Init(width);
            _lastPlatformWidth = width;
            //SetupMoving
            TrySpawnSomething(platformPos);
        }


        while (_lastWallHeight < _currentLevelHeight + 10)
        {

            for (int i = 0; i < 2; i++)
            {
                GameObject wallGO = GameObject.Instantiate(Prefabs.I.Wall, Vector3.zero, Quaternion.identity, _parent) as GameObject;
                var wall = wallGO.GetComponent<Platform>();
                wall.Init(new Vector2(Variables.I.WallThickness, Variables.I.WallSegmentHeight));
                //var wallSprite = wallGO.GetComponent<SpriteRenderer>();
                //wallSprite.size = new Vector2(10, 40f);
                Vector3 wallPos = Vector3.zero.With(y: _lastWallHeight);

                if (i == 0)
                    wallPos = wallPos.With(x: _levelHalfWidth + Variables.I.WallThickness / 2f);
                else
                    wallPos = wallPos.With(x: -_levelHalfWidth - Variables.I.WallThickness / 2f);

                wallGO.transform.position = wallPos;
            }

            _lastWallHeight += Variables.I.WallSegmentHeight;
        }

    }

    private void GenerateFirstPlatform()
    {

        _lastPlatform = Prime31Pool.I.Spawn(Prefabs.I.RegularPlatform, new Vector3(0, -1.5f), Quaternion.identity, _parent).GetComponent<Platform>();
        _lastPlatform.Init(new Vector2(Mathf.Abs(_levelHalfWidth) + Mathf.Abs(-_levelHalfWidth), 3f));
    }

    protected virtual void TryGenerateLevelText()
    {
        if (_levelText != null) return;

        var textGO = GameObject.Instantiate(Prefabs.I.LevelText, Vector3.down * 1.5f, Quaternion.identity, _parent);
        _levelText = textGO.GetComponent<TextMeshPro>();
        _levelText.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _levelHalfWidth * 2f);

        if (_goals.Any(x => x.GoalType == LevelGoalType.ReachTheFlag))
            _levelText.text = $"{Variables.ARROW_UP} Go {Variables.ARROW_UP}";
        else if (_goals.Any(x => x.GoalType == LevelGoalType.Collect))
        {
            CollectLevelGoal goal = _goals.First(x => x.GoalType == LevelGoalType.Collect) as CollectLevelGoal;
            int target = goal.TargetValue;
            _levelText.text = $"Collect {target}{Variables.COLLECTABLE_TAG}";
        }

    }

    protected virtual void TryGenerateFlag()
    {
        if (IsInfinite) return;

        //Generate Flag
        if (_goals.Any(x => x.GoalType == LevelGoalType.ReachTheFlag))
        {
            var flagGO = GameObject.Instantiate(Prefabs.I.Flag, Vector3.zero.With(y: _levelHeight), Quaternion.identity, _parent);
            var newSize = flagGO.GetComponent<SpriteRenderer>().size;
            flagGO.GetComponent<SpriteRenderer>().size = newSize.With(x: _levelHalfWidth * 2f);
        }
    }

    protected void SetUpCamera()
    {
        //Reset camera first
        //GameController.I.CameraFollow.CameraType = CameraFollow.CameraFollowType.HeightFollow;
        GameController.I.CameraFollow.CameraType = CameraFollow.CameraFollowType.PlayerFollow;
        GameController.I.CameraFollow.TrackX = false;

        //Set up
        if (_settings.Direction == DirectionType.Down)
            GameController.I.CameraFollow.CameraType = CameraFollow.CameraFollowType.PlatformFollow;

        if (_settings.LevelWidth > 1)
            GameController.I.CameraFollow.TrackX = true;
    }


    protected void GenerateLava()
    {
        if (_goals.Any(x => x.GoalType == LevelGoalType.SlowLava))
        {
            GameController.I.Lava = GameObject.Instantiate(Prefabs.I.Lava, Vector3.down * 20f, Quaternion.identity, _parent).GetComponent<Lava>();
            GameController.I.Lava.Init(true, _levelHalfWidth * 2f);
        }
        else if (_goals.Any(x => x.GoalType == LevelGoalType.FastLava))
        {
            GameController.I.Lava = GameObject.Instantiate(Prefabs.I.Lava, Vector3.down * 20f, Quaternion.identity, _parent).GetComponent<Lava>();
            GameController.I.Lava.Init(false, _levelHalfWidth * 2f);
        }
    }

    protected virtual void TrySpawnSomething(Vector3 platformPos)
    {
        if (_lastPlatform.CanAnythingSpawnOnTop == false) return;
        if (platformPos.y >= _levelHeight && IsInfinite == false) return;

        //GOLD
        if (platformPos.y - _lastCoinY > Variables.I.CoinSpreadDistance)
        {
            //if (Random.Range(0, 2) == 0)
            //{
            GameObject.Instantiate(Prefabs.I.Coin, platformPos + Vector3.up, Quaternion.identity, _parent);
            _lastCoinY = platformPos.y + 2f.Range(2f);
            return;
            //}
        }
        //COLLECTIBLE
        if (_spawnCollectables && platformPos.y - _lastCollectableY > Variables.I.CollectableSpreadDistance)
        {

            //if (Random.Range(0, 3) == 0)
            //{
            GameObject.Instantiate(Prefabs.I.Collectable, platformPos + Vector3.up, Quaternion.identity, _parent);
            _lastCollectableY = platformPos.y + 5f.Range(5f);
            return;
            //}
        }
        //BONUS

        //if (platformPos.y - _lastGeneratedBonusPos.y < 50f) return;

        if (platformPos.y - _lastBonusY > Variables.I.BonusSpreadDistance)
        {
            //if (Random.Range(0, 3) == 0)
            //{
            GameObject bonus = Prefabs.I.Bonuses.PickRandom();
            var bc = GameObject.Instantiate(Prefabs.I.BonusCollectable, platformPos + Vector3.up, Quaternion.identity, _parent);
            bc.GetComponent<BonusCollectable>().Init(bonus);
            _lastBonusY = platformPos.y + 5f.Range(5f);
            return;
            //}
        }
    }

    protected int i = 1;
    float markerEveryUints = 25f;
    protected virtual void TryGenerateHeightMarker()
    {
        if (_goals.Any(x => x.GoalType == LevelGoalType.ReachTheFlag))
        {
            if (_lastPlatformHeight > i * _levelHeight / 4f)
            {
                var go = GameObject.Instantiate(Prefabs.I.HeightMark, Vector3.zero.With(y: i * _levelHeight / 4f), Quaternion.identity, _parent);
                go.GetComponentInChildren<TextMesh>().text = $"{(i * 25f).ToString("0")} %";
                i++;
            }

        }
        else if (_lastPlatformHeight > markerEveryUints * i)
        {
            var go = GameObject.Instantiate(Prefabs.I.HeightMark, Vector3.zero.With(y: markerEveryUints * i), Quaternion.identity, _parent);
            go.GetComponentInChildren<TextMesh>().text = (markerEveryUints * i).ToString("0");
            i++;
        }
    }



    protected virtual void SetPlatformHeight()
    {
        float progress = ((_lastPlatformHeight % _levelHeight) / _levelHeight) % 1f;
        float heightFromCurve = _settings.PlatformDistCurve.Evaluate(progress);
        heightFromCurve = Mathf.Clamp01(heightFromCurve + GameController.I.Difficulty / 2f);

        if (_settings.UsePlatfromGrid)
        {
            WeightedFloatList heightList = new WeightedFloatList(_settings.MinPlatformDist * _settings.GridSize, _settings.MaxPlatformDist * _settings.GridSize);
            heightList.SetPeak(heightFromCurve, 7f, 0.3f);
            float randomHeight = heightList.Items.Random(i => i.Weight).Value;
            randomHeight = Mathf.RoundToInt(randomHeight / _settings.GridSize);
            _lastPlatformHeight += randomHeight * _settings.GridSize;
        }
        else
        {
            WeightedFloatList heightList = new WeightedFloatList(_settings.MinPlatformDist, _settings.MaxPlatformDist);
            heightList.SetPeak(heightFromCurve, 7f, 0.3f);
            float randomHeight = heightList.Items.Random(i => i.Weight).Value;

            _lastPlatformHeight += randomHeight;
        }
    }

    protected float GetPlatformWidth()
    {

        float progress = ((_lastPlatformHeight % _levelHeight) / _levelHeight) % 1f;
        float widthFromCurve = _settings.PlatformWidthCurve.Evaluate(progress);
        widthFromCurve = Mathf.Clamp01(widthFromCurve - GameController.I.Difficulty / 2f);

        WeightedFloatList widthList = new WeightedFloatList(_settings.MinPlatformWidth, _settings.MaxPlatformWidth);
        widthList.SetPeak(widthFromCurve, 5f, 0.3f);

        float width = widthList.Items.Random(i => i.Weight).Value;
        //float width = Random.Range(_settings.MinPlatformWidth, _settings.MaxPlatformWidth);

        return width;
    }

    protected virtual float GetXPos(float platformWidth)
    {
        List<MinMaxFloatPair> ranges = new List<MinMaxFloatPair>();
        float xpos = 0f;

        MinMaxFloatPair range1 = new MinMaxFloatPair(-_levelHalfWidth + platformWidth / 2f, _lastPlatform.transform.position.x - _lastPlatformWidth / 2f);
        if (range1.IsValid())
            ranges.Add(range1);

        MinMaxFloatPair range2 = new MinMaxFloatPair(_lastPlatform.transform.position.x + _lastPlatformWidth / 2f, _levelHalfWidth - platformWidth / 2f);
        if (range2.IsValid())
            ranges.Add(range2);


        if (ranges.Count > 1)
        {
            MinMaxFloatPair selectedRange;
            if (ranges[0].Range() > ranges[1].Range())
                selectedRange = ranges[0];
            else
                selectedRange = ranges[1];
            xpos = Random.Range(selectedRange.Min, selectedRange.Max);
        }
        else if (ranges.Count == 1)
        {
            xpos = Random.Range(ranges[0].Min, ranges[0].Max);
        }
        else
        {
            xpos = Random.Range(-_levelHalfWidth + platformWidth / 2f, _levelHalfWidth - platformWidth / 2f);
        }

        return xpos;
    }

}

public class MinMaxFloatPair
{
    public float Min, Max;

    public MinMaxFloatPair(float min, float max)
    {
        Min = min;
        Max = max;
    }

    public bool IsValid()
    {
        if (Max >= Min)
            return true;
        else
            return false;
    }

    public float Range()
    {
        return Max - Min;
    }
}
