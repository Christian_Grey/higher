﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GeneratorPresets : PersistentHumbleSingleton<GeneratorPresets>
{

    public GeneratorSettings[] Presets;
    public GoalCombinationSO[] BasicGoalCombinations;

    void OnEnable()
    {
        Presets = Resources.LoadAll<GeneratorSettings>("GeneratorPresets/Pool");
        BasicGoalCombinations = Resources.LoadAll<GoalCombinationSO>("GoalCombinations/Pool");
    }

    /*
    public static Dictionary<string, GeneratorSettings> Settings = new Dictionary<string, GeneratorSettings>()
    {
        {"Basic",
            new GeneratorSettings {
                GridSize = 2,
                UsePlatfromGrid = true,
                UseLevelBounds = true,
                LevelHeight = 150f,
                MinPlatformDist = 1f,
                MaxPlatformDist = 2f,
                MinPlatformWidth = 2f,
                MaxPlatformWidth = 6.5f,
                Platforms = new Dictionary<string, int>() { { "OneWay", 20 }, { "Jumpy", 1 } }
            }
        },

        {"Small but frequent",
            new GeneratorSettings {
                GridSize = 1,
                UsePlatfromGrid = true,
                UseLevelBounds = true,
                LevelHeight = 150f,
                MinPlatformDist = 1f,
                MaxPlatformDist = 1f,
                MinPlatformWidth = 1.5f,
                MaxPlatformWidth = 3f,
                Platforms = new Dictionary<string, int>() { { "OneWay", 20 }, { "Jumpy", 1 } }
            }
        },

        {"Descent1",
            new GeneratorSettings {
                Direction = DirectionType.Down,
                GridSize = 8,
                UsePlatfromGrid = true,
                UseLevelBounds = true,
                LevelHeight = 150f,
                MinPlatformDist = 1f,
                MaxPlatformDist = 1f,
                MinPlatformWidth = 2f,
                MaxPlatformWidth = 4f,
                Platforms = new Dictionary<string, int>() { { "Sticky", 10 } }
}
        },

        {"Descent2",
            new GeneratorSettings {
                Direction = DirectionType.Down,
                GridSize = 4.5f,
                UsePlatfromGrid = true,
                UseLevelBounds = true,
                LevelHeight = 150f,
                MinPlatformDist = 1f,
                MaxPlatformDist = 1f,
                MinPlatformWidth = 2f,
                MaxPlatformWidth = 4f,
                Platforms = new Dictionary<string, int>() { { "Sticky", 10 } }
            }
        },

    };
    */

}




