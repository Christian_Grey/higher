﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class LevelGeneratorDescent : LevelGenerator
{

    public override void GenerateLevel(GeneratorSettings settings, Transform parent, List<LevelGoal> goals)
    {
        LevelLoader.clear();
        _settings = settings;
        _currentLevelHeight = -10f;
        _parent = parent;
        _levelHeight = -_settings.LevelHeight - (_settings.LevelHeight * GameController.I.Difficulty);
        _goals = goals;
        _levelHalfWidth = Variables.I.LEVEL_HALF_WIDTH * _settings.LevelWidth;
        _lastWallHeight = 10f;
        _spawnCollectables = goals.Any(x => x.GoalType == LevelGoalType.Collect);
        _platformPool = new PoolPseudoRandom<PlatformType>(PlatformWeight.ToDictionary(_settings.PlatformWeights));

        // INFINITE level if NOT reach the flag
        if (_goals.Any(g => g.GoalType == LevelGoalType.ReachTheFlag) == false)
            IsInfinite = true;

        //Make starting platform
        _lastPlatform = Prime31Pool.I.Spawn(Prefabs.I.StickyPlatform, new Vector3(0, 0), Quaternion.identity, parent).GetComponent<Platform>();
        _lastPlatform.Init(new Vector2(3.5f, 0.5f));
        // _lastPlatform.GetComponent<SpriteRenderer>().size = new Vector2(GetPlatformWidth(), 1f);

        TryGenerateFlag();

        //Checkpoint
        Checkpoint.Point = _lastPlatform.transform.position + Vector3.up;

        //Intermediate platforms
        GenerateIntermediatePlatforms();
        GameController.I.Player.DescentMode = true;

        SetUpCamera();
        GameController.I.RepositionPlayer();
        SpawnObjectDestructionArea();

    }

    void SpawnObjectDestructionArea()
    {
        GameObject.Instantiate(Prefabs.I.ObjectDestructionArea, Vector3.up * 35f, Quaternion.identity, Camera.main.transform);
    }

    public override void GenerateIntermediatePlatforms(float addHeight = 0)
    {
        _currentLevelHeight = Mathf.Clamp(_currentLevelHeight - addHeight, _levelHeight, 0);
        while (_lastPlatformHeight > _currentLevelHeight)
        {
            SetPlatformHeight();
            if (_lastPlatformHeight < _levelHeight + 2.5f && IsInfinite == false)
            {
                //Debug.Log($"l pl height: {_lastPlatformHeight}, level height {_levelHeight}");
                _lastPlatformHeight = _levelHeight;
                break;
            }

            float width = GetPlatformWidth();

            //float rightBound = 
            // if last platform was more to the right --> make next to the left
            //if (_lastPlatform.transform.position.x > 0)



            float xPos = GetXPos(width);

            TryGenerateHeightMarker();

            Vector3 platformPos = new Vector3(xPos, _lastPlatformHeight);
            //var platformName = _settings.PlatformWeights.Random(x => x.Weight).Type.ToString();
            //GameObject platformToSpawn = Prefabs.DestructibleObjects[platformName];

            PlatformType platformType = _platformPool.GetRandom();
            _platformPool.DecreaseChance(platformType);
            GameObject platformToSpawn = Prefabs.DestructibleObjects[platformType.ToString()];

            _lastPlatform = Prime31Pool.I.Spawn(platformToSpawn, platformPos, Quaternion.identity, _parent).GetComponent<Platform>();
            _lastPlatform.Init(width);
            _lastPlatformWidth = width;

            TrySpawnSomething(platformPos);
        }

        while (_lastWallHeight > _currentLevelHeight - 20)
        {

            for (int i = 0; i < 2; i++)
            {
                GameObject wallGO = GameObject.Instantiate(Prefabs.I.Wall, Vector3.zero, Quaternion.identity, _parent) as GameObject;
                var wall = wallGO.GetComponent<Platform>();
                wall.Init(new Vector2(10f, Variables.I.WallSegmentHeight));
                Vector3 wallPos = Vector3.zero.With(y: _lastWallHeight);

                if (i == 0)
                    wallPos = wallPos.With(x: _levelHalfWidth + 5f);
                else
                    wallPos = wallPos.With(x: -_levelHalfWidth - 5f);

                wallGO.transform.position = wallPos;
            }

            _lastWallHeight -= Variables.I.WallSegmentHeight;
        }

    }

    protected override void TryGenerateLevelText()
    {
        if (_levelText != null) return;

        var textGO = GameObject.Instantiate(Prefabs.I.LevelText, Vector3.up * 3.5f, Quaternion.identity, _parent);
        _levelText = textGO.GetComponent<TextMeshPro>();
        _levelText.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _levelHalfWidth * 2f);

        if (_goals.Any(x => x.GoalType == LevelGoalType.ReachTheFlag))
            _levelText.text = $"{Variables.ARROW_DOWN} Go {Variables.ARROW_DOWN}";
        else if (_goals.Any(x => x.GoalType == LevelGoalType.Collect))
        {
            CollectLevelGoal goal = _goals.First(x => x.GoalType == LevelGoalType.Collect) as CollectLevelGoal;
            int target = goal.TargetValue;
            _levelText.text = $"Collect {target}{Variables.COLLECTABLE_TAG}";
        }

    }

    protected override void TrySpawnSomething(Vector3 platformPos)
    {
        if (platformPos.y <= _levelHeight && IsInfinite == false) return;
        if (_lastPlatform.CanAnythingSpawnOnTop == false) return;

        //GOLD
        if (_lastCoinY - platformPos.y > Variables.I.CoinSpreadDistance)
        {
            if (Random.Range(0, 4) == 0)
            {
                GameObject.Instantiate(Prefabs.I.Coin, platformPos + Vector3.up, Quaternion.identity, _parent);
                _lastCoinY = platformPos.y;
                return;
            }
        }
        //COLLECTIBLE
        if (_spawnCollectables && _lastCollectableY - platformPos.y > Variables.I.CollectableSpreadDistance)
        {

            if (Random.Range(0, 4) == 0)
            {
                GameObject.Instantiate(Prefabs.I.Collectable, platformPos + Vector3.up, Quaternion.identity, _parent);
                _lastCollectableY = platformPos.y;
                return;
            }
        }

    }



    //protected override void TryGenerateFlag()
    //{
    //    //Generate Flag
    //    if (_lastPlatformHeight <= _levelHeight)
    //    {
    //        if (_goals.Any(x => x.GoalType == LevelGoalType.ReachTheFlag))
    //        {
    //            Debug.Log("Trying to generate flag");
    //            Vector3 flagPos = _lastPlatform.transform.position + Vector3.up;
    //            GameObject.Instantiate(Prefabs.I.Flag, flagPos, Quaternion.identity, _parent);
    //        }
    //    }
    //}

    protected override float GetXPos(float platformWidth)
    {
        List<MinMaxFloatPair> ranges = new List<MinMaxFloatPair>();
        float xpos = 0f;

        MinMaxFloatPair range1 = new MinMaxFloatPair(-_levelHalfWidth + platformWidth / 2f, _lastPlatform.transform.position.x - _lastPlatformWidth / 2f - platformWidth / 4f);
        if (range1.IsValid())
            ranges.Add(range1);

        MinMaxFloatPair range2 = new MinMaxFloatPair(_lastPlatform.transform.position.x + _lastPlatformWidth / 2f + platformWidth / 4f, _levelHalfWidth - platformWidth / 2f);
        if (range2.IsValid())
            ranges.Add(range2);


        if (ranges.Count > 1)
        {
            MinMaxFloatPair selectedRange;
            if (ranges[0].Range() > ranges[1].Range())
                selectedRange = ranges[0];
            else
                selectedRange = ranges[1];
            xpos = Random.Range(selectedRange.Min, selectedRange.Max);
        }
        else if (ranges.Count == 1)
        {
            xpos = Random.Range(ranges[0].Min, ranges[0].Max);
        }
        else
        {
            xpos = Random.Range(-_levelHalfWidth + platformWidth / 2f, _levelHalfWidth - platformWidth / 2f);
        }

        return xpos;
    }

    protected override void SetPlatformHeight()
    {
        if (_settings.UsePlatfromGrid)
        {
            _lastPlatformHeight -= Mathf.RoundToInt(Random.Range(_settings.MinPlatformDist, _settings.MaxPlatformDist)) * _settings.GridSize;
        }
        else
        {
            float progress = (_lastPlatformHeight / _settings.LevelHeight) % 1;
            float heightFromCurve = Mathf.Lerp(_settings.MinPlatformDist, _settings.MaxPlatformDist, _settings.PlatformWidthCurve.Evaluate(progress));
            float randomHeight = Random.Range(_settings.MinPlatformDist, _settings.MaxPlatformDist);
            float bias = .5f;
            _lastPlatformHeight -= Mathf.Lerp(randomHeight, heightFromCurve, bias);
        }
    }

    float markerEveryUints = 25f;
    protected override void TryGenerateHeightMarker()
    {
        //if (_lastPlatformHeight < -100f * i)
        //{
        //    var go = GameObject.Instantiate(Prefabs.I.HeightMark, Vector3.zero.With(y: -100f * i), Quaternion.identity, _parent);
        //    go.GetComponentInChildren<TextMesh>().text = (100f * i).ToString("0");
        //    i++;
        //}



        if (_goals.Any(x => x.GoalType == LevelGoalType.ReachTheFlag))
        {
            if (_lastPlatformHeight < i * _levelHeight / 4f)
            {
                var go = GameObject.Instantiate(Prefabs.I.HeightMark, Vector3.zero.With(y: i * _levelHeight / 4f), Quaternion.identity, _parent);
                go.GetComponentInChildren<TextMesh>().text = $"{(i * 25f).ToString("0")} %";
                i++;
            }

        }
        else if (_lastPlatformHeight < markerEveryUints * i)
        {
            var go = GameObject.Instantiate(Prefabs.I.HeightMark, Vector3.zero.With(y: markerEveryUints * i), Quaternion.identity, _parent);
            go.GetComponentInChildren<TextMesh>().text = (markerEveryUints * i).ToString("0");
            i++;
        }

    }

}
