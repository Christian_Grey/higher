﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Level
{
    public float MaxLevelHeight => Generator.LevelHeight;
    public float CurrentMaxLevelHeight => Generator.CurrentLevelHeight;

    public int LevelMoney;

    //public LevelSettings LevelSettings;
    public GeneratorSettings Settings { get; set; }
    public LevelGenerator Generator;

    public List<LevelGoal> Goals = new List<LevelGoal>();

    public void AddLevelMoney(int v)
    {
        LevelMoney += v;
        EventSystem.FireEvent<MoneyChanged>(new MoneyChanged(LevelMoney, true));
    }

    public bool GoalsReached()
    {
        bool reached = Goals.TrueForAll(x => x.Reached);
        bool superReached = Goals.Any(g => g.SuperReached);
        return (reached || superReached);
    }

    public bool AnyGoalFailed()
    {
        return Goals.Any(x => x.Failed);
    }

    /*
    public Level(LevelSettings settings)
    {
        Settings = settings.GeneratorSettings;
        var goals = new List<LevelGoal>();
        goals.
        LevelGenerator gen = new LevelGenerator();
        gen.GenerateLevel(Settings, GetLevelParent(), goals);
    }
    */

    public Level(GeneratorSettings settings)
    {
        Settings = settings;
        GenerateGoals();

        if (Settings.Direction == DirectionType.Down)
            Generator = new LevelGeneratorDescent();
        else
            Generator = new LevelGenerator();
    }

    public void Generate()
    {
        Generator.GenerateLevel(Settings, GetLevelParent(), Goals);
    }

    void GenerateGoals()
    {
        foreach (var goalType in Settings.GoalsCombination.Goals)
        {
            AddGoal(goalType);
        }



        //Modify time trial
        //if (goalTypes.Goal2 == LevelGoalType.TimeTrial)
        //{
        //    var timeTrial = (TimeTrialLevelGoal)goals[1];

        //    if (goalTypes.Goal1 == LevelGoalType.ReachTheFlag)
        //    {
        //        timeTrial.SetTargetTime(Settings.LevelHeight * 0.15f);
        //    }
        //    if (goalTypes.Goal1 == LevelGoalType.Collect)
        //        timeTrial.SetTargetTime(((CollectLevelGoal)goals[0]).TargetValue * 8f);
        //    if (goalTypes.Goal1 == LevelGoalType.FloorsInRow)
        //        timeTrial.SetTargetTime(((FloorsInRowLevelGoal)goals[0]).TargetValue * 1.3f);
        //}

    }

    void AddGoal(LevelGoalType goalType)
    {

        switch (goalType)
        {
            case LevelGoalType.ReachTheFlag:
                Goals.Add(GameObject.Instantiate(Prefabs.I.ReachTheFlagGoalUIItem));
                break;
            case LevelGoalType.Collect:
                Goals.Add(GameObject.Instantiate(Prefabs.I.CollectGoalUIItem));
                break;
            case LevelGoalType.FloorsInRow:
                Goals.Add(GameObject.Instantiate(Prefabs.I.FloorsInRowGoalUIItem));
                break;
            case LevelGoalType.DontDie:
                Goals.Add(GameObject.Instantiate(Prefabs.I.DontDieGoalUIItem));
                break;
            case LevelGoalType.Die:
                Goals.Add(GameObject.Instantiate(Prefabs.I.DieGoalUIItem));
                break;
            case LevelGoalType.SlowLava:
                Goals.Add(GameObject.Instantiate(Prefabs.I.SlowLavaGoalUIItem));
                break;
            case LevelGoalType.FastLava:
                Goals.Add(GameObject.Instantiate(Prefabs.I.FastLavaGoalUIItem));
                break;

            default:
                break;
        }
    }

    Transform GetLevelParent()
    {
        var levelParent = GameObject.Find("Level").transform;

        if (levelParent == null)
            levelParent = new GameObject("Level").transform;

        return levelParent;
    }

}

