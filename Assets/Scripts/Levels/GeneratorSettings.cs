﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DirectionType { Up, Down }

[CreateAssetMenu()]
[System.Serializable]
public class GeneratorSettings : ScriptableObject
{
    public DirectionType Direction = DirectionType.Up;
    public GoalCombinationSO[] AvailableGoalsCombination;
    [HideInInspector] public GoalCombinationSO GoalsCombination;
    public float Probability;
    public float GridSize;
    public bool UsePlatfromGrid;
    public float LevelHeight;
    public float LevelWidth = 1f;
    public float MinPlatformDist, MaxPlatformDist;
    public float MinPlatformWidth, MaxPlatformWidth;
    //public Dictionary<string, int> Platforms = new Dictionary<string, int>() { { "OneWay", 10 } };
    public PlatformWeight[] PlatformWeights;
    public AnimationCurve PlatformWidthCurve;
    public AnimationCurve PlatformDistCurve;
}


