﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterController2 : VirtCharacherController
{
    public override bool IsFlying { get => _isFlying; }
    public override float BottomY { get => _collider.bounds.min.y; }
    public override Vector2 CurrentVeloctiy { get => _rigidbody2d.velocity; }

    //Settings
    [SerializeField] LayerMask _raycastAgainThisLayers;
    [SerializeField] float _standartGroundDrag;
    [SerializeField] float _jumpForce;
    [SerializeField] float _descentJumpForce;
    [SerializeField] Vector2 _initGravity;
    [SerializeField] float _airDrag;
    [SerializeField] float _airAcceleration;
    [SerializeField] float _airSpeedAfterMaxSpeed;
    [SerializeField] float _maxAirSpeed;
    [SerializeField] float _flightSpeed;
    [SerializeField] float _velocityPollingDelay;
    [SerializeField] float _maxDownSpeed;


    //Links
    [SerializeField] Rigidbody2D _rigidbody2d;
    [SerializeField] BoxCollider2D _collider;
    [SerializeField] SpriteRenderer _sprite;
    [SerializeField] Transform _spriteScaler;
    [SerializeField] GameObject _landingParticles;
    [SerializeField] Trail _trail;

    //internal
    Platform _platformWeTouch;
    Vector2 _velocity;
    Vector2 _prevFrameVelocity;
    Vector2 _2frameAgoVel;
    Vector2 _gravity;
    Vector2 _lastCheckpointSpot;
    float _horizontalInput;
    float _jumpTime;
    float _lastGroundedTime;
    float _noCollisionCheckTime = 0.02f;
    float _wallBouceTime;
    bool _motionAllowed = true;
    bool _touchingWall;
    float _jumpMultiplier = 1f;
    bool _isFlying;
    Queue<Vector2> _velocityPoll = new Queue<Vector2>();
    float _lastTimeVelPolled;


    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2d = GetComponent<Rigidbody2D>();
        _gravity = _initGravity;
        MakeCheckpoint();
        LoadSkins();
    }

    void Update()
    {
        PollVelocity();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_motionAllowed == false) return;

        HorizontalMovement();
        ModifyGravityScale();
        ApplyGravity();
        StretchAndSquash();
        TryFlying();
        // OneWayLogic();
        ApplyExternalForce();
        ApplyVelocity();
        //End of frame
        ResetInfluences();
        SaveVelocityHistory();
    }

    void OnEnable()
    {
        EventSystem.AddListener<BonusFlightEvent>(EnableFlight);
    }

    void OnDisable()
    {
        EventSystem.DeleteListener<BonusFlightEvent>(EnableFlight);
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        ResolveCollision(collision);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        ResolveCollision(collision);
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        _platformWeTouch = null;
        Grounded = false;
    }



    public override void SetJumpMultiplier(float value)
    {
        _jumpMultiplier = value;
    }

    public override void Jump()
    {
        if (DescentMode)
        {
            JumpDescent();
            return;
        }

        PlayLandingParticles(_collider.bounds.center.With(y: _collider.bounds.min.y));
        AudioController.I.Play(AudioController.I.JumpSound, 0.2f);
        float platformMultiplier = _platformWeTouch == null ? 1 : _platformWeTouch.JumpMultiplier;
        Grounded = false;
        AdditionalVelocity = Vector2.zero;
        _jumpTime = Time.time;
        float speedComponent = Mathf.Abs(_velocity.x) / 1.3f;
        _velocity.y = _jumpForce + _jumpMultiplier + platformMultiplier + speedComponent;
        EventSystem.FireEvent<PlayerStartedJump>(new PlayerStartedJump(transform.position, _velocity, _platformWeTouch.gameObject));
    }

    void JumpDescent()
    {
        PlayLandingParticles(_collider.bounds.center.With(y: _collider.bounds.min.y));
        AudioController.I.Play(AudioController.I.JumpSound, 0.2f);
        Grounded = false;
        AdditionalVelocity = Vector2.zero;
        _jumpTime = Time.time;
        _velocity.y = _descentJumpForce;
        EventSystem.FireEvent<PlayerStartedJump>(new PlayerStartedJump(transform.position, _velocity, _platformWeTouch.gameObject));
    }

    public override void Bounce(float q)
    {
        Grounded = false;
        AdditionalVelocity = Vector2.zero;
        _jumpTime = Time.time;
        _velocity = new Vector2(_2frameAgoVel.x, -(_2frameAgoVel.y * q));
    }


    public override void StopMotionInstantly()
    {
        _velocity = Vector2.zero;
        _prevFrameVelocity = Vector2.zero;
        _2frameAgoVel = Vector2.zero;
        _rigidbody2d.velocity = _velocity;

        _spriteScaler.localScale = Vector3.one;
    }
    public override void AllowMotion(bool b)
    {
        _motionAllowed = b;
    }

    public override void TeleportTo(Vector3 newPos)
    {
        StopMotionInstantly();
        transform.position = newPos;
    }

    public override void PlayTrail()
    {
        _trail.Play();
    }

    public override void StopTrail()
    {
        _trail.Stop();
    }

    public void ReceiveHorizInput(float input)
    {
        _horizontalInput = input;
    }

    public void LoadSkins()
    {
        ChangeSkin(GameDataHolder.I.Save.CurrentSkin);
        ChangeLandingFX(GameDataHolder.I.Save.CurrentLandingFX);
        ChangeTrail(GameDataHolder.I.Save.CurrentTrail);
    }

    public override void ChangeSkin(string skinName)
    {
        Debug.Log($"Assigning skin: {skinName}");
        var skinData = Prefabs.I.Skins.GetGOByName(skinName);

        if (skinData == null)
        {
            Debug.LogError("No SKIN with such name");
            return;
        }

        _sprite.sprite = skinData.CustomizationItem.GetComponent<SpriteRenderer>().sprite;
        GameDataHolder.I.Save.CurrentSkin = skinName;
        GameDataHolder.I.SaveGameData();
    }

    public override void ChangeLandingFX(string fxName)
    {
        Debug.Log($"Assigning landing fx");
        base.ChangeLandingFX(fxName);
        var landingData = Prefabs.I.LandingFX.GetGOByName(fxName);

        if (landingData == null)
        {
            Debug.LogError("No LANDING_FX with such name");
            return;
        }

        _landingParticles = landingData.CustomizationItem;
        GameDataHolder.I.Save.CurrentLandingFX = fxName;
        GameDataHolder.I.SaveGameData();
    }

    public override void ChangeTrail(string trailName)
    {
        Debug.Log($"Assigning trail");
        base.ChangeTrail(trailName);
        var trailData = Prefabs.I.Trails.GetGOByName(trailName);

        if (trailData == null)
        {
            Debug.LogError("No TRAIL with such name");
            return;
        }
        if (_trail != null)
        {
            Debug.Log($"Trying to destoy trail: {_trail.name}");
            Destroy(_trail.gameObject);
        }

        _trail = Instantiate(trailData.CustomizationItem, _spriteScaler.position, Quaternion.identity, _spriteScaler).GetComponent<Trail>();
        GameDataHolder.I.Save.CurrentTrail = trailName;
        GameDataHolder.I.SaveGameData();
        _trail.Stop();
    }

    void MakeCheckpoint()
    {
        Checkpoint.Point = transform.position;
    }

    void TryFlying()
    {
        if (_isFlying == true)
            Fly();
    }

    void Fly()
    {
        _velocity.y = _flightSpeed;
    }

    void HorizontalMovement()
    {
        //DRAG
        //    _velocity.x = Mathf.Lerp(_velocity.x, 0, _airDrag * Time.fixedDeltaTime);

        //var scaledInput = -1 * _maxAirSpeed;
        var scaledInput = _horizontalInput * _maxAirSpeed;
        float delta;
        if (scaledInput == 0)
        {
            delta = 0f;
        }
        else if (scaledInput > 0 && _velocity.x > 0)
        {
            delta = scaledInput - _velocity.x;
            delta = Mathf.Clamp(delta, _airSpeedAfterMaxSpeed, _maxAirSpeed);
        }
        else if (scaledInput < 0 && _velocity.x < 0)
        {
            delta = scaledInput - _velocity.x;
            delta = Mathf.Clamp(delta, -_maxAirSpeed, -_airSpeedAfterMaxSpeed);
        }
        else
        {
            delta = scaledInput - _velocity.x;
        }

        delta *= _airAcceleration * Time.fixedDeltaTime;
        _velocity.x += delta;

    }

    void ModifyGravityScale()
    {
        if (_rigidbody2d.velocity.y < 0)
            _gravity = _initGravity * 2f;
        else
            _gravity = _initGravity;
    }

    void ApplyGravity()
    {
        if (Grounded) return;

        _velocity += _gravity * Time.fixedDeltaTime;

        if (_velocity.y < -_maxDownSpeed)
            _velocity.y = -_maxDownSpeed;
    }

    void StretchAndSquash()
    {
        var lookDir = _velocity.y > 0 ? (Vector3)_rigidbody2d.velocity.normalized : -(Vector3)_rigidbody2d.velocity.normalized;
        float mult = _velocity.magnitude.RemapClamp(5, 45, 0, 1);

        _spriteScaler.LookAt(_spriteScaler.position + (Vector3)_rigidbody2d.velocity, Vector3.back);
        _sprite.transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
        float zStretch = _velocity.y.RemapClamp(-25f, 35f, 0.85f, 1.65f);
        float xStretch = _velocity.y.RemapClamp(-25f, 35f, 1.1f, 0.6f);
        xStretch = Mathf.Lerp(1, xStretch, mult);
        zStretch = Mathf.Lerp(1, zStretch, mult);
        _spriteScaler.localScale = new Vector3(xStretch, 1, zStretch);
    }

    void ApplyExternalForce()
    {
        _velocity += ExternalForce * Time.fixedDeltaTime;
    }

    void ApplyVelocity()
    {
        //print($"apply VEl: {ExternalVelocity} Vel: {_velocity}");
        _rigidbody2d.velocity = AdditionalVelocity + _velocity;
    }

    void ResetInfluences()
    {
        ExternalForce = Vector2.zero;
        AdditionalVelocity = Vector2.zero;
    }

    void SaveVelocityHistory()
    {
        _2frameAgoVel = _prevFrameVelocity;
        _prevFrameVelocity = _velocity;
    }

    void PlayLandingParticles(Vector3 pos)
    {
        Instantiate(_landingParticles, pos, _landingParticles.transform.rotation);
    }

    //bool CastFromLine(Vector2 point1, Vector2 point2, int numberOfRays, Vector2 castDirection, float distance, out GameObject collisionObj)
    //{
    //    collisionObj = null;
    //    RaycastHit2D hit;
    //    //find all the points
    //    for (int i = 0; i < numberOfRays; i++)
    //    {
    //        Vector2 castPoint = Vector2.Lerp(point1, point2, (float)i / (float)(numberOfRays - 1));
    //        hit = Physics2D.Raycast(castPoint, castDirection, distance, _raycastAgainThisLayers);
    //        if (hit)
    //        {
    //            collisionObj = hit.collider.gameObject;
    //            return true;
    //        }
    //    }
    //    return false;
    //}

    void EnableFlight(BonusFlightEvent info)
    {
        _isFlying = info.Enable;
    }

    void ResolveCollision(Collision2D collision)
    {
        if (_motionAllowed == false) return;
        if (Time.time - _jumpTime < _noCollisionCheckTime) return;


        if (collision.gameObject.GetComponent<Platform>() != null)
            _platformWeTouch = collision.gameObject.GetComponent<Platform>();

        //DOWN CASE
        if (Vector2.Dot(collision.GetContact(0).normal, Vector2.up).CloseTo(1, 0.2f))
        {
            //if (Grounded == false)
            //PlayLandingParticles(collision.GetContact(0).point);
            Grounded = true;
            //Moving platform case
            if (_platformWeTouch != null && _platformWeTouch.GetComponent<MovingObject>() != null)
                AdditionalVelocity = _platformWeTouch.GetComponent<MovingObject>().Velocity;

            //Ice platform case
            if (_platformWeTouch != null && _platformWeTouch.PlatformDrag == 0 && _horizontalInput == 0)
                _velocity.y = 0;
            else if (_horizontalInput != 0)
                //Jump right away
                Jump();
            else
                Jump();
        }
        //SIDES
        if (Vector2.Dot(collision.GetContact(0).normal, Vector2.right).Abs().CloseTo(1, 0.2f))
        {
            _wallBouceTime = Time.time;
            _velocity.x = -_2frameAgoVel.x * (_platformWeTouch != null ? _platformWeTouch.SideBounciness : 0.5f);
        }
        //UP
        if (Vector2.Dot(collision.GetContact(0).normal, Vector2.down).CloseTo(1, 0.2f))
        {
            _velocity.y = -1f;
        }
    }


    void PollVelocity()
    {
        if (_velocityPoll.Count >= 8)
            _velocityPoll.Dequeue();

        _velocityPoll.Enqueue(_rigidbody2d.velocity);
        _lastTimeVelPolled = Time.time;
        RecalcAverageVel();
    }

    void RecalcAverageVel()
    {
        AverageVelocity = AverageVector2(_velocityPoll.ToArray());
    }

    Vector2 AverageVector2(params Vector2[] vectors)
    {
        Vector2 result;
        result.x = vectors.Sum(v => v.x) / vectors.Length;
        //Debug.Log($"Total X: {vectors.Sum(v => v.x)}, Length: {vectors.Length}, avg X: {result.x}");
        result.y = vectors.Sum(v => v.y) / vectors.Length;
        //Debug.Log($"Total Y: {vectors.Sum(v => v.y)}, Length: {vectors.Length}, avg Y: {result.y}");

        return result;
    }


}