﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtCharacherController : MonoBehaviour
{
    public virtual bool IsFlying { get; }
    public virtual bool DescentMode { get; set; }
    public bool Grounded { get; set; }
    public virtual Vector2 CurrentVeloctiy { get; set; }
    public Vector2 AverageVelocity { get; set; }
    public Vector2 AdditionalVelocity { get; set; }
    public Vector2 ExternalForce { get; set; } // should not multiply by delta time
    public virtual float BottomY { get; set; }

    public virtual void Jump() { }
    public virtual void Bounce(float q) { }
    public virtual void AllowMotion(bool b) { }
    public virtual void StopMotionInstantly() { }
    public virtual void TeleportTo(Vector3 newPos) { }
    public virtual void SetJumpMultiplier(float value) { }
    public virtual void ChangeSkin(string skinName) { }
    public virtual void ChangeLandingFX(string fxName) { }
    public virtual void ChangeTrail(string trailName) { }
    public virtual void PlayTrail() { }
    public virtual void StopTrail() { }

}