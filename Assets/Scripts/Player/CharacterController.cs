﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public bool Grounded { get; set; }
    public Vector2 AdditionalVelocity { get; set; }
    public Vector2 ExternalForce { get; set; } // should not multiply by delta time

    //Settings
    [SerializeField] LayerMask _raycastAgainThisLayers;
    [SerializeField] float _standartGroundDrag;
    [SerializeField] float _jumpForce;
    [SerializeField] Vector2 _jumpAngle;
    [SerializeField] Vector2 _initGravity;

    //Links
    [SerializeField] SmoothFollow _camera;
    [SerializeField] Rigidbody2D _rigidbody2d;
    [SerializeField] BoxCollider2D _collider;

    //internal
    MyPhysicsMaterial _materialWeTouch;
    Vector2 _velocity;
    Vector2 _prevFrameVelocity;
    Vector2 _2frameAgoVel;
    Vector2 _gravity;
    Vector2 _lastCheckpointSpot;
    float _jumpTime;
    float _noCollisionCheckTime = 0.05f;
    float _wallBouceTime;
    bool _motionAllowed = true;

    public void Jump(float force01, int direction)
    {
        float jumpMultiplier = _materialWeTouch == null ? 1 : _materialWeTouch.JumpMultiplier;
        Grounded = false;
        AdditionalVelocity = Vector2.zero;
        _jumpTime = Time.time;
        float force = Mathf.Pow(force01, 1f / 2f);
        _velocity = Vector2.zero;
        _velocity.x = _jumpAngle.x * _jumpForce * direction * force * jumpMultiplier;
        _velocity.y = _jumpAngle.y * _jumpForce * force * jumpMultiplier;
    }

    public void Jump(float force01, Vector2 direction)
    {
        float jumpMultiplier = _materialWeTouch == null ? 1 : _materialWeTouch.JumpMultiplier;
        Grounded = false;
        AdditionalVelocity = Vector2.zero;
        _jumpTime = Time.time;
        float force = Mathf.Pow(force01, 1f / 2f); // Transform to root function
        direction.Normalize();
        _velocity = _jumpForce * direction * force * jumpMultiplier;
    }

    public void Bounce(float q)
    {
        Grounded = false;
        AdditionalVelocity = Vector2.zero;
        _jumpTime = Time.time;
        _velocity = new Vector2(_2frameAgoVel.x, -(_2frameAgoVel.y * q));
    }


    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2d = GetComponent<Rigidbody2D>();
        _jumpAngle.Normalize();
        _gravity = _initGravity;
        MakeCheckpoint();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_motionAllowed == false) return;

        CheckCollisionsAndModifyVelocity();
        RepositionCamera();
        ModifyGravityScale();
        ModifyGravity();
        ApplyExternalForce();
        ApplyVelocity();
        //End of frame
        ResetInfluences();
        SaveVelocityHistory();
    }

    public void StopMotionLerp()
    {

    }

    public void StopMotionInstantly()
    {
        _velocity = Vector2.zero;
        _prevFrameVelocity = Vector2.zero;
        _2frameAgoVel = Vector2.zero;
        _rigidbody2d.velocity = _velocity;
    }
    public void AllowMotion(bool b)
    {
        _motionAllowed = b;
    }

    public void TeleportTo(Vector3 newPos)
    {
        StopMotionInstantly();
        transform.position = newPos;
    }

    private void RepositionCamera()
    {
        //if (Grounded)
        //  _camera.UpdateTargetPos(transform.position);
    }

    private void MakeCheckpoint()
    {
        Checkpoint.Point = transform.position;
    }

    private void CheckCollisionsAndModifyVelocity()
    {
        CastUpAndDown();
        CastSides();
    }

    private void CastUpAndDown()
    {
        //cast DOWN
        Vector2 point1 = transform.position.WithModified(x: -_collider.size.x / 2);
        Vector2 point2 = transform.position.WithModified(x: +_collider.size.x / 2);
        float dist = (_collider.size.y / 2) + 0.02f;
        if (Time.time - _jumpTime > _noCollisionCheckTime && _rigidbody2d.velocity.y <= 0)
        {
            if (CastFromLine(point1, point2, 3, Vector2.down, dist, out _materialWeTouch))
            {
                if (_materialWeTouch != null && _materialWeTouch.GetComponent<MovingObject>() != null)
                    AdditionalVelocity = _materialWeTouch.GetComponent<MovingObject>().Velocity;

                _velocity = Vector2.zero;
                _velocity.y = 0;
                //Ground drag
                _velocity.x = _prevFrameVelocity.x / (1 + (Time.fixedDeltaTime * (_materialWeTouch != null ? _materialWeTouch.PlatformDrag : _standartGroundDrag)));
                Grounded = true;
            }
            else
            {
                AdditionalVelocity = Vector2.zero;
                Grounded = false;
            }
        }

        //UP
        if (_rigidbody2d.velocity.y > 0)
        {
            if (CastFromLine(point1, point2, 3, Vector2.up, dist, out MyPhysicsMaterial platform))
            {
                Debug.Log("UP Contact");
                _velocity.y = 0;
            }
        }

    }

    private void CastSides()
    {
        //RIGHT
        Vector2 point1 = transform.position.WithModified(y: -_collider.size.y / 2);
        Vector2 point2 = transform.position.WithModified(y: +_collider.size.y / 2);
        float dist = _collider.size.x / 2 + 0.02f;
        if (Time.time - _wallBouceTime > _noCollisionCheckTime)
        {
            if (CastFromLine(point1, point2, 5, Vector2.right, dist, out MyPhysicsMaterial platform))
            {
                Debug.Log("RIHGT Contact");
                _wallBouceTime = Time.time;
                _velocity.x = -_2frameAgoVel.x * (platform != null ? platform.SideBounciness : 0.5f);
            }

            //LEFT
            if (CastFromLine(point1, point2, 5, Vector2.left, dist, out MyPhysicsMaterial platform2))
            {
                Debug.Log("LEFT Contact");
                _wallBouceTime = Time.time;
                _velocity.x = -_2frameAgoVel.x * (platform2 != null ? platform2.SideBounciness : 0.5f);
            }
        }
    }

    private void ModifyGravityScale()
    {
        if (_rigidbody2d.velocity.y < 0)
            _gravity = _initGravity * 2f;
        else
            _gravity = _initGravity;
    }

    private void ModifyGravity()
    {
        if (Grounded) return;

        _velocity += _gravity * Time.fixedDeltaTime;

        if (_velocity.y < -20f)
            _velocity.y = -20f;
        //_velocity.y = _velocity.y / (1 + (Time.fixedDeltaTime * _airDrag));
    }

    private void ApplyExternalForce()
    {
        _velocity += ExternalForce * Time.fixedDeltaTime;
    }

    private void ApplyVelocity()
    {
        //print($"apply VEl: {ExternalVelocity} Vel: {_velocity}");
        _rigidbody2d.velocity = AdditionalVelocity + _velocity;
    }

    private void ResetInfluences()
    {
        ExternalForce = Vector2.zero;
        AdditionalVelocity = Vector2.zero;
    }

    private void SaveVelocityHistory()
    {
        _2frameAgoVel = _prevFrameVelocity;
        _prevFrameVelocity = _velocity;
    }

    private bool CastFromLine(Vector2 point1, Vector2 point2, int numberOfRays, Vector2 castDirection, float distance, out MyPhysicsMaterial platform)
    {
        platform = null;
        RaycastHit2D hit;
        //find all the points
        for (int i = 0; i < numberOfRays; i++)
        {
            Vector2 castPoint = Vector2.Lerp(point1, point2, (float)i / (float)(numberOfRays - 1));
            //Debug.DrawRay(castPoint, castDirection * distance, Color.yellow);
            hit = Physics2D.Raycast(castPoint, castDirection, distance, _raycastAgainThisLayers);
            if (hit)
            {
                if (hit.collider.GetComponent<MyPhysicsMaterial>())
                    platform = hit.collider.GetComponent<MyPhysicsMaterial>();
                //Debug.DrawRay(hit.point, castDirection * distance, Color.red);
                return true;
            }
        }
        return false;
    }

    private bool CastFromLine(Vector2 point1, Vector2 point2, int numberOfRays, Vector2 castDirection, float distance, out GameObject collisionObj)
    {
        collisionObj = null;
        RaycastHit2D hit;
        //find all the points
        for (int i = 0; i < numberOfRays; i++)
        {
            Vector2 castPoint = Vector2.Lerp(point1, point2, (float)i / (float)(numberOfRays - 1));
            hit = Physics2D.Raycast(castPoint, castDirection, distance, _raycastAgainThisLayers);
            if (hit)
            {
                collisionObj = hit.collider.gameObject;
                return true;
            }
        }
        return false;
    }
}