﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GUIController : PersistentHumbleSingleton<GUIController>
{
    [Header("GUIElements")]
    [Header("Money")]
    public UIWindow Money;
    [SerializeField] TextMeshProUGUI _moneyText;

    public UIWindow LevelMoney;
    [SerializeField] TextMeshProUGUI _levelMoneyText;

    [Header("Bonus related")]
    [SerializeField] Slider _bonusTimer;
    [SerializeField] Text _currentBonus;
    [SerializeField] Text _totalBonus;
    [Header("Timer")]
    public Text TimerText;

    public GridLayoutGroup BonusGrid;
    public GridLayoutGroup GoalsGrid;
    public Transform HiddenGoalsGrid;

    public void Clear()
    {
        TimerText.gameObject.SetActive(false);

        //Clear goals
        foreach (Transform child in GoalsGrid.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        //
        //Clear bonuses
        foreach (Transform child in BonusGrid.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (Transform child in HiddenGoalsGrid.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

    }



    public void UpdateMoneyText()
    {
        _moneyText.text = GameDataHolder.I.Save.Money.ToString() + Variables.COIN_TAG;
        _levelMoneyText.text = GameController.I.CurrentLevel.LevelMoney.ToString() + Variables.COIN_TAG;
    }

    // Start is called before the first frame update
    void Start()
    {
        Money.Open();
        LevelMoney.Close();
        UpdateMoneyText();
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnEnable()
    {
        EventSystem.AddListener<BonusTimerChanged>(OnBonusTimerChanged);
        EventSystem.AddListener<FloorsInRowValuesChanged>(OnBonusValuesChanged);
        EventSystem.AddListener<MoneyChanged>(OnMoneyChanged);
    }

    private void OnDisable()
    {
        EventSystem.DeleteListener<BonusTimerChanged>(OnBonusTimerChanged);
        EventSystem.DeleteListener<FloorsInRowValuesChanged>(OnBonusValuesChanged);
        EventSystem.DeleteListener<MoneyChanged>(OnMoneyChanged);
    }

    void OnBonusTimerChanged(BonusTimerChanged info)
    {
        _bonusTimer.value = info.NewTimerValueNormalized;
    }

    void OnBonusValuesChanged(FloorsInRowValuesChanged info)
    {
        _currentBonus.text = info.FloorInRow.ToString("0");
        _totalBonus.text = info.FloorsInRowTotal.ToString("0");
    }

    void OnMoneyChanged(MoneyChanged info)
    {
        if (info.LevelMoney)
            _levelMoneyText.text = info.Amount.ToString() + Variables.COIN_TAG;
        else
            _moneyText.text = info.Amount.ToString() + Variables.COIN_TAG;
    }
}
