﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Prefabs : PersistentHumbleSingleton<Prefabs>
{
    [Header("Platforms")]
    public GameObject RegularPlatform;
    public GameObject OneWayPlatform;
    public GameObject MovingPlatform;
    public GameObject JumpyPlatform;
    public GameObject IcyPlatform;
    public GameObject StickyPlatform;
    public GameObject FalsePlatform;
    public GameObject CrumblyPlatform;
    [Header("Objects")]
    public GameObject Wall;
    public GameObject Bat;
    public GameObject BlackHole;
    public GameObject Chest;
    public GameObject Coin;
    public GameObject Flag;
    public GameObject Spike;
    public GameObject Collectable;
    public GameObject Lava;
    public GameObject LevelText;

    [Header("Bonuses")]
    public List<GameObject> Bonuses;
    public GameObject BonusCollectable;

    [Header("Goals")]
    public LevelGoal ReachTheFlagGoalUIItem;
    public LevelGoal CollectGoalUIItem;
    public LevelGoal FloorsInRowGoalUIItem;
    public LevelGoal DontDieGoalUIItem;
    public LevelGoal DieGoalUIItem;
    public LevelGoal SlowLavaGoalUIItem;
    public LevelGoal FastLavaGoalUIItem;

    [Header("Utility")]
    public GameObject OneWayTrigger;
    public GameObject BubblePlatform;
    public GameObject HeightMark;
    public GameObject ObjectDestructionArea;

    [Header("UI")]
    public GameObject BonusUpgradeUIItem;
    public GameObject BonusStatUIItem;
    public GameObject StatTextUIItem;
    public GameObject WindowSpriteButton;
    public UIAnimatedPopup CountdownUIItem;
    public UIAnimatedPopup PopupMessageUIItem;

    [Header("FX")]
    public GameObject RushJump;

    [Header("Skins")]
    public SkinData[] Skins;
    public SkinData[] LandingFX;
    public SkinData[] Trails;

    public static Dictionary<string, GameObject> DestructibleObjects;
    public static HashSet<string> DestructibleByLava;

    private void OnEnable()
    {
        print("Init prefabs");

        Skins = Resources.LoadAll<SkinData>("Customization/Skins");
        LandingFX = Resources.LoadAll<SkinData>("Customization/LandingFX");
        Trails = Resources.LoadAll<SkinData>("Customization/Trail");

        DestructibleObjects = new Dictionary<string, GameObject>()
        {
            {"Regular", RegularPlatform},
            {"OneWay", OneWayPlatform},
            {"Moving", MovingPlatform},
            {"Jumpy", JumpyPlatform},
            {"Icy", IcyPlatform},
            {"Sticky", StickyPlatform},
            {"False", FalsePlatform},
            {"Crumbly", CrumblyPlatform},

            {"Wall", Wall},
            {"HeightMark", HeightMark},
            {"Bat", Bat},
            {"BlackHole", BlackHole},
            {"Chest", Chest},
            {"Coin", Coin},
            {"Flag", Flag},
            {"Spike", Spike},
            {"Collectable", Collectable},
            {"BonusCollectable", BonusCollectable},
            {"Lava", Lava},
            {"ObjectDestructionArea",ObjectDestructionArea},
            {"LevelText", LevelText},
        };

        DestructibleByLava = new HashSet<string>()
        {
            "RegularPlatform",
            "OneWayPlatform",
            "MovingPlatform",
            "JumpyPlatform",
            "IcyPlatform",
            "StickyPlatform",
            "FalsePlatform",
            "CrumblyPlatform",

            "Wall",
            "HeightMark",
            "Bat",
            "BlackHole",
            "Chest",
            "Coin",
            "Spike",
            "Collectable",
            "BonusCollectable",
            "LevelText"
        };

        foreach (var bonus in Bonuses)
        {
            DestructibleObjects.Add(bonus.name, bonus);
        }

        print("Done: Init prefabs");
    }

}
