﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ExternalService;
using UnityEngine;
using UnityEngine.Events;

public class GameController : PersistentHumbleSingleton<GameController>
{
    public bool IsRunning { get; set; }
    public bool AlreadyDied { get; private set; }

    public float Difficulty;
    public int LevelsVariant;
    [Header("Debug level settings")]
    public bool UseDebugSettings;
    public int DebugLevelNumber;
    public GeneratorSettings DebugSettings;
    public GoalCombinationSO DebugGoalCombination;

    public AnimationCurve GlobalDifficulty;
    public AnimationCurve LocalDifficulty;
    public Dictionary<string, Bonus> Bonuses = new Dictionary<string, Bonus>();
    public Level CurrentLevel { get; set; }
    public VirtCharacherController Player;
    public Lava Lava;
    public CameraFollow CameraFollow;

    public UnityEvent WinEvent;
    public UnityEvent FailEvent;

    GeneratorSettings _settings;
    protected PoolPseudoRandom<GeneratorSettings> _levelSettingsPool;

    [SerializeField] float _levelGenerationThreshold;

    bool startOfSession = true;

    // Start is called before the first frame update
    public override void Awake()
    {
        base.Awake();
        Application.targetFrameRate = 60;
        CameraFollow = UnityEngine.Camera.main.GetComponent<CameraFollow>();
        Player = GameObject.FindObjectOfType<VirtCharacherController>();
        //UIController.I.PlayMessage($"Load time: {Time.realtimeSinceStartup.ToString("0.0")}, time: {Time.time.ToString("0.0")}");

        var levelSettings = Resources.LoadAll<GeneratorSettings>($"GeneratorPresets/Pool").ToList();
        _levelSettingsPool = new PoolPseudoRandom<GeneratorSettings>(levelSettings, x => x.Probability);
    }

    void Start()
    {
        Player.AllowMotion(false);

        LoadLevel();
    }

    void Update()
    {
        if (!IsRunning) return;

        CheckWinFailState();
        TryGenerateAdditionalPlatforms();


        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Instantiate(Prefabs.I.Bonuses.GetGOByName("BonusBubble"));
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Instantiate(Prefabs.I.Bonuses.GetGOByName("BonusFlight"));
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Instantiate(Prefabs.I.Bonuses.GetGOByName("BonusHigherJump"));
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Instantiate(Prefabs.I.Bonuses.GetGOByName("BonusPlatformStretch"));
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReloadLevel();
        }

    }

    public void CollectLevelMoney(float multiplier)
    {
        //float additionalAvgMoney = (CurrentLevel.LevelMoney - GameDataHolder.I.Save.AvgMoneyPerLevel) / GameDataHolder.I.Save.CurrentLevel - 1;
        //GameDataHolder.I.Save.AvgMoneyPerLevel += additionalAvgMoney;
        GameDataHolder.I.AddMoney((int)(CurrentLevel.LevelMoney * multiplier));
        UIController.I.PlayMessage($"You collected {(int)(CurrentLevel.LevelMoney * multiplier)}{Variables.COIN_TAG}", 1.5f);
        CurrentLevel.LevelMoney = 0;
    }

    //public void AddMoney(int amount)
    //{
    //    GameDataHolder.I.AddMoney(amount);
    //}

    //public void PlayerDied()
    //{
    //    PauseGame();
    //    if (_alreadyDied)
    //        UIController.I.GameOverWindow.Open();

    //    else
    //    {
    //        UIController.I.SecondChanceWindow.Open();
    //        _alreadyDied = true;
    //    }
    //}

    public void Quit()
    {
        Application.Quit();
    }

    public void PauseGame()
    {
        IsRunning = false;
        Player.AllowMotion(false);
        Player.StopMotionInstantly();
        UIController.I.GameWindow.Close();
    }

    public void Play()
    {
        GameDataHolder.I.Save.CurrentLevelAttempt++;
        ResumeGameWithCountdown();
    }

    public void ResumeGameWithCountdown()
    {
        CurrentLevel.Generator.RevealGoals();
        UIController.I.PlayCountdown(ResumeGame);
    }

    public void TryToShowInterstitial()
    {
        if (GoogleAds.I.IsInterstitialReady() && GameDataHolder.I.Save.CurrentLevel >= GameDataHolder.I.Save.NextInterstitialAd)
        {
            GameDataHolder.I.Save.NextInterstitialAd += Variables.I.InterstitialEveryNLevel.Range(1);
            GameDataHolder.I.SaveGameData();
            GoogleAds.I.ShowInterstitial();
        }
    }

    public void GiveSecondChance()
    {
        UnfailLevelGoals();
        TeleportPlayerToClosestPlatform();
        ResumeGameWithCountdown();
    }

    public void TeleportPlayerToClosestPlatform()
    {
        List<Platform> allPlatforms = FindObjectsOfType<Platform>().Where(x => x.name.StartsWith("Wall") == false).ToList();
        if (allPlatforms.Count < 1) return;

        if (CurrentLevel.Settings.Direction == DirectionType.Up)
        {
            allPlatforms = allPlatforms.Where(x => x.transform.position.y > Player.transform.position.y + 15f).ToList();
        }
        else
        {
            allPlatforms = allPlatforms.Where(x => x.transform.position.y < (Player.transform.position.y + 10f)).ToList();
        }


        Vector3 screenTop = Camera.main.ViewportToWorldPoint(new Vector2(0.5f, 0.9f)).With(z: 0f);
        Debug.DrawRay(screenTop, Vector3.right, Color.red, 15f);
        Platform closestPlatform = allPlatforms[0];

        foreach (var pl in allPlatforms)
        {
            if (Vector3.Distance(screenTop, pl.transform.position) <
                Vector3.Distance(screenTop, closestPlatform.transform.position))
            {
                closestPlatform = pl;
            }
        }

        Checkpoint.Point = closestPlatform.transform.position.WithModified(y: 5f);
        RepositionPlayer();
    }

    public void Clear()
    {
        GUIController.I.Clear();
    }

    public void ReloadLevel()
    {
        AlreadyDied = false;
        Clear();
        PauseGame();
        GameDataHolder.I.SaveGameData();
        GenerateLevel();
        //RepositionPlayer();
    }

    public void LoadLevel()
    {
        AlreadyDied = false;
        Clear();
        CalculateDifficulty();
        GoogleAds.I.RequestInterstitial();
        GoogleAds.I.RequestRewarded();
        PauseGame();
        GameDataHolder.I.SaveGameData();
        //Player.StopMotionInstantly();
        //Player.AllowMotion(false);

        //++GameData.Instance.currentLevel;
        //GameData.Instance.save();
        SelectNewSettings();

        AnalyticsController.I.SetUserProperty("current_level", GameDataHolder.I.Save.CurrentLevel);
        AnalyticsController.I.SetUserProperty("current_level_settings", $"{_settings.name}_{_settings.GoalsCombination.name}");


        GenerateLevel();
        //LevelLoader.I.level = GameData.Instance.currentLevel.ToString();
        //LevelLoader.I.load();

        //RepositionPlayer();
    }



    public void GenerateLevel()
    {
        if (_settings == null)
        {
            Debug.LogError("Trying to generate level, NO Settings or NO goals!");
            Debug.Log($"Settings is null: {_settings == null}");
        }

        CurrentLevel = new Level(_settings);
        CurrentLevel.Generate();
        GUIController.I.UpdateMoneyText();
        EventSystem.FireEvent<LevelStarted>(new LevelStarted());
    }

    public void RepositionPlayer()
    {

        Player.TeleportTo(Checkpoint.Point);
        //Player.AllowMotion(true);
        CameraFollow.TeleportTo(Checkpoint.Point);
    }


    void UnfailLevelGoals()
    {
        foreach (var goal in CurrentLevel.Goals)
        {
            if (goal.Failed == true)
                goal.Failed = false;
        }
    }

    void ResumeGame()
    {
        if (startOfSession)
        {
            UIController.I.TutorialWindow.Open();
            startOfSession = false;
        }

        UIController.I.GameWindow.Open();
        IsRunning = true;
        Player.AllowMotion(true);

    }

    void SelectNewSettings()
    {
        int randomSeed = UnityEngine.Random.Range(0, int.MaxValue - 1);
        UnityEngine.Random.InitState(GameDataHolder.I.Save.Seed + (GameDataHolder.I.Save.CurrentLevel * 7));

        var loadedLevel = Resources.Load($"Levels/Level_{(UseDebugSettings ? DebugLevelNumber : GameDataHolder.I.Save.CurrentLevel)}_{LevelsVariant}");

        //SET UP SETTINGS
        if (UseDebugSettings == true)
        {
            if (loadedLevel != null)
            {
                var levelJsonGen = LevelLoader.LoadFromResources($"Levels/Level_{DebugLevelNumber}_{LevelsVariant}");
                _settings = GetSettingsByName(levelJsonGen.GeneratorSettingsName);
                _settings.GoalsCombination = GetGoalsByName(levelJsonGen.GoalsCombination.name);
                Debug.Log($"---[DEBUG-loaded] Level {DebugLevelNumber}");
            }
            else
            {
                _settings = DebugSettings;
                _settings.GoalsCombination = DebugGoalCombination;
                Debug.Log($"---[DEBUG-from_Debug_settings] Level {DebugLevelNumber}");
            }

        }
        else if (loadedLevel != null)
        {
            var levelJsonGen = LevelLoader.LoadFromResources($"Levels/Level_{GameDataHolder.I.Save.CurrentLevel}_{LevelsVariant}");
            _settings = GetSettingsByName(levelJsonGen.GeneratorSettingsName);
            _settings.GoalsCombination = GetGoalsByName(levelJsonGen.GoalsCombination.name);
            Debug.Log($"---[LOADED] Level {GameDataHolder.I.Save.CurrentLevel}");
        }
        else
        {
            _settings = _levelSettingsPool.GetRandom();
            _levelSettingsPool.DecreaseChance(_settings);
            Debug.Log($"---[GENERATED] Level {GameDataHolder.I.Save.CurrentLevel}, {_settings.name}, has any goals?: {_settings.GoalsCombination}");
        }

        //SET UP GOALS
        if (_settings.GoalsCombination == null)
        {
            if (_settings.AvailableGoalsCombination.Count() == 0)
            {
                Debug.Log($"--- Goals picked randomly from basic pool");
                _settings.GoalsCombination = GeneratorPresets.I.BasicGoalCombinations.Random(g => g.Probability);
            }
            else
            {
                Debug.Log($"--- Goals picked randomly from available goals");
                _settings.GoalsCombination = (_settings.AvailableGoalsCombination.Random(g => g.Probability));
            }
        }

        UnityEngine.Random.InitState(randomSeed);

    }

    GeneratorSettings GetSettingsByName(string settingsName)
    {
        var settings = _levelSettingsPool.Objects.GetByName(settingsName, x => x.name);
        if (settings == null)
            settings = Resources.Load($"GeneratorPresets/NotInPool/{settingsName}") as GeneratorSettings;

        return settings;
    }

    GoalCombinationSO GetGoalsByName(string goalsName)
    {
        var goalComb = Resources.Load($"GoalCombinations/Pool/{goalsName}") as GoalCombinationSO;
        if (goalComb == null)
            goalComb = Resources.Load($"GoalCombinations/SpecialCases/{goalsName}") as GoalCombinationSO;

        return goalComb;
    }

    void TryGenerateAdditionalPlatforms()
    {
        if (CurrentLevel.Settings.Direction == DirectionType.Down)
        {
            //if (!CurrentLevel.Generator.IsInfinite && Player.transform.position.y < CurrentLevel.Generator.LevelHeight) return;
            //if (!CurrentLevel.Generator.IsInfinite && CurrentLevel.Generator.CurrentLevelHeight <= CurrentLevel.Generator.LevelHeight) return;

            if (Player.transform.position.y < CurrentLevel.Generator.CurrentLevelHeight + _levelGenerationThreshold)
            {
                CurrentLevel.Generator.GenerateIntermediatePlatforms(10f);
            }
        }
        else
        {
            //Dont Add platforms if player reached top
            //if (!CurrentLevel.Generator.IsInfinite && Player.transform.position.y > CurrentLevel.Generator.LevelHeight) return;
            //if (!CurrentLevel.Generator.IsInfinite && CurrentLevel.Generator.CurrentLevelHeight >= CurrentLevel.Generator.LevelHeight) return;


            if (Player.transform.position.y > CurrentLevel.Generator.CurrentLevelHeight - _levelGenerationThreshold)
            {
                CurrentLevel.Generator.GenerateIntermediatePlatforms(10f);
            }
        }
    }

    void CalculateDifficulty()
    {
        if (UseDebugSettings)
        {
            float localModifier = (LocalDifficulty.Evaluate(DebugLevelNumber % 10) - 0.5f) / 2f;
            Difficulty = Mathf.Clamp01(GlobalDifficulty.Evaluate(DebugLevelNumber) + localModifier);
        }
        else
        {
            float localModifier = (LocalDifficulty.Evaluate(GameDataHolder.I.Save.CurrentLevel % 10) - 0.5f) / 2f;
            Difficulty = Mathf.Clamp01(GlobalDifficulty.Evaluate(GameDataHolder.I.Save.CurrentLevel) + localModifier);
        }
    }

    void CheckWinFailState()
    {
        if (CurrentLevel.GoalsReached())
        {
            Debug.Log($"Goals reached");
            PauseGame();

            if (UseDebugSettings)
                DebugLevelNumber++;
            else
            {
                AnalyticsController.I.LogEvent($"level_complete",
                    new Firebase.Analytics.Parameter($"level_number", GameDataHolder.I.Save.CurrentLevel),
                    new Firebase.Analytics.Parameter($"level_settings", $"{_settings.name}_{_settings.GoalsCombination.name}"),
                    new Firebase.Analytics.Parameter($"attempts", GameDataHolder.I.Save.CurrentLevelAttempt),
                    new Firebase.Analytics.Parameter($"difficulty", ((GameDataHolder.I.Save.CurrentLevelAttempt - 1) / GameDataHolder.I.Save.CurrentLevelAttempt) * 100));
                GameDataHolder.I.Save.CurrentLevelAttempt = 0;
                GameDataHolder.I.Save.CurrentLevel++;
            }

            AudioController.I.Play(AudioController.I.GameWin, 0.7f);
            UIController.I.LevelCompleteWindow.Open();
        }

        if (CurrentLevel.AnyGoalFailed())
        {
            Debug.Log($"Goals failed");
            PauseGame();
            if (AlreadyDied)
            {
                AudioController.I.Play(AudioController.I.GameLose, 0.5f);
                UIController.I.GameOverWindow.Open();
            }
            else
            {
                UIController.I.SecondChanceWindow.Open();
                AlreadyDied = true;
            }
        }
    }

    /*
    void OnPlayerJump(PlayerStartedJump info)
    {
        
    }


    private void OnEnable()
    {
        EventSystem.AddListener<PlayerStartedJump>(OnPlayerJump);
    }

    private void OnDisable()
    {
        EventSystem.DeleteListener<PlayerStartedJump>(OnPlayerJump);
    }
    */
}
