﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorsInRowController : PersistentHumbleSingleton<FloorsInRowController>
{
    public float NumberOfSuperJumps { get; private set; }
    public float PrevTotalHeight { get; set; }

    [Header("Settings")]
    [SerializeField] float _yThreshold = 15f;
    [SerializeField] float _maxTimer = 3f;
    [SerializeField] float _jumpSpeedYThreshold = 15f;

    //[SerializeField] ParticleSystem _particles;
    [SerializeField] Transform _rushParent;
    [SerializeField] Transform _rushBar;
    [SerializeField] Text _rushText;
    [SerializeField] CameraShaker _camShaker;

    float _thisJumpMaxHeight;
    Rigidbody2D _playerRB;
    float _jumpStartY;
    bool _countingCurrJump;
    bool _onStreak;
    Vector3 _rushBarInitScale;
    bool _lockTimer;

    float Timer
    {
        get
        {
            return _timer;
        }
        set
        {
            _timer = value;
        }
    }

    float _timer;

    void Start()
    {
        _playerRB = GameController.I.Player.GetComponent<Rigidbody2D>();
        _rushBarInitScale = _rushBar.localScale;
        _camShaker = Camera.main.GetComponent<CameraShaker>();
        _rushParent.gameObject.SetActive(false);
    }

    void FixedUpdate()
    {
        if (_onStreak)
        {
            if (_lockTimer == false)
                Timer = Mathf.Clamp(Timer - Time.fixedDeltaTime, 0, 1000);


            float timerProgress = (Timer / _maxTimer);
            _rushBar.localScale = _rushBar.localScale.With(x: _rushBarInitScale.x * timerProgress);
            _rushText.text = NumberOfSuperJumps.ToString("0");


        }
    }
    void NewSuperJump()
    {
        _onStreak = true;
        //_rushParent.gameObject.SetActive(true);
        Timer = _maxTimer;
        AudioController.I.Play(AudioController.I.SuperJumpSound, 0.7f);
        GameController.I.Player.PlayTrail();
        //_particles.Stop();
        //_particles.Play();


        Instantiate(Prefabs.I.RushJump, _playerRB.position, Quaternion.identity);
        NumberOfSuperJumps++;
    }


    //private void StoreFloorsInRow()
    //{
    //    TotalHeight += FloorsInRow;
    //    FloorsInRow = 0;
    //    //_particles.gameObject.SetActive(false);
    //    _particles.Stop();
    //    EventSystem.FireEvent<FloorsInRowValuesChanged>(new FloorsInRowValuesChanged(FloorsInRow, TotalHeight));
    //}

    //private void AddFloorsInRow(float amount)
    //{
    //    FloorsInRow += amount;
    //    Timer = _maxTimer;
    //    //_particles.gameObject.SetActive(true);
    //    _particles.Stop();
    //    _particles.Play();
    //    EventSystem.FireEvent<FloorsInRowValuesChanged>(new FloorsInRowValuesChanged(FloorsInRow, TotalHeight));
    //}

    //void StartStreak()
    //{
    //    _rushParent.gameObject.SetActive(true);
    //    _onStreak = true;
    //    _countingCurrJump = true;
    //    Timer = _maxTimer;
    //    _particles.Stop();
    //    _particles.Play();
    //    //Debug.Log($"Streak started. Total: {TotalHeight}, this jump: {_thisJumpMaxHeight}");

    //}

    //void ContinueStreak()
    //{
    //    _countingCurrJump = true;
    //    PrevTotalHeight += _thisJumpMaxHeight;
    //    _thisJumpMaxHeight = 0f;
    //    Timer = _maxTimer;
    //    //Debug.Log($"Continue streak. Total: {TotalHeight}, this jump: {_thisJumpMaxHeight}");
    //}

    void FlightTimer(bool lockTimer)
    {
        if (_onStreak == false) return;
        Timer = _maxTimer;
        _lockTimer = lockTimer;
    }

    //void PauseStreak()
    //{
    //    _countingCurrJump = false;
    //    PrevTotalHeight += _thisJumpMaxHeight;
    //    _thisJumpMaxHeight = 0f;
    //}
    void EndStreak()
    {
        NumberOfSuperJumps = 0f;
        //_rushParent.gameObject.SetActive(false);
        _onStreak = false;
        PrevTotalHeight = 0f;
        _thisJumpMaxHeight = 0f;
        _countingCurrJump = false;
        GameController.I.Player.StopTrail();
        //        _particles.Stop();
    }

    private void OnEnable()
    {
        EventSystem.AddListener<PlayerStartedJump>(OnPlayerJump);
        EventSystem.AddListener<LevelStarted>(OnLevelStarted);
        EventSystem.AddListener<BonusFlightEvent>(OnPlayerFlight);
    }
    private void OnDisable()
    {
        EventSystem.DeleteListener<PlayerStartedJump>(OnPlayerJump);
        EventSystem.DeleteListener<LevelStarted>(OnLevelStarted);
        EventSystem.DeleteListener<BonusFlightEvent>(OnPlayerFlight);
    }

    void OnPlayerFlight(BonusFlightEvent info)
    {
        if (info.Enable)
            FlightTimer(true);
        else
            FlightTimer(false);

    }

    void OnPlayerJump(PlayerStartedJump info)
    {
        if (info.JumpVelocity.y > _jumpSpeedYThreshold)
        {
            NewSuperJump();
            float trauma = info.JumpVelocity.y.Remap(_jumpSpeedYThreshold, _jumpSpeedYThreshold + 15f, 0.55f, 1f);
            _camShaker.AddTrauma(trauma);
        }
        else
        {
            if (Timer <= 0)
                EndStreak();
        }

    }

    void OnLevelStarted(LevelStarted info)
    {
        NumberOfSuperJumps = 0;
        PrevTotalHeight = 0;
        Timer = 0;
        _thisJumpMaxHeight = 0;
        GameController.I.Player.StopTrail();
        //_particles.Stop();
        EventSystem.FireEvent<FloorsInRowValuesChanged>(new FloorsInRowValuesChanged(NumberOfSuperJumps, PrevTotalHeight));
    }





}
