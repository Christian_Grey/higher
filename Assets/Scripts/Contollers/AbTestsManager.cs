﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Extensions;
using System;
using UnityEngine;
using System.Threading.Tasks;

/*
 * Created on Sat Jul 13 2019
 *
 * Copyright (c) 2019 Global Sculptor LLC
 */
public class AbTestsManager : PersistentSingleton<AbTestsManager>
{
    public string levelVariant = "1";

    // Start is called before the first frame update
    void Start()
    {
        System.Collections.Generic.Dictionary<string, object> defaults = new System.Collections.Generic.Dictionary<string, object>();

        // These are the values that are used if we haven't fetched data from the
        // service yet, or if we ask for values that the service doesn't have:
        defaults.Add("propertyname_string", "default local string");
        defaults.Add("propertyname_int", 1);
        defaults.Add("propertyname_float", 1.0);
        defaults.Add("propertyname_bool", false);

        Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
        fetchDataAsync();
    }

    // Start a fetch request.
    public Task fetchDataAsync() {
        // FetchAsync only fetches new data if the current data is older than the provided
        // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
        // By default the timespan is 12 hours, and for production apps, this is a good
        // number.  For this example though, it's set to a timespan of zero, so that
        // changes in the console will always show up immediately.
        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(TimeSpan.Zero);
        return fetchTask.ContinueWithOnMainThread(fetchComplete);
    }

    void fetchComplete(Task fetchTask) {
        if (fetchTask.IsCanceled) {
            Debug.Log("Fetch canceled.");
        } else if (fetchTask.IsFaulted) {
            Debug.Log("Fetch encountered an error.");
        } else if (fetchTask.IsCompleted) {
            Debug.Log("Fetch completed successfully!");
        }

        var info = Firebase.RemoteConfig.FirebaseRemoteConfig.Info;
        switch (info.LastFetchStatus) {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();
                Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).", info.FetchTime));
                levelVariant = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("level_variant").StringValue;
            break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                switch (info.LastFetchFailureReason) {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.Log("Fetch failed for unknown reason");
                    break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                    break;
                }
            break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.Log("Latest Fetch call still pending.");
            break;
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
