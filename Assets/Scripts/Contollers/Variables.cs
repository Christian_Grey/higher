﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Variables : PersistentHumbleSingleton<Variables>
{
    public float LEVEL_HALF_WIDTH = 5.6f;
    public float AdditionalPlatformWidth;

    public const string COIN_TAG = "<sprite=\"IconsAtlas\" name=\"Coin\">";
    public const string COLLECTABLE_TAG = "<sprite=\"IconsAtlas\" name=\"Collectable\">";
    public const string ARROW_UP = "<sprite=\"IconsAtlas\" name=\"Arrow_Up\">";
    public const string ARROW_DOWN = "<sprite=\"IconsAtlas\" name=\"Arrow_Down\">";
    public const string ARROW_RIGHT = "<sprite=\"IconsAtlas\" name=\"Arrow_Right\">";
    public const string ARROW_LEFT = "<sprite=\"IconsAtlas\" name=\"Arrow_Left\">";

    public float BonusSpreadDistance = 90f;
    public float CoinSpreadDistance = 20f;
    public float CollectableSpreadDistance = 40f;

    public float PlatformDestroyMargin = 30f;
    public float WallThickness = 10f;
    public float WallSegmentHeight = 10f;

    public int InterstitialEveryNLevel = 5;
    public int RewardedX10EveryNLevel = 8;


    //public List<StatUpgradeValuesWithKey> BonusStatUpgradeValues;

    //public Dictionary<string, float> BonusDurationStepMap = new Dictionary<string, float>()
    //{
    //    {"BonusFlight", 0.2f },
    //    {"BonusBubble", 0.2f },
    //    {"BonusHigherJump", 0.2f },
    //    {"BonusPlatformStretch", 0.2f },
    //};

    //public Dictionary<string, int> BonusDurationCostMap = new Dictionary<string, int>()
    //{
    //    {"BonusFlight", 100 },
    //    {"BonusBubble", 100 },
    //    {"BonusHigherJump", 100 },
    //    {"BonusPlatformStretch", 100 },
    //};

}

