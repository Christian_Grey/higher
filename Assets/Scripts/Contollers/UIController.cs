﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : PersistentHumbleSingleton<UIController>
{
    public UIWindow StartWindow;
    public UIWindow UpgradeWindow;
    public UIWindow SkinsWindow;
    public UIWindow SettingsWindow;
    public UIWindow GameOverWindow;
    public UIWindow LevelCompleteWindow;
    public UIWindow SecondChanceWindow;
    public UIWindow GoalsWindow;
    public UIWindow ADWindow;
    public UIWindow NotEnoughMoney;
    public UIWindow PauseWindow;
    public UIWindow GameWindow;
    public UIWindow TutorialWindow;
    [SerializeField] Transform _uiRoot;

    // Start is called before the first frame update
    void Start()
    {
        StartWindow.Open();
        UpgradeWindow.Close();
        SkinsWindow.Close();
        SecondChanceWindow.Close();
        GameOverWindow.Close();
        GoalsWindow.Close();
        LevelCompleteWindow.Close();
        NotEnoughMoney.Close();
        PauseWindow.Close();
        GameWindow.Close();
        TutorialWindow.Close();
    }

    public void HotEnoughMoneyPopup()
    {
        NotEnoughMoney.Open();
        Debug.Log($"NOT ENOUGH MONEY");
    }

    public void PlayMessage(string msg)
    {
        Instantiate(Prefabs.I.PopupMessageUIItem, _uiRoot).Init(msg);
    }
    public void PlayMessage(string msg, float onScreenTime)
    {
        Instantiate(Prefabs.I.PopupMessageUIItem, _uiRoot).Init(msg, onScreenTime);
    }

    public void PlayCountdown(Action onComplete)
    {
        Sequence seq = DOTween.Sequence();

        var digitLifetime = 0.58f;

        seq.AppendCallback(() => Instantiate(Prefabs.I.CountdownUIItem, _uiRoot).Init(3.ToString(), digitLifetime));
        seq.AppendInterval(digitLifetime);
        seq.AppendCallback(() => Instantiate(Prefabs.I.CountdownUIItem, _uiRoot).Init(2.ToString(), digitLifetime));
        seq.AppendInterval(digitLifetime);
        seq.AppendCallback(() => Instantiate(Prefabs.I.CountdownUIItem, _uiRoot).Init(1.ToString(), digitLifetime));
        seq.AppendInterval(digitLifetime);
        seq.AppendCallback(() => onComplete());
    }
}
