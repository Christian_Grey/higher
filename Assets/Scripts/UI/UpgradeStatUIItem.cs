﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradeStatUIItem : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _statNameText;
    [SerializeField] TextMeshProUGUI _statAmountText;
    [SerializeField] TextMeshProUGUI _increaseStepText;
    [SerializeField] TextMeshProUGUI _costText;
    string _statName;
    Bonus _parentBonus;
    int _cost;

    public void Init(Bonus bonus, string statName)
    {
        _parentBonus = bonus;
        _statName = statName;
        UpdateValues();
    }

    void OnEnable()
    {

    }

    public void Upgrade()
    {
        GameDataHolder.I.SpendMoney(_cost, () => GameDataHolder.I.IncreaseBonusStatLevel(_parentBonus.name, _statName));
    }

    public void UpdateValues()
    {
        StatData statData = GameDataHolder.I.Save.BonusesData.GetByName(_parentBonus.name, x => x.Name).Stats.GetByName(_statName, x => x.Name);
        BonusStat bonusStat = _parentBonus.Stats.GetByName(statData.Name, x => x.Name);
        float currentValue = bonusStat.Value + statData.Level * bonusStat.UpgradeStep;
        float increaseStep = bonusStat.UpgradeStep;
        _cost = bonusStat.Cost + statData.Level * bonusStat.CostStep;

        //<color=green><size=75>1</size></color>
        _statNameText.text = $"{_statName}";
        _statAmountText.text = $"{currentValue}";
        _increaseStepText.text = $"+{increaseStep.ToString()}";
        _costText.text = _cost.ToString() + Variables.COIN_TAG;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
