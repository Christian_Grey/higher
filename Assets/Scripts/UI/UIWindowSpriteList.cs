﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWindowSpriteList : UIWindowWithTab
{
    //public List<AbstactSkinButton> ButtonsToDraw;
    [SerializeField] string _skinDataPath;
    [SerializeField] AbstactSkinButton _buttonToDraw;
    [SerializeField] Transform _grid;

    public override void Close()
    {
        ClearButtons();

        base.Close();
    }

    public override void Open()
    {
        base.Open();
        PopulateWithButtons();
    }


    void PopulateWithButtons()
    {
        ClearButtons();
        var skinsData = Resources.LoadAll<SkinData>(_skinDataPath);
        Debug.Log($"Skin data array: {skinsData.Length}");

        foreach (var sData in skinsData)
        {
            var button = Instantiate(_buttonToDraw, _grid);
            button.Init(sData);
        }
    }

    void ClearButtons()
    {
        foreach (Transform tr in _grid)
        {
            Destroy(tr.gameObject);
        }
    }

}
