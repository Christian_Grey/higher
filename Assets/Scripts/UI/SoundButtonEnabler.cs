﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundButtonEnabler : MonoBehaviour
{
    public bool EnableIfSoundEnabled;

    // Start is called before the first frame update
    void OnEnable()
    {
        Debug.Log($"save sound enabled? {GameDataHolder.I.Save.SoundEnabled}");
        if (GameDataHolder.I.Save.SoundEnabled && EnableIfSoundEnabled)
            gameObject.SetActive(true);
        else
            gameObject.SetActive(false);
    }

}
