﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpgradeWindow : UIWindow
{
    [SerializeField] Transform _grid;
    [SerializeField] Transform _statGrid;

    public override void Close()
    {
        base.Close();
        ClearStats();
    }

    public override void Open()
    {
        base.Open();
    }

    public void OpenStatsForBonus(string bonusName)
    {
        var bonusGO = Prefabs.I.Bonuses.GetGOByName(bonusName);
        var bonus = bonusGO.GetComponent<Bonus>();

        ClearStats();

        var description = Instantiate(Prefabs.I.StatTextUIItem, _statGrid).GetComponentInChildren<TextMeshProUGUI>();
        description.text = bonus.Description;

        foreach (var statData in GameDataHolder.I.Save.BonusesData.GetByName(bonusName, x => x.Name).Stats)
        {
            var statItem = Instantiate(Prefabs.I.BonusStatUIItem, _statGrid).GetComponent<UpgradeStatUIItem>();
            statItem.Init(bonus, statData.Name);
        }

    }

    void ClearStats()
    {
        foreach (Transform tr in _statGrid)
        {
            Destroy(tr.gameObject);
        }
    }
}
