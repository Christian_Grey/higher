﻿using UnityEngine;
using System.Collections;
using ExternalService;
using DG.Tweening;
using UnityEngine.UI;

public class ContinueAdButton : MonoBehaviour
{
    [Autohook]
    public Button Button;

    public void Click()
    {
        Debug.Log($"Continue button clicked, Ad ready? {GoogleAds.I.IsRewardedReady()}");
        if (GoogleAds.I.IsRewardedReady())
        {
            GoogleAds.I.ShowRewarded();
            Button.interactable = false;
        }
        else
        {
            GoogleAds.I.RequestRewarded();
            UIController.I.PlayMessage("Ad not ready", 1.25f);
        }
#if UNITY_EDITOR
        Button.interactable = false;
        OnSuccessWithDelay();
#endif


    }

    void OnEnable()
    {
        Button.interactable = true;
        GoogleAds.UserEarnedRewardEvent += OnSuccessWithDelay;
    }

    void OnDisable()
    {
        GoogleAds.UserEarnedRewardEvent -= OnSuccessWithDelay;
    }

    void OnSuccessWithDelay()
    {
        //UIController.I.PlayMessage("Connecting to server", 1.25f);
        DOTween.Sequence().SetDelay(1.25f).AppendCallback(OnSuccess);
    }

    protected virtual void OnSuccess()
    {
        UIController.I.SecondChanceWindow.Close();
        GameController.I.GiveSecondChance();
    }

}
