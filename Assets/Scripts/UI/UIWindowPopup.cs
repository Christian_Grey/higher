﻿using UnityEngine;
using System.Collections;

public class UIWindowPopup : UIWindow
{
    [SerializeField] float _onScreenTime = 2f;
    public override void Close()
    {
        base.Close();
        CancelInvoke();
    }

    public override void Open()
    {
        base.Open();
        Invoke("Close", _onScreenTime);
    }
}
