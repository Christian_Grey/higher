﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIWindowGoals : UIWindow
{
    [SerializeField] Transform _goalsGrid;

    public override void Close()
    {
        base.Close();
    }

    public override void Open()
    {
        base.Open();

        //TODO:
        //Instantiate GoalUIStripped
        // Assign text and image to this
        foreach (var goal in GameController.I.CurrentLevel.Goals)
        {
            var goalUI = GameObject.Instantiate(goal, _goalsGrid);
            StripAllGameScripts(goalUI.gameObject);
        }

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void StripAllGameScripts(GameObject go)
    {
        var list = new List<MonoBehaviour>();
        list.AddRange(go.GetComponentsInChildren<MonoBehaviour>().Where(b =>
                !(b is Image || b is Text)));
        foreach (var behaviour in list)
        {
            Destroy(behaviour);
        }
    }
}
