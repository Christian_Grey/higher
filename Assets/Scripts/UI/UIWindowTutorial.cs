﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWindowTutorial : UIWindow
{
    bool _rightButtonPressed, _leftButtonPressed;

    public void RightButtonPressed()
    {
        _rightButtonPressed = true;
        CheckForCompletion();
    }
    public void LeftButtonPressed()
    {
        _leftButtonPressed = true;
        CheckForCompletion();
    }

    void CheckForCompletion()
    {
        if (_leftButtonPressed && _rightButtonPressed)
            this.Close();
    }


}
