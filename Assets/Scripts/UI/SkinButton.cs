﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinButton : AbstactSkinButton
{
    protected override void ChangeSkin()
    {
        GameController.I.Player.ChangeSkin(_skinData.name);
    }
}
