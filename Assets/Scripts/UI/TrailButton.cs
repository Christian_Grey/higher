﻿using UnityEngine;
using System.Collections;

public class TrailButton : AbstactSkinButton
{
    protected override void ChangeSkin()
    {
        GameController.I.Player.ChangeTrail(_skinData.name);
    }
}
