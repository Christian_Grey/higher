﻿using UnityEngine;
using System.Collections;
using ExternalService;
using DG.Tweening;
using UnityEngine.UI;

public class CollectMoneyAdButton : CollectMoneyButton
{
    [Autohook]
    public Button Button;

    public override void Click()
    {
        Debug.Log($"Collect button clicked, Ad ready? {GoogleAds.I.IsRewardedReady()}");
        if (GoogleAds.I.IsRewardedReady())
        {
            GoogleAds.I.ShowRewarded();
            Button.interactable = false;
        }
        else
        {
            GoogleAds.I.RequestRewarded();
            UIController.I.PlayMessage("Ad not ready", 1.25f);
        }

#if UNITY_EDITOR
        Button.interactable = false;
        OnSuccess();
#endif
    }

    void OnEnable()
    {
        Button.interactable = true;
        GoogleAds.UserEarnedRewardEvent += OnSuccess;
    }

    void OnDisable()
    {
        GoogleAds.UserEarnedRewardEvent -= OnSuccess;
    }

    protected override void OnSuccess()
    {
        DOTween.Sequence().SetDelay(1.25f).AppendCallback(base.OnSuccess);
    }
}
