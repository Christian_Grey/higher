﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;

public class PanelScaler : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _targetText;
    [SerializeField] float _horizontalOffset, _verticalOffset;
    [SerializeField] bool _conformToTextAlpha;


    RectTransform _recTrans;
    Image _panelImage;
    RectTransform _targetTextRecTrans;

    float _initPanelAlpha;

    void Start()
    {
        _panelImage = GetComponent<Image>();
        _initPanelAlpha = _panelImage.color.a;
        _recTrans = GetComponent<RectTransform>();
        _targetTextRecTrans = _targetText.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        _recTrans.position = _targetTextRecTrans.position;
        _recTrans.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _targetText.bounds.size.y * _targetTextRecTrans.localScale.y + _verticalOffset * 2f);
        _recTrans.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _targetText.bounds.size.x * _targetTextRecTrans.localScale.x + _horizontalOffset * 2f);
        if (_conformToTextAlpha)
        {
            var newCol = _panelImage.color;
            newCol.a = _targetText.color.a.Remap(0, 1, 0, _initPanelAlpha);
            _panelImage.color = newCol;
        }
    }
}
