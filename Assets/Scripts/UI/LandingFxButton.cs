﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingFxButton : AbstactSkinButton
{
    protected override void ChangeSkin()
    {
        GameController.I.Player.ChangeLandingFX(_skinData.name);
    }
}
