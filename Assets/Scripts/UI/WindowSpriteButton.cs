﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WindowSpriteButton : LockedButton
{
    [SerializeField] Button _button;
    [SerializeField] Image _imageOnButton;

    public void Init(Sprite sprite, Action onClickEvent, Action unlockEvent, Action unlockUnsuccessfulEvent, Func<bool> unlockCondition)
    {

        _imageOnButton.sprite = sprite;
        OnClickEvent.AddListener(() => onClickEvent());
        UnlockEvent.AddListener(() => unlockEvent());
        UnlockUnsuccessful.AddListener(() => unlockUnsuccessfulEvent());
        UnlockCondition = unlockCondition;
        _button.onClick.AddListener(() => OnClick());
    }


}
