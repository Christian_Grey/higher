﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class UIStartWindow : UIWindow
{
    //[SerializeField] Text _levelNumberText;
    [SerializeField] TextMeshProUGUI _levelNumberText;

    [SerializeField] GameObject SoundOnButton;
    [SerializeField] GameObject SoundOffButton;

    public override void Open()
    {
        base.Open();
        int levelNumber = GameController.I.UseDebugSettings ? GameController.I.DebugLevelNumber : GameDataHolder.I.Save.CurrentLevel;
        _levelNumberText.text = $"Level {levelNumber.ToString()}";

        if (GameDataHolder.I.Save.SoundEnabled)
        {
            SoundOnButton.SetActive(true);
            SoundOffButton.SetActive(false);
        }
        else
        {
            SoundOnButton.SetActive(false);
            SoundOffButton.SetActive(true);
        }
    }
}
