﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public abstract class AbstactSkinButton : MonoBehaviour
{
    bool Locked
    {
        set
        {
            _locked = value;
            _lockImage.SetActive(_locked);
        }
        get
        {
            return _locked;
        }
    }

    //public int Cost;

    //Links
    [SerializeField] GameObject _lockImage;
    [SerializeField] Image _skinImage;
    [SerializeField] TextMeshProUGUI _costText;
    protected SkinData _skinData;

    //protected Sprite _skinSprite;
    bool _locked;

    void Start()
    {

    }

    public void Init(SkinData sData)
    {
        _skinData = sData;
        _costText.text = _skinData.UnlockCost.ToString() + Variables.COIN_TAG;
        _skinImage.sprite = _skinData.UISprite;
        Locked = !GameDataHolder.I.Save.UnlockedSkins.Contains(_skinData.name);
    }

    public void Click()
    {
        // 
        if (_locked)
        {
            //try to buy
            GameDataHolder.I.SpendMoney(_skinData.UnlockCost, UnlockSkin);
        }
        else
        {
            //wear
            ChangeSkin();
        }
    }

    void UnlockSkin()
    {
        GameDataHolder.I.UnlockSkin(_skinData.name);
        Locked = false;
        ChangeSkin();
    }

    protected abstract void ChangeSkin();

}
