﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LockedButton : MonoBehaviour
{
    public UnityEvent UnlockEvent;
    public UnityEvent UnlockUnsuccessful;
    public UnityEvent OnClickEvent;
    public System.Func<bool> UnlockCondition;

    [SerializeField] GameObject _lockImage;

    bool _locked = true;



    public void OnClick()
    {
        if (_locked)
        {
            if (UnlockCondition())
            {
                UnlockEvent.Invoke();
                _locked = false;
                _lockImage.SetActive(false);
            }
            else
                UnlockUnsuccessful.Invoke();
        }
        else
        {
            OnClickEvent.Invoke();
        }
    }
}
