﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITabbedWindow : UIWindow
{
    public Button TabButton;
    [SerializeField] Transform _tabGrid;
    [SerializeField] Transform _buttonsGrid;
    [SerializeField] Transform _windowsParent;

    UIWindowWithTab[] _windows;
    Button[] _buttons;

    public override void Close()
    {
        base.Close();
        if (_windows == null)
            InitWindows();
        if (_buttons == null)
            InitButtons();


        foreach (var window in _windows)
        {
            window.Close();
        }

        //foreach (Transform tr in _tabGrid)
        //{
        //    Destroy(tr.gameObject);
        //}
    }

    public override void Open()
    {
        if (_windows == null)
            InitWindows();
        if (_buttons == null)
            InitButtons();
        base.Open();

        //foreach (var window in _windows)
        //{
        //    //var currentWindow = window;
        //    var button = Instantiate(TabButton, _tabGrid);
        //    button.transform.Find("Image").GetComponent<Image>().sprite = window.TabImage;
        //    button.onClick.AddListener(() =>
        //    {
        //        foreach (var w in _windows)
        //        {
        //            if (w == window)
        //                w.Open();
        //            else
        //                w.Close();

        //        }
        //    }
        //    );
        //}
    }

    void InitWindows()
    {
        _windows = _windowsParent.GetComponentsInChildren<UIWindowWithTab>(true);

        foreach (var window in _windows)
        {
            window.Close();
        }
        _windowsParent.gameObject.SetActive(true);
    }

    void InitButtons()
    {
        _buttons = _buttonsGrid.GetComponentsInChildren<Button>(true);

        for (int i = 0; i < _buttons.Length; i++)
        {
            int buttonNumb = i;
            _buttons[buttonNumb].onClick.AddListener(() =>
            {
                for (int j = 0; j < _windows.Length; j++)
                {
                    if (buttonNumb == j)
                        _windows[j].Open();
                    else
                        _windows[j].Close();
                }
            });





        }
    }
}



