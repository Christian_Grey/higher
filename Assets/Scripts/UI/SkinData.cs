﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class SkinData : ScriptableObject
{
    public Sprite UISprite;
    public GameObject CustomizationItem;
    public int UnlockCost = 100;
}
