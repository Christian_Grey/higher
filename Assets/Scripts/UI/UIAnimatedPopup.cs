﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
public class UIAnimatedPopup : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _messageText;
    [SerializeField] float _onScreenTime;

    public void Init(string message)
    {
        Init(message, _onScreenTime);
    }

    public void Init(string message, float onScreenTime)
    {
        _messageText.text = message;
        Color startingColor = _messageText.color;
        Color modifiedColor = startingColor;
        modifiedColor.a = 0f;
        _messageText.color = modifiedColor;

        Sequence seq = DOTween.Sequence();
        seq.Append(_messageText.DOColor(startingColor, onScreenTime * 0.2f));
        seq.AppendInterval(onScreenTime * 0.6f);
        seq.Append(_messageText.DOColor(modifiedColor, onScreenTime * 0.2f));
        seq.Join(_messageText.transform.DOScale(Vector3.one * 1.3f, onScreenTime));
        seq.OnKill(() => Destroy(gameObject));
    }

}
