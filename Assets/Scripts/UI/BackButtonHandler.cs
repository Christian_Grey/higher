﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BackButtonHandler : MonoBehaviour
{
    public UnityEvent OnBack;

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            OnBack?.Invoke();
        }
    }
}
