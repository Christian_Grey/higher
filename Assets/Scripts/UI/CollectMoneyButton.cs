﻿using ExternalService;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectMoneyButton : MonoBehaviour
{
    [SerializeField] float _moneyMultiplier;

    public virtual void Click()
    {
        GameController.I.TryToShowInterstitial();
        OnSuccess();
    }


    protected virtual void OnSuccess()
    {
        //TODO: Завернуть в делей
        GameController.I.CollectLevelMoney(_moneyMultiplier);
        UIController.I.LevelCompleteWindow.Close();
        UIController.I.StartWindow.Open();
        GUIController.I.Money.Open();
        GUIController.I.LevelMoney.Close();
        GameController.I.LoadLevel();
    }

}
