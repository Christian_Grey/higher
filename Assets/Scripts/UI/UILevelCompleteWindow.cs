﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UILevelCompleteWindow : UIWindow
{
    [SerializeField] GameObject _buttonx3;
    [SerializeField] GameObject _buttonx10;
    [SerializeField] TextMeshProUGUI _moneyText;

    public override void Close()
    {
        base.Close();
    }

    public override void Open()
    {
        base.Open();
        GUIController.I.LevelMoney.Close();
        _moneyText.text = GameController.I.CurrentLevel.LevelMoney.ToString() + Variables.COIN_TAG;

        if (GameDataHolder.I.Save.CurrentLevel >= GameDataHolder.I.Save.NextX10Ad)
        {
            _buttonx3.SetActive(false);
            _buttonx10.SetActive(true);
            GameDataHolder.I.Save.NextX10Ad += Variables.I.RewardedX10EveryNLevel.Range(2);
            GameDataHolder.I.SaveGameData();
        }
        else
        {
            _buttonx3.SetActive(true);
            _buttonx10.SetActive(false);
        }
    }
}
