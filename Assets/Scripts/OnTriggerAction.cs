﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerAction : MonoBehaviour
{
    public LayerMask LayersAllowedToTrigger;
    [SerializeField] bool _oneTime;
    public UnityEvent OnTriggerEnterEvent;

    bool _triggered;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //print(LayersAllowedToTrigger.value);
        Debug.Log($"{gameObject.name} DETECTED collision {collision.gameObject.name}");

        if (_oneTime && _triggered) return;

        OnTriggerEnterEvent.Invoke();
        _triggered = true;
    }
}
