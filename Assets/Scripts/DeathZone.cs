﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    BoxCollider2D _collider;


    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<BoxCollider2D>();

    }

    // Update is called once per frame
    void LateUpdate()
    {
        var cameraPoint = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0f, 0f));
        var offsetY = _collider.size.y / 2f;

        transform.position = cameraPoint.WithModified(y: -offsetY);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            EventSystem.FireEvent<PlayerHasDied>(new PlayerHasDied());
        }
    }

}
