﻿using System.Collections.Generic;
using UnityEngine;


public interface IPool
{
    GameObject Spawn(GameObject prefab);
    void Despawn(GameObject transform);
    void AddRecycleBin(GameObject prefab, List<TrashManRecycleBin> recycleBins, int preload);
    void ClearRecycleBins(List<TrashManRecycleBin> recycleBins);
}
