﻿using System.Collections.Generic;
using UnityEngine;


public class Prime31Pool : PersistentHumbleSingleton<Prime31Pool>, IPool
{
    private Dictionary<string, int> _spawned = new Dictionary<string, int>();

    public GameObject Spawn(GameObject prefab)
    {
        return TrashMan.spawn(prefab);
    }

    public GameObject Spawn(GameObject prefab, Vector3 pos, Quaternion rot, Transform parent)
    {
        var go = TrashMan.spawn(prefab, pos, rot);
        go.transform.SetParent(parent);

        return go;
    }

    public void PrintAll()
    {
        foreach (var pair in _spawned)
        {
            MonoBehaviour.print(pair.Key + ": " + pair.Value);
        }
    }

    public void Despawn(GameObject go)
    {
        TrashMan.despawn(go);
    }

    public void AddRecycleBin(GameObject prefab, List<TrashManRecycleBin> recycleBins, int preload)
    {
        var bin = new TrashManRecycleBin
        {
            prefab = prefab,
            instancesToPreallocate = preload <= 0 ? 5 : preload
        };
        TrashMan.manageRecycleBin(bin);
        recycleBins.Add(bin);
    }

    public void ClearRecycleBins(List<TrashManRecycleBin> recycleBins)
    {
        if (!TrashMan.instance) return;
        foreach (var bin in recycleBins)
        {
            TrashMan.removeRecycleBin(bin);
        }
    }


}
