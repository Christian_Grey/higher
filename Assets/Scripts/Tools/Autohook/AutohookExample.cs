using TMPro;
using UnityEngine;


public class AutohookExample : MonoBehaviour
{
    [Autohook]
    public TextMeshPro textMesh;
}