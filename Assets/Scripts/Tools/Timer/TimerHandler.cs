using System;
using System.Collections.Generic;
using UnityEngine;


public class TimerHandler : MonoBehaviour
{

    List<TimerInfo> timerList = new List<TimerInfo>();
    int runningTimerIdx = 0;

    void Update()
    {
        if (timerList.Count != 0)
        {
            for (int i = 0; i < timerList.Count; i++)
            {
                TimerInfo info = timerList[i];
                bool finished = info.UpdateTimer();
                if (finished)
                {
                    timerList.RemoveAt(i);
                }
            }
        }
    }


    // Public Static

    public static int ScheduleTimer(float delayInSecs, float lengthInSecs, bool repeats, TimerDelegate del)
    {
        TimerHandler handler = Camera.main.GetComponent<TimerHandler>();

        if (handler != null)
        {
            return handler.ScheduleTimerInternal(lengthInSecs, delayInSecs, repeats, del);
        }
        else
        {
            Debug.LogError("Main Camera does not contain TimerHandler component");
            return -1;
        }
    }

    public static bool CancelTimer(int id)
    {
        TimerHandler handler = Camera.main.GetComponent<TimerHandler>();

        if (handler != null)
        {
            return handler.CancelTimerInternal(id);
        }
        else
        {
            Debug.LogError("Main Camera does not contain TimerHandler component");
            return false;
        }
    }


    // Private

    public int ScheduleTimerInternal(float lengthInSecs, float delayInSecs, bool repeats, TimerDelegate del)
    {
        runningTimerIdx++;
        timerList.Add(new TimerInfo(runningTimerIdx, repeats, delayInSecs, lengthInSecs, del));

        return runningTimerIdx;
    }

    private bool CancelTimerInternal(int id)
    {
        if (timerList.Count != 0)
        {
            for (int i = 0; i < timerList.Count; i++)
            {
                if (timerList[i].id == id)
                {
                    timerList.RemoveAt(i);
                    return true;
                }
            }
        }
        return false;
    }
}