using UnityEngine;

public class TimerInfo : MonoBehaviour
{
    public int id;

    bool repeats;
    float timerLength;

    public float timeDelayRemaining;
    public float timeRemaining;
    public TimerDelegate timerDelegate;

    public TimerInfo(int id, bool repeats, float timeDelay, float timeRemaining, TimerDelegate timerDelegate)
    {
        this.id = id;

        this.repeats = repeats;
        timerLength = timeRemaining;

        timeDelayRemaining = timeDelay;
        this.timeRemaining = timeRemaining;
        this.timerDelegate = timerDelegate;
    }

    public bool UpdateTimer()
    {

        if (timeDelayRemaining > 0)
        {
            timeDelayRemaining -= Time.deltaTime;
        }
        else
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                timerDelegate(id);
                if (repeats)
                {
                    timeRemaining = timerLength;
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        return false;
    }
}

public delegate void TimerDelegate(int timerId);