﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class FileUtils
{
    public static void SafeWriteToFile(string content, string path)
    {
        try
        {
            using (StreamWriter sw = new StreamWriter(string.Concat(Application.persistentDataPath, "/", path), false))
            {
                sw.Write(content);
            }
        }
        catch (IOException e)
        {
            Debug.Log(e.StackTrace);
        }
    }

    public static T LoadAndDeserialize<T>(string path)
    {
        Debug.Log("var save = default(T);");
        var save = default(T);
        try
        {
            Debug.Log("var fullPath = string.Concat(Application.persistentDataPath, path);");
            var fullPath = string.Concat(Application.persistentDataPath, "/", path);
            Debug.Log("if (File.Exists(fullPath))");
            if (File.Exists(fullPath))
            {
                Debug.Log("using (var sr = new StreamReader(fullPath))");
                using (var sr = new StreamReader(fullPath))
                {
                    Debug.Log("save = JsonReader.Deserialize<T>(sr.ReadToEnd());");
                    string data = sr.ReadToEnd();
                    Debug.Log(data);
                    save = JsonUtility.FromJson<T>(data);
                }
            }
            Debug.Log("return save;");
            return save;
        }
        catch (IOException e)
        {
            Debug.Log("Debug.LogError(e.StackTrace);");
            Debug.LogError(e.StackTrace);
        }
        return default(T);
    }

    public static string Load(string path)
    {
        try
        {
            if (File.Exists(path))
            {
                using (var sr = new StreamReader(path))
                {
                    string data = sr.ReadToEnd();
                    return data;
                }
            }
            return null;
        }
        catch (IOException e)
        {
            Debug.Log($"File not found at: {path}");
        }
        return null;
    }
}
