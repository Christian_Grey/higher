﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ScreenshotTaker : MonoBehaviour
{
    [MenuItem("Screenshot/Take screenshot %#e")]
    static void Screenshot()
    {
        ScreenCapture.CaptureScreenshot("screenshot_ " + DateTime.Now.ToString("yyyy_MM_dd_THH_mm_ss") + ".png");
    }
}
