using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlatformPoolPseudoRandom
{
    public List<PlatformType> PlatformType = new List<PlatformType>();
    public List<float> InitWeights = new List<float>();
    public List<float> CurrentWeights;
    public float WeightSum;



    public PlatformPoolPseudoRandom(PlatformWeight[] inputObjects)
    {
        foreach (var platformAndWeight in inputObjects)
        {
            PlatformType.Add(platformAndWeight.Type);
            InitWeights.Add(platformAndWeight.Weight);
        }

        CurrentWeights = new List<float>(InitWeights);
        CalculateWeightSum();
    }

    protected void CalculateWeightSum()
    {
        WeightSum = 0;
        foreach (var item in InitWeights)
        {
            WeightSum += item;
        }
    }

    /// <summary>
    /// Range 0-1, where 0 is lowest difficulty available for current setting, and 1 is vice versa
    /// </summary>
    /// <param name="difficulty"></param>


    public PlatformType GetDefault()
    {
        return PlatformType.FirstOrDefault();
    }

    public PlatformType GetRandom()
    {
        PlatformType type = PlatformType.Random(GetWeight);

        return type;
    }


    /// <summary>
    /// Decreases the probability chance for this prefab. INTELLEGENTLY. That means that probability curve will NOT smooth out over time.
    /// </summary>
    /// <param name="obj">Object.</param>
    public void DecreaseProbabilityChanceForThisType(PlatformType type)
    {
        //if (!currentWeights.ContainsKey(obj.name))
        //{
        //    Debug.LogWarning("Can't decrease probability. Obj was not found:   " + obj.name);
        //    return;
        //}


        // This is my algorithm
        // Decrease appearance chance for the element we got, icrease appearance change for everyone else
        var index = PlatformType.IndexOf(type);
        Debug.Log($"Index of random obj {index}");

        for (int i = 0; i < CurrentWeights.Count; i++)
        {
            var weightToPercent = (InitWeights[i] / WeightSum) * 100f; //take init weight and scale. Range now >>>> from 0 to 100
            Debug.Log($"{i} - Weight: {InitWeights[i]}, Current weight: {CurrentWeights[i]}, Percent: {weightToPercent}");
            var missingTo100 = 100f - weightToPercent;

            var decreasingQuant = missingTo100 / 1f;
            var increasingQuant = weightToPercent / 1f;

            //from percent to weights
            decreasingQuant = decreasingQuant / 100f * WeightSum;
            increasingQuant = increasingQuant / 100f * WeightSum;

            // когда попадает на объект минусуем сколько % до 100 не хватает 
            if (i == index)
            {
                CurrentWeights[i] -= decreasingQuant;
                Debug.Log($"{i} Descreasing quant: {decreasingQuant}, Weight after decr: {CurrentWeights[i]}");
                continue;
            }
            //всем остальным прибавляем свое значение
            CurrentWeights[i] += increasingQuant;
        }

    }

    float GetWeight(PlatformType type)
    {
        int index = PlatformType.IndexOf(type);
        return CurrentWeights.ElementAt(index);
    }

}

