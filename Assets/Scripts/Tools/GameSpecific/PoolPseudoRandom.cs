using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class PoolPseudoRandom<T>
{
    public List<T> Objects;
    public List<float> InitWeights;
    public List<float> CurrentWeights;
    public float WeightSum;

    public PoolPseudoRandom(List<T> objects, List<float> weights)
    {
        Objects = new List<T>(objects);
        InitWeights = new List<float>(weights);
        CurrentWeights = new List<float>(InitWeights);
        CalculateWeightSum();
    }

    public PoolPseudoRandom(List<T> objects, Func<T, float> weightFunc)
    {
        Objects = new List<T>(objects);
        InitWeights = new List<float>();

        foreach (var obj in Objects)
        {
            InitWeights.Add(weightFunc(obj));
        }

        CurrentWeights = new List<float>(InitWeights);
        CalculateWeightSum();
    }

    public PoolPseudoRandom(Dictionary<T, float> objectsAndWeights)
    {
        Objects = new List<T>();
        InitWeights = new List<float>();
        foreach (var obj in objectsAndWeights)
        {
            Objects.Add(obj.Key);
            InitWeights.Add(obj.Value);
        }

        CurrentWeights = new List<float>(InitWeights);
        CalculateWeightSum();
    }

    protected void CalculateWeightSum()
    {
        WeightSum = 0;
        foreach (var item in InitWeights)
        {
            WeightSum += item;
        }
    }

    /// <summary>
    /// Range 0-1, where 0 is lowest difficulty available for current setting, and 1 is vice versa
    /// </summary>
    /// <param name="difficulty"></param>

    public T GetDefault()
    {
        return Objects.FirstOrDefault();
    }

    public T GetRandom()
    {
        T obj = Objects.Random(GetWeight);

        return obj;
    }

    /// <summary>
    /// Decreases the probability chance for this prefab. INTELLEGENTLY. That means that probability curve will NOT smooth out over time.
    /// </summary>
    /// <param name="obj">Object.</param>
    public void DecreaseChance(T obj)
    {
        if (Objects.Contains(obj) == false)
        {
            Debug.LogError("can't decrease probability. obj was not found:   " + obj.ToString());
            return;
        }

        // This is my algorithm
        // Decrease appearance chance for the element we got, icrease appearance change for everyone else
        var index = Objects.IndexOf(obj);

        for (int i = 0; i < CurrentWeights.Count; i++)
        {
            var weightToPercent = (InitWeights[i] / WeightSum) * 100f; //Weight to percent. from 0 to 100
            var missingTo100 = 100f - weightToPercent;

            var decreasingQuant = missingTo100 / 1f;
            var increasingQuant = weightToPercent / 1f;

            //from percent to weights
            decreasingQuant = decreasingQuant / 100f * WeightSum;
            increasingQuant = increasingQuant / 100f * WeightSum;

            // когда попадает на объект минусуем сколько % до 100 не хватает 
            if (i == index)
            {
                CurrentWeights[i] -= decreasingQuant;
                continue;
            }
            //всем остальным прибавляем свое значение
            CurrentWeights[i] += increasingQuant;
        }

    }

    float GetWeight(T obj)
    {
        int index = Objects.IndexOf(obj);
        return CurrentWeights.ElementAt(index);
    }

}

