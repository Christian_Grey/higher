﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    /// <summary>
    /// Times per second
    /// </summary>
    public float UpdateFrequency = 1f;

    float timeBetweenUpdates;
    float timer;
    float fps;
    string fpsText;
    GUIStyle style = new GUIStyle();
    Rect rect = new Rect(10, 30, 200, 40);

    void Start()
    {
        timeBetweenUpdates = 1f / UpdateFrequency;
        style.normal.textColor = Color.black;
        style.fontSize = 35;
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer > timeBetweenUpdates)
        {
            timer = 0f;
            fps = 1f / Time.deltaTime;
            fpsText = $"{fps.ToString("F")} ({ (Time.deltaTime * 1000f).ToString("F") }ms)";
        }
    }

    private void OnGUI()
    {
        GUI.Label(rect, fpsText, style);
    }
}
