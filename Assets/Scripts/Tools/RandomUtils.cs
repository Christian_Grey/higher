﻿using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public static class RandomUtils
{

    public static T Random<T>(this IEnumerable<T> enumerable, Func<T, float> weightFunc)
    {
        float totalWeight = 0; // this stores sum of weights of all elements before current
        T selected = default(T); // currently selected element
        foreach (var data in enumerable)
        {
            //Есть слабость эдж кейса: если вес равен 0 - то все равно есть небольшой шанс что префаб выпадет
            float weight = UnityEngine.Mathf.Clamp(weightFunc(data), 0, float.MaxValue); // weight of current element
            float r = UnityEngine.Random.Range(0, totalWeight + weight); // random value
            if (r >= totalWeight) // probability of this is weight/(totalWeight+weight)
                selected = data; // it is the probability of discarding last selected element and selecting current one instead
            totalWeight += weight; // increase weight sum
        }

        return selected; // when iterations end, selected is some element of sequence. 
    }

    /*
    public static T PickRandom<T>(this IList<T> source)
    {
        return source[UnityEngine.Random.Range(0, source.Count)];
    }
    */

    public static T PickRandom<T>(this ICollection<T> source)
    {
        //return source[UnityEngine.Random.Range(0, source.Count())];
        return source.ElementAt(UnityEngine.Random.Range(0, source.Count()));
    }



    //	public static T Random<T> (this IEnumerable<T> enumerable, Func<T, int> weightFunc)
    //	{
    //		int totalWeight = 0; // this stores sum of weights of all elements before current
    //		T selected = default(T); // currently selected element
    //		foreach (var data in enumerable) {
    //			int weight = weightFunc (data); // weight of current element
    //			int r = UnityEngine.Random.Range (0, totalWeight + weight); // random value
    //			if (r >= totalWeight) // probability of this is weight/(totalWeight+weight)
    //				selected = data; // it is the probability of discarding last selected element and selecting current one instead
    //			totalWeight += weight; // increase weight sum
    //		}
    //		
    //		return selected; // when iterations end, selected is some element of sequence. 
    //	}
}

public class WeightedFloatList
{
    public List<WeightedFloatPair> Items = new List<WeightedFloatPair>();

    public WeightedFloatList(float minValue, float maxValue)
    {
        float range = maxValue - minValue;
        float step = range / 10f;

        for (int i = 0; i <= 10; i++)
        {
            Items.Add(new WeightedFloatPair(minValue + step * i, 10f));
        }
    }

    public void SetPeak(float peakLocation, float peakHeightMultiplier, float peakHalfWidth)
    {
        ResetWeights();

        for (int i = 0; i <= 10; i++)
        {
            float progress = i / 10f;
            float distToPeak = Mathf.Abs(peakLocation - progress);
            float progressToPeak = Mathf.Clamp01(distToPeak / peakHalfWidth);
            progressToPeak = (progressToPeak - 1) * -1;

            Items[i].Weight = (Mathf.Lerp(10f, 10f * peakHeightMultiplier, progressToPeak));
        }
    }

    public override string ToString()
    {
        string s = "";
        for (int i = 0; i <= 10; i++)
        {
            s += $"Value: {Items[i].Value.ToString("0.0")} ; ";
            s += $"Weigth: {Items[i].Weight.ToString("0.0")} ; \n ------- \n";
        }

        return s;

    }

    void ResetWeights()
    {
        for (int i = 0; i <= 10; i++)
        {
            Items[i].Weight = (10f);
        }
    }


}

public class WeightedFloatPair
{
    public float Value;
    public float Weight;

    public WeightedFloatPair(float value, float weight)
    {
        Value = value;
        Weight = weight;
    }
}