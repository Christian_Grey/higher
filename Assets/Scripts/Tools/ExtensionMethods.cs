using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    #region float Extension methods
    public static bool CloseTo(this float value, float closeTo, float margin)
    {
        if (value >= closeTo - margin && value <= closeTo + margin)
            return true;
        else
            return false;
    }

    public static float StepTo(this float value, float target, float stepSize)
    {
        float direction = Mathf.Sign(target - value);
        float result = value + (stepSize.Abs() * direction);
        if (target < value && result < target)
            result = target;
        if (target > value && result > target)
            result = target;

        return result;
    }

    public static float Clamp01(this float value)
    {
        return Mathf.Clamp01(value);
    }

    public static float Range(this float value, float spreadValue)
    {
        return Random.Range(value - spreadValue, value + spreadValue);
    }

    public static float Clamp(this float value, float min, float max)
    {
        return Mathf.Clamp(value, min, max);
    }

    public static float Abs(this float value)
    {
        return Mathf.Abs(value);
    }

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
    public static float RemapClamp(this float value, float from1, float to1, float from2, float to2)
    {
        var result = (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        if (to2 > from2)
            result = result.Clamp(from2, to2);
        else
            result = result.Clamp(to2, from2);
        return result;
    }

    public static float Invert01(this float value)
    {
        //return value.Remap(0f, 1f, 1f, 0f);
        return (value - 1f) * -1f;
    }

    public static float LerpTo(this float value, float lerpToNumber, float lerpQ = 0.5f)
    {
        return Mathf.Lerp(value, lerpToNumber, lerpQ);
    }
    #endregion

    #region int Extension methods

    public static int Range(this int value, int spreadValue)
    {
        return Random.Range(value - spreadValue, value + spreadValue + 1);
    }

    #endregion

    #region Vector3 Extension methods
    public static Vector3 With(this Vector3 original, float? x = null, float? y = null, float? z = null)
    {
        float newX = x.HasValue ? x.Value : original.x;
        float newY = y.HasValue ? y.Value : original.y;
        float newZ = z.HasValue ? z.Value : original.z;

        return new Vector3(newX, newY, newZ);
    }

    public static Vector3 WithModified(this Vector3 original, float? x = null, float? y = null, float? z = null)
    {
        float newX = x.HasValue ? original.x + x.Value : original.x;
        float newY = y.HasValue ? original.y + y.Value : original.y;
        float newZ = z.HasValue ? original.z + z.Value : original.z;

        return new Vector3(newX, newY, newZ);
    }

    public static Vector3 NearestPointOnAxis(this Vector3 axisDirection, Vector3 point, bool isNormalized = false)
    {
        if (!isNormalized) axisDirection.Normalize();
        var d = Vector3.Dot(point, axisDirection);
        return axisDirection * d;
    }
    #endregion

    #region Vector2 Extension methods
    public static Vector2 With(this Vector2 original, float? x = null, float? y = null)
    {
        float newX = x.HasValue ? x.Value : original.x;
        float newY = y.HasValue ? y.Value : original.y;

        return new Vector2(newX, newY);
    }

    public static Vector2 WithModified(this Vector2 original, float? x = null, float? y = null)
    {
        float newX = x.HasValue ? original.x + x.Value : original.x;
        float newY = y.HasValue ? original.y + y.Value : original.y;

        return new Vector2(newX, newY);
    }

    #endregion

    public static bool ContainBounds(this Bounds bounds, Bounds target)
    {
        return bounds.Contains(target.min) && bounds.Contains(target.max);
    }

    public static T GetGOByName<T>(this ICollection<T> source, string name) where T : Object
    {
        foreach (var obj in source)
        {
            if (obj.name == name)
                return obj;
        }

        return default(T);
    }

    public static T GetByName<T>(this ICollection<T> source, string name, System.Func<T, string> nameFunc)
    {
        foreach (var obj in source)
        {
            if (nameFunc(obj) == name)
                return obj;
        }

        return default(T);
    }

    public static T GetByName<T>(this ICollection<T> source, string name) where T : IHasName
    {
        foreach (var obj in source)
        {
            if (obj.GetName() == name)
                return obj;
        }

        return default(T);
    }
    public interface IHasName
    {
        string GetName();
    }
    #region Color Extension methods
    public static Color FromHex(this Color color, string hexString)
    {
        Color newCol;
        if (ColorUtility.TryParseHtmlString(hexString, out newCol))
            return newCol;
        else
            return color;
    }

    public static string ToHex(this Color color)
    {
        return ColorUtility.ToHtmlStringRGBA(color);
    }

    #endregion
}

