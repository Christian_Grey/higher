using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Tools
{
    public static void GetInChildren<T>(Transform transformToSearch, ref List<T> resultingList)
    {
        T c = transformToSearch.GetComponent<T>();
        if (c != null)
            resultingList.Add(c);
        foreach (Transform child in transformToSearch)
            GetInChildren<T>(child, ref resultingList);
    }


    public static bool PerlinNoiseBool(float x, float y, float threshold, float bias = 0f)
    {
        float value = Mathf.PerlinNoise(x, y);
        value = Mathf.Clamp01(value + bias);

        return value > threshold ? true : false;
    }




    public static float Elerp(float a, float b, float t)
    {
        return a * Mathf.Pow(b / a, t);
    }
}

