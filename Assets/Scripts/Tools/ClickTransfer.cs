﻿using UnityEngine;
using System.Collections;

public class ClickTransfer
{
    public static bool MouseOnGround(out Vector3 mouseGroundPos)
    {
        Plane ground = new Plane(Vector3.up, 0f);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float distToPlane;
        if (ground.Raycast(ray, out distToPlane))
        {
            //Get the point that is clicked
            mouseGroundPos = ray.GetPoint(distToPlane);
            return true;
        }
        else
        {
            Debug.LogWarning("Mouse can't intersect with ground");
            mouseGroundPos = -Vector3.one;
            return false;
        }

    }

}
