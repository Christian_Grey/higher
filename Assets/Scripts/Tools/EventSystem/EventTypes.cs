using UnityEngine;
using System.Collections;

public abstract class EventInfo
{
}

public class UnitDeathEventInfo : EventInfo
{

}

public class NewCommandSent : EventInfo
{

}

public class PlayerStartedJump : EventInfo
{
    public Vector3 FromCoords;
    public Vector3 JumpVelocity;
    public GameObject FromObject;

    public PlayerStartedJump(Vector3 fromCoords, Vector3 jumpVel, GameObject fromObject)
    {
        this.FromCoords = fromCoords;
        this.JumpVelocity = jumpVel;
        this.FromObject = fromObject;
    }
}

public class BonusTimerChanged : EventInfo
{
    public float NewTimerValueNormalized;

    public BonusTimerChanged(float newTimerValueNorm)
    {
        NewTimerValueNormalized = newTimerValueNorm;
    }
}

public class BonusFlightEvent : EventInfo
{

    public bool Enable;

    public BonusFlightEvent(bool enable)
    {
        this.Enable = enable;
    }
}

public class FloorsInRowValuesChanged : EventInfo
{
    public float FloorInRow;
    public float FloorsInRowTotal;

    public FloorsInRowValuesChanged(float currentFloorsInRow, float floorsInRowTotal)
    {
        FloorInRow = currentFloorsInRow;
        FloorsInRowTotal = floorsInRowTotal;
    }
}

public class PlayerCollectedGold : EventInfo
{
    public int Amount;

    public PlayerCollectedGold(int amount)
    {
        Amount = amount;
    }
}

public class MoneyChanged : EventInfo
{
    public int Amount;
    public bool LevelMoney;

    public MoneyChanged(int amount, bool levelMoney)
    {
        Amount = amount;
        LevelMoney = levelMoney;
    }
}

public class PlayerHasDied : EventInfo
{

    public PlayerHasDied()
    {

    }
}

public class LevelStarted : EventInfo
{
}


public class PlayerPassedALevel : EventInfo
{

}

public class FlagReached : EventInfo
{

}

public class CollectableCollected : EventInfo
{

}

