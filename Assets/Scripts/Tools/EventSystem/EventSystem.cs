﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSystem
{
    public delegate void EventListener<T>(T eventInfo) where T : EventInfo;

    private static Dictionary<System.Type, IList> eventListeners = new Dictionary<System.Type, IList>();

    public static void AddListener<T>(EventListener<T> listener) where T : EventInfo
    {
        System.Type t = typeof(T);
        if (eventListeners.ContainsKey(t) == false)
            eventListeners[t] = new List<EventListener<T>>();

        eventListeners[t].Add(listener);
    }

    public static void DeleteListener<T>(EventListener<T> listener) where T : EventInfo
    {
        System.Type t = typeof(T);
        eventListeners[t].Remove(listener);
    }

    public static void FireEvent<T>(T eventInfo) where T : EventInfo
    {
        System.Type t = typeof(T);
        if (eventListeners.ContainsKey(t) == false)
        {
            Debug.LogWarning(eventInfo.GetType().ToString() + " : Event fired but no one is listening!");
            return;
        }

        List<EventListener<T>> funcs = new List<EventListener<T>>((List<EventListener<T>>)eventListeners[t]);

        //foreach (EventListener<T> func in eventListeners[t])
        foreach (EventListener<T> func in funcs)
        {
            func(eventInfo);
        }
    }

    public static void Clear<T>() where T : EventInfo
    {
        System.Type t = typeof(T);
        eventListeners[t].Clear();
    }

}
