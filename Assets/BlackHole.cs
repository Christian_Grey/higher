﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    [SerializeField] float _pullForce;

    Transform _playerTransform;
    VirtCharacherController _player;
    float _radius;

    private void Start()
    {
        _radius = GetComponent<CircleCollider2D>().radius * transform.localScale.x;
    }

    private void FixedUpdate()
    {
        if (_playerTransform == null) return;

        Vector2 forceVector = transform.position - _playerTransform.position;
        var dist = Vector3.Distance(_playerTransform.position, transform.position);
        var knob = Mathf.Clamp01(dist / _radius).Invert01();
        forceVector = forceVector.normalized * _pullForce;
        _player.ExternalForce = forceVector;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.layer == 10)
        {
            _playerTransform = collision.transform;
            _player = collision.GetComponent<VirtCharacherController>();
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            _playerTransform = null;
            _player = null;
        }
    }

}
