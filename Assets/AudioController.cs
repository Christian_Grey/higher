﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : PersistentHumbleSingleton<AudioController>
{
    [Autohook]
    public AudioSource Source;

    public float GlobalVolume = 0.6f;

    public AudioClip JumpSound;
    public AudioClip SuperJumpSound;
    public AudioClip CollectableSound;
    public AudioClip BonusPickup;
    public AudioClip GameWin;
    public AudioClip GameLose;

    private readonly Dictionary<AudioClip, long> _timers = new Dictionary<AudioClip, long>();

    public void EnableSound(bool enable)
    {
        GameDataHolder.I.Save.SoundEnabled = enable;
        GameDataHolder.I.SaveGameData();
    }

    public void Play(AudioClip audioClip, float volume = 1)
    {
        if (GameDataHolder.I.Save.SoundEnabled == false)
            return;

        var ticks = DateTime.UtcNow.Ticks;
        long timer;
        _timers.TryGetValue(audioClip, out timer);
        //if (DateTime.UtcNow.Ticks - timer < 600000) return;

        Source.PlayOneShot(audioClip, volume * GlobalVolume);
        _timers[audioClip] = ticks;
    }

}
